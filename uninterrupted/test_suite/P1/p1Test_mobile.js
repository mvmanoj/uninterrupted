var conf = require('../../../nightwatch.conf.js');
var commonMethods= require('../../common/commonMethods');
var common_mobile= require('../../common/common_mobile');

module.exports = {

    /**
     * Test case is to validate that user is able to connect with Google
     */
    'UN 01: Connect With Google' : function connectWithGoogle(browser) {
        var homePage = browser.page.homepage();
        var googleLogin = browser.page.googleLoginPage();
        var feedPage = browser.page.feedPage();
        var header = browser.page.header();
        common_mobile.launchBrowser(browser);
        common_mobile.signOutIfSignedIn(browser);
        common_mobile.enableCookies(browser);
        header
            .clickOnConnectIcon_mobile();
        browser.pause(4000)
        homePage
            .waitForElementVisible('@connectWithUsText',4000)
            .clickOnGoogle_mob();
        browser.pause(6000);
        browser.window_handles(function (result) {
            var handle = result.value[1];
            browser.switchWindow(handle)
            googleLogin.waitForElementVisible('@emailID_Google',8000)
            browser.assert.title(browser.globals.title_google);
            googleLogin
                .enterEmail(browser.globals.email_google)
                .clickOnEmail_NextButton();
            googleLogin.enterPassword(browser.globals.pass_google)
                .clickOnPass_NextButton();
            browser
                .switchWindow(result.value[0]);
        })
        browser.pause(6000);
        header.assertHamburger_mob();
    },


    /**
     * Test case is to validate that user is able to connect with Facebook
     */
    'UN 02: Connect With Facebook' : function connectWithGoogle(browser) {
        var homePage = browser.page.homepage();
        var facebookLogin = browser.page.facebookLoginPage();
        var feedPage = browser.page.feedPage();
        var header = browser.page.header();
        common_mobile.launchBrowser(browser);
        common_mobile.signOutIfSignedIn(browser);
        common_mobile.enableCookies(browser);
        header
            .clickOnConnectIcon_mobile();
        browser.pause(2000)
        homePage
            .waitForElementVisible('@connectWithUsText',2000)
            .clickOnFacebook_mob();
        browser.pause(6000);
        browser.pause(6000);
        browser.window_handles(function (result) {
            var handle = result.value[1];
            browser.switchWindow(handle)
            facebookLogin.waitForElementVisible('@loginButton',2000)
            browser.assert.title(browser.globals.title_facebook)
            facebookLogin
                .enterUserCredentials(browser.globals.emailID_Fb, browser.globals.pass_Fb)
                .login();
            browser
                .switchWindow(result.value[0]);
        })
        browser.pause(6000);
        header.assertHamburger_mob();
    },

    /**
     * Test case is to validate that user is able to connect with Twitter
     */
    'UN 03: Connect With Twitter' : function connectWithTwitter(browser) {
        var homePage = browser.page.homepage();
        var twitterLogin = browser.page.twitterLoginPage();
        var feedPage = browser.page.feedPage();
        var header = browser.page.header();
        common_mobile.launchBrowser(browser);
        common_mobile.signOutIfSignedIn(browser);
        common_mobile.enableCookies(browser);
        header
            .clickOnConnectIcon_mobile();
        browser.pause(2000)
        homePage.clickOnTwitter_mob();
        browser.pause(6000);
        browser.window_handles(function (result) {
            var handle = result.value[1];
            browser.switchWindow(handle)
            twitterLogin.waitForElementVisible('@loginButton',2000)
            twitterLogin
                .enterUserCredentials(browser.globals.email_twitter, browser.globals.pass_twitter)
                .login();
            browser
                .switchWindow(result.value[0]);
        })
        browser.pause(6000);
        header.assertHamburger_mob();
    },

    /**
     * Test case is to validate that user is able to navigate to Podcast Page
     */
    'UN 04: Navigate to Podcast' : function navigateToPodcast(browser) {
        var podcastPage = browser.page.podcastPage();
        var header = browser.page.header();
        common_mobile.launchBrowser(browser);
        common_mobile.enableCookies(browser);
        header.clickOnPodcast();
        podcastPage
            .assertHeading(browser.globals.podcast_heading);
    },

    /**
     * Test case is to validate that user is able to navigate to Originals Page
     */
    'UN 05: Navigate to Originals' : function navigateToOriginals(browser) {
        var originalsPage = browser.page.originalsPage();
        var header = browser.page.header();
        common_mobile.launchBrowser(browser);
        common_mobile.enableCookies(browser);
        header.clickOnOriginal();
        originalsPage.assertHeading(browser.globals.originals_heading)
    },

    /**
     * Test case is to validate that user is able to view Search Ghost text 
     */
    'UN 07: Search Bar' : function searchBar(browser) {
        var header = browser.page.header();
        common_mobile.launchBrowser(browser);
        common_mobile.enableCookies(browser);
        header.clickOnSearch_mob()
            .assertSearchGhostText(browser.globals.search_ghostText);        
    },

    /**
     * Test case is to validate that user is able to navigate to Originals Page
     * from home page by clicking on "View More Originals" in Originals Container
     */
    'UN 08: View More Originals' : function viewMoreOriginals(browser) {
        var homepage = browser.page.homepage();
        var originalsPage = browser.page.originalsPage();
        common_mobile.launchBrowser(browser);
        common_mobile.enableCookies(browser);
        browser.execute(function() { window.scrollBy(0, 2000); }, [])
        homepage.clickOnMoreOriginals()
        originalsPage.assertHeading(browser.globals.originals_heading)
    },

    /**
     * Test case is to validate that user is able to view the originals
     * show by clicking on the Card in Original Container
     */
    'UN 09: View Originals Show' : function viewOriginalsShow(browser) {
        var homepage = browser.page.homepage();
        var originalCard = browser.page.originalCardPage();
        var athleteName;
        common_mobile.launchBrowser(browser);
        common_mobile.enableCookies(browser);
        browser.execute(function() { window.scrollBy(0, 2000); }, [])
        browser.pause(2000);
        homepage.getText('@originalCardName',function (result) {
            athleteName = result.value;
        })
            .clickOnOriginalCard();
        browser.pause(2000,function () {
            originalCard.assertOriginalAthleteName(athleteName);
        });
    },

    /**
     * Test case is to validate that user is able to navigate to Podcasts Page
     * from home page by clicking on "View More Podcasts" in Podcast Container
     */
    'UN 10: View More Podcasts' : function viewMorePodcasts(browser) {
        var homepage = browser.page.homepage();
        var podcastPage = browser.page.podcastPage();
        common_mobile.launchBrowser(browser);
        common_mobile.enableCookies(browser);
        browser.execute(function() { window.scrollBy(0, 3000); }, [])
        homepage.clickOnMorePodcasts();
        podcastPage
            .assertHeading(browser.globals.podcast_heading)
    },

    /**
     * Test case is to validate that user is able to view the podcast
     * show by clicking "All Episodes" on the Card from Podcasts Container
     */
    'UN 11: Podcasts Container All Episodes' : function allEpisodes_PodcastsContainer(browser) {
        var homePage = browser.page.homepage();
        var podcastCard = browser.page.podcastCardPage();
        var featAthleteName;
        common_mobile.launchBrowser(browser);
        common_mobile.enableCookies(browser);
        browser.execute(function() { window.scrollBy(0, 2000); }, [])
        homePage.waitForElementPresent('@podcastContainer',2000)
            .getText('@atheleteName_podcast',function (result) {
            featAthleteName= result.value;
        })
         //   .clickOnAllEpisodes_podcast();
      //  browser.pause(2000);
        browser.perform(function () {
            podcastCard.assertathlete(featAthleteName);
        });
    },

    /**
     * Test case is to validate that name of the video played from carousel
     *  is same as the video is playing
     */
    'UN 12: Carousel Video Name' : function carouselVideoName(browser) {
        var homepage = browser.page.homepage();
        var videoPage = browser.page.videoPage();
        var videoNameExpected, videoNameActual;
        common_mobile.launchBrowser(browser);
        common_mobile.enableCookies(browser);
        homepage
            .clcikOnFirstCarouselBar()
        browser.pause(1000);
        homepage
            .getText('@firstVideoPlayed_Carousel',function (result) {
            var input = result.value;
            videoNameExpected = input.toLowerCase();
        })
            .clickOnFirstWatch();        
        browser.pause(7000);
        videoPage.getText('@playedVideoName',function (result) {
            var input= result.value;
            videoNameActual = input.toLowerCase();
        });
        browser.perform(function () {
            browser.assert.equal(videoNameActual,videoNameExpected)
        });
    },

    /**
     * Test case is to validate that user is able to play video from "Latest Stories" Container
     */
    'UN 13: Play Video Latest Stories Container' : function playLatestStories(browser) {
        var homepage = browser.page.homepage();
        var videoPage = browser.page.videoPage();
        var videoNameExpected, videoNameActual;
        common_mobile.launchBrowser(browser);
        common_mobile.enableCookies(browser);
        browser.execute(function() { window.scrollBy(0, 5000); }, [])
        homepage.waitForElementVisible('@originalCard',1000);
        browser.execute(function() { window.scrollBy(0, 5000); }, [])
        browser.pause(1000)
        homepage.getText('@playedVideoName_latestStories',function (result) {
            var input = result.value;
            videoNameExpected = input.slice(0,45); 
        })
            .clickOnPlay_latestStories();
        browser.pause(2000)
        browser.execute(function() { window.scrollBy(0, 1000); }, [])
        browser.pause(500,function(){
            common_mobile.assertVideoPlayedName(browser,videoNameExpected);
        })
    },

    /**
     * Test case is to validate that user is able to play video from any Container
     *** Here we tried to play video from first container
     */
    'UN 14: Play Video First Container' : function playVideoContainer(browser) {
        var homepage = browser.page.homepage();
        var videoPage = browser.page.videoPage();
        var videoNameExpected, videoNameActual;
        common_mobile.launchBrowser(browser);
        common_mobile.enableCookies(browser);
        browser.execute(function() { window.scrollBy(0, 5000); }, [])
            .pause(1000)
        homepage.getText('@playedVideoName_firstContainer',function (result) {
            videoNameExpected = result.value;
        })
            .clickOnPlay_firstContainer();
        browser.pause(2000)
        browser.execute(function() { window.scrollBy(0, 2000); }, [])
            .pause(1000)
        browser.pause(500,function(){
            common_mobile.assertVideoPlayedName(browser,videoNameExpected);
        })
    },

    /**
     * Test case is to validate that video is automatically played.
     * i.e. When user clicked on "Watch" button, status of video in JWPlayer should by "playing"
     */
    'UN 15: Carousel Video Played Status' : function carouselVideoPlayedStatus(browser) {
        var homepage = browser.page.homepage();
        var videoPage = browser.page.videoPage();
        common_mobile.launchBrowser(browser);
        common_mobile.enableCookies(browser);
        homepage
            .waitForElementPresent('@firstCarouselSlide',2000)
            .clcikOnFirstCarouselBar()
            .clickOnFirstWatch();
        browser.refresh()
            .pause(7000);
            videoPage.clickOnPlayButton_mob();
        browser.execute(commonMethods.getJwPlayerState, function (version) {
            
            browser.assert.equal(version.value,browser.globals.state_play_jwplayer);
        });
    },

    /**
     * Test case is to validate that any video can be viewed from the carousel with the following functionality
     * Play
     * Pause
     * Mute
     * Next
     * Close
     */
    'UN 16: J W Player Cases' : function jwPlayerCases(browser) {
        var homepage = browser.page.homepage();
        var videoPage = browser.page.videoPage();
        var videoNameExpected;
        common_mobile.launchBrowser(browser);
        common_mobile.enableCookies(browser);
        browser.getTitle(function (result) {
            homepage
                .waitForElementPresent('@secCarouselSlide',2000)
                .clcikOnSecCarouselBar()
                .clickOnSecWatch();
            browser
                .refresh()
                .pause(5000)
            videoPage.moveToElement('@videoScreen',100,100,function () {
                //videoPage.clickOnPlayButton_mob();
                browser.pause(2000);
                    browser
                        .execute(commonMethods.getJwPlayerState, function (playerState) {
                        browser.assert.equal(playerState.value,browser.globals.state_pause_jwplayer);
                        browser.pause(2000);
                        browser.execute(function() { window.scrollBy(0, 500); }, [])
                        videoPage.getText('@secondVideo',function (result) {                           
                            videoNameExpected = result.value;
                        });
                    });
                    videoPage
                        .clickOnMute();
                    browser
                        .pause(1000)
                        .execute(commonMethods.getJwPlayerMuteState, function (muteState) {
                        browser.assert.equal(muteState.value,true);
                    });
                    videoPage.clickOnFullScreenOn()
                        .clickOnFullScreenOff()
                    browser.execute(function() { window.scrollBy(0, 500); }, [])
                    browser.pause(2000,function(){
                        common_mobile.assertVideoPlayedName(browser,videoNameExpected) 
                    });
            });
                videoPage.clickOnCloseButton();
            browser
                .refresh()
                .pause(3000)
                .assert.title(result);
        })
    },

    /**
     * Test Case is to validate user is able to play any video from Drawer in video page
     */
    'UN 17: Play Video From Drawer' : function playVideo_drawer(browser) {
        var homepage = browser.page.homepage();
        var videoPage = browser.page.videoPage();
        var videoNameExpected, videoNameActual;
        common_mobile.launchBrowser(browser);
        common_mobile.enableCookies(browser);
        homepage
            .waitForElementPresent('@firstCarouselSlide',2000)
            .clcikOnSecCarouselBar()
            .clickOnSecWatch();
        browser.pause(2000);
        browser.execute(function() { window.scrollBy(0, 500); }, [])
        videoPage
            .getText('@secondVideo',function (result) {
            videoNameExpected = result.value;
        })
        videoPage.playSecondVideo()
        browser.pause(1000);
        browser.execute(function() { window.scrollBy(0, 500); }, [])
        videoPage
            .waitForElementPresent('@playedVideoName',2000)
            .getText('@playedVideoName',function (result) {
            videoNameActual = result.value;
        });
        browser.perform(function () {
            browser.assert.equal(videoNameActual,videoNameExpected)
        });
    },

    /**
     * Test Case is to validate user is able to change playlist from video drawer
     * And play video from the playlist
     */
    'UN 18: Play Video From Playlist' : function playVideo_playlist(browser) {
        var homepage = browser.page.homepage();
        var videoPage = browser.page.videoPage();
        common_mobile.launchBrowser(browser);
        common_mobile.enableCookies(browser);
        homepage
            .waitForElementPresent('@firstCarouselSlide',2000)
            .clcikOnSecCarouselBar()
            .clickOnSecWatch();
        browser.pause(6000)
        videoPage.clickOnFirstPlaylist();
        browser.pause(1000);
        videoPage.assertPlaylistColor(browser.globals.played_playlist_color);
        videoPage.getText('@firstVideo',function (result) {
            videoNameExpected = result.value;
        })
        videoPage.playFirstVideo();
        browser.pause(2000);
        videoPage.getText('@playedVideoName',function (result) {
            videoNameActual = result.value;
        });
        browser.perform(function () {
            browser.assert.equal(videoNameActual,videoNameExpected)
        });
    },

    /**
     * Test case is to validate that user is able to share video on facebook
     * For validation:: Name of the video shared should match in Facebook popup
    **/
    'UN 21: Signed with Facebook::Share Video' : function shareVideo_signed(browser) {
        var homepage = browser.page.homepage();
        var videoPage = browser.page.videoPage();
        var postToFacebook = browser.page.postToFacebookPage();
        var header = browser.page.header();
        var videoNameExpected;
        common_mobile.launchBrowser(browser);
        common_mobile.signOutIfSignedIn(browser);
        common_mobile.connectWithFacebook(browser, browser.globals.emailID_Fb, browser.globals.pass_Fb);
        header.clickOnHomePageLink();
        common_mobile.enableCookies(browser);
        homepage
            .clcikOnFirstCarouselBar()
        browser.pause(3000);
        homepage.getText('@videoName_Carousel',function(result){
                var input = result.value;
                videoNameExpected = input.toLowerCase();
        })
        .clickOnFirstWatch();
        browser.pause(2000);
        videoPage
            .waitForElementPresent('@facebookShareButton_mob',2000)
            .clickOnFacebookShareButton_mob();
        browser.perform(function(){
            commonMethods.assertFacebookShareContent(browser,videoNameExpected);
        });
    },

    // /**
    //  * Test case is to validate that user is able to tweet video
    //  * For validation:: Link of the video shared should match in Twitter popup
    // **/
    // 'UN 22: Signed with Twitter::Tweet Video' : function tweetVideo_signed(browser) {
    //     var homepage = browser.page.homepage();
    //     var videoPage = browser.page.videoPage();
    //     var header = browser.page.header();
    //     var tweet = browser.page.tweetPage();
    //     common_mobile.launchBrowser(browser);
    //     common_mobile.signOutIfSignedIn(browser);
    //     common_mobile.connectWithTwitter(browser, browser.globals.email_twitter, browser.globals.pass_twitter);
    //     header.clickOnHomePageLink();
    //     common_mobile.enableCookies(browser);
    //     homepage
    //         .moveToElement('@firstCarouselSlide',0,0,function(){
    //             homepage.clcikOnFirstCarouselBar();
    //             browser.pause(2000);
    //             homepage.clickOnFirstWatch();
    //         })
    //     videoPage.waitForElementPresent('@tweetButton',6000);
    //     videoPage.clickOnTweetButton_mob();
    //     commonMethods.assertTwitterTweetContent(browser);
    // },

    /**
     * Test case is to validate that user is able to play the podcast from the card
     */
    'UN 23: Play Podcast From Card' : function playPodcast_Card(browser){
        var header = browser.page.header();
        var podcastPage = browser.page.podcastPage();
        common_mobile.launchBrowser(browser);
        common_mobile.enableCookies(browser);
        header.clickOnPodcast();
        podcastPage
            .assertHeading(browser.globals.podcast_heading)
            .playPodcast()
            .assertStickyPlayerPresent()
            .assertBgColor(browser.globals.bgColor_podcastCard);
    },

    /**
     * Test case is to validate that user is able to navigate to Podcast episode by 
     * clicking on the show name on sticky player.
     */
    'UN 24: Navigate Podcast From Sticky Player' : function navigatePodcast_Sticky(browser){
        var header = browser.page.header();
        var podcastPage = browser.page.podcastPage();
        common_mobile.launchBrowser(browser);
        common_mobile.enableCookies(browser);
        var podcastNameOnSticky;
        header.clickOnPodcast();
        podcastPage.waitForElementPresent('@headphoneIcon',2000)
            .playPodcast()
            .waitForElementPresent('@playedPodcast_Sticky',5000)
        browser.pause(1000);
        podcastPage.getText('@playedPodcast_Sticky',function(name){
                podcastNameOnSticky = name.value;
            })
            .clickOnPodcast_Sticky();
        browser.pause(5000);
        browser.perform(function(){
            var locatorVal = "(//p[text() = '"+podcastNameOnSticky+"']/parent::div/parent::div//div)[5]/child::*";
            browser
                .useXpath().waitForElementPresent(locatorVal,3000)
                .getAttribute(locatorVal,'class',function(result){
                browser.assert.equal(result.value, browser.globals.pause_className);
            });
        });
    },

    /**
     * Test case is to validate that user is able to view the podcast
     * show by clicking "All Episodes" on the Card from Podcasts Page
     */
    'UN 25: All Episodes Podcast' : function allEpisodesPodcast(browser){
        var podcastPage = browser.page.podcastPage();
        var podcastCard = browser.page.podcastCardPage();
        var header = browser.page.header();
        var podcastCardName;
        common_mobile.launchBrowser(browser);
        common_mobile.enableCookies(browser);
        header.clickOnPodcast();
        browser.pause(2000);
        browser.execute(function() { window.scrollBy(0, 1000); }, [])
        podcastPage
            .getText('@atheleteName',function (result) {
            featAthleteName= result.value;
        })
        //podcastPage
          //  .clickOnAllEpisodes();
        browser.pause(2000);
        browser.perform(function () {
            podcastCard.assertathlete(featAthleteName);
        });
    },

    /**
     * Test case is to validate the total count of episodes on the first page of a 
     * podcast by clicking on "All Episodes" in the Podcast Card
     * i.e Last episode displayed should be count = (Total-19) 
     * if there are more than or equal to 20 episodes
     */
    'UN 26: Episodes Count' : function episodesCount(browser){
        var podcastPage = browser.page.podcastPage();
        var podcastCard = browser.page.podcastCardPage();
        var header = browser.page.header();
        var podcastName;
        var lastEpisodeLocate = "(//p[@class='src-components-podcast-episode-list-__PodcastEpisode_meta__title_2-eH5'])[28]"
        common_mobile.launchBrowser(browser);
        common_mobile.enableCookies(browser);
        header.clickOnPodcast();
        browser.execute(function() { window.scrollBy(0, 1000); }, [])
        podcastPage.waitForElementPresent('@allEpisodesButton',2000)
            .clickOnAllEpisodes();
        podcastCard
            .waitForElementPresent('@totalNoOfEpisode',2000)
        podcastCard.getText('@totalNoOfEpisode',function (result) {
            noOfEpisode = result.value.split(" ",1);
        });
        browser.element('xpath',lastEpisodeLocate,function (result) {
            if(result.status != -1) {
                podcastCard.getText('@lastEpisodeOnPage',function (result2) {
                    var lastEpisodeText = result2.value;
                    lastEpisode = lastEpisodeText.slice(8);
                    browser.assert.equal((parseInt(noOfEpisode)-19),parseInt(lastEpisode));
                });
            } else {
                podcastCard.getText('@firstEpisodeOnPage',function (result) {
                    var firstEpisodeText = result.value;
                    firstEpisode = firstEpisodeText.slice(8);
                    //browser.assert.equal(parseInt(noOfEpisode),parseInt(firstEpisode));
                })
            }
        });
    },

    /**
     * Test case is to validate that user is able to play podcast episode from episode thumbnail
     * Validation:: Episode name should be displayed on the sticky player
     */
    'UN 27: Play Podcast From Thumbnail' : function playPodcast_thumbnail(browser){
        var podcastPage = browser.page.podcastPage();
        var podcastCard = browser.page.podcastCardPage();
        var header = browser.page.header();
        var podcastName, podcastNameOnSticky;
        common_mobile.launchBrowser(browser);
        common_mobile.enableCookies(browser);
        header.clickOnPodcast();
        browser.execute(function() { window.scrollBy(0, 1000); }, [])
        podcastPage.waitForElementPresent('@allEpisodesButton',2000)
            .clickOnAllEpisodes();
        podcastCard
            .waitForElementPresent('@headphone_first',2000)
            .clickOnHeadphone_first()
            .getText('@episodeName_first',function(result){
                podcastName = result.value;
        })
            .waitForElementPresent('@playedPodcast_Sticky',2000)
            .getText('@playedPodcast_Sticky',function(name){
                podcastNameOnSticky = name.value;
        });
        browser.perform(function(){
            browser.assert.equal(podcastNameOnSticky,podcastName)
        })
    },

    /**
     * Test case is to validate Play and pause button in episode thumbnail is 
     * sync with play and pause button of sticky player
     */
    'UN 28: Link Podcast From Sticky Player' : function linkPodcast_Sticky(browser){
        var header = browser.page.header();
        var podcastPage = browser.page.podcastPage();
        var podcastCard = browser.page.podcastCardPage();
        common_mobile.launchBrowser(browser);
        common_mobile.enableCookies(browser);
        var podcastNameOnSticky, classOnSticky, class_Headphn
        header.clickOnPodcast();
        browser.execute(function() { window.scrollBy(0, 1000); }, [])
        podcastPage.waitForElementPresent('@allEpisodesButton',2000)
        podcastPage.clickOnAllEpisodes();
        podcastCard
            .waitForElementPresent('@headphone_first',2000)
            .clickOnHeadphone_first()
            .waitForElementVisible('@headphoneIcon_first',3000)
            .waitForElementVisible('@stickyPlayerState',3000)
        browser.pause(7000)
        podcastCard.getAttribute('@headphoneIcon_first','xlink:href',function(result){
            class_Headphn = result.value;
        });
        podcastCard.getAttribute('@stickyPlayerState','xlink:href',function(result){
            classOnSticky = result.value;
        });
        browser.perform(function(){
            browser.assert.equal(class_Headphn,classOnSticky)
        });
        podcastPage.clickOnPlayPause_Sticky();
        podcastCard.getAttribute('@headphoneIcon_first','xlink:href',function(result){
            class_Headphn = result.value;
        });
        podcastCard.getAttribute('@stickyPlayerState','xlink:href',function(result){
            classOnSticky = result.value;
        });
        browser.perform(function(){
            browser.assert.equal(class_Headphn,classOnSticky)
        });
    },

    /**
     * Test case is to validate user is able to switch podcast episodes while playing one
     * Validation:: Podcast name on sticky player changed to the current played episode
     */
    'UN 29: Switch Podcast Episodes While Playing' : function switch_podcastEpisodes(browser){
        var header = browser.page.header();
        var podcastPage = browser.page.podcastPage();
        var podcastCard = browser.page.podcastCardPage();
        common_mobile.launchBrowser(browser);
        common_mobile.enableCookies(browser);
        var episode_first, episode_sec
        header.clickOnPodcast();
        browser.execute(function() { window.scrollBy(0, 1000); }, [])
        podcastPage.waitForElementPresent('@allEpisodesButton',2000)
            .clickOnAllEpisodes();
        podcastCard
            .waitForElementPresent('@headphone_first',2000)
            .clickOnHeadphone_first()
            .getText('@playedPodcast_Sticky',function(result){
            episode_first = result.value;
        });
        browser.execute(function() { window.scrollBy(0, 2000); }, [])
        podcastCard.clickOnHeadphone_sec();
        podcastCard.getText('@playedPodcast_Sticky',function(result){
            episode_sec = result.value;
        });
        browser.perform(function(){
            browser.assert.notEqual(episode_first,episode_sec)
        })
    },

    /**
     * Test case is to validate that user is able to share podcast episode on Facebook
     */
    'UN 32: Signed with Facebook::Share Podcast Episode' : function shareEpisode_Signed(browser) {
        var header = browser.page.header();
        var podcastPage = browser.page.podcastPage();
        var podcastCard = browser.page.podcastCardPage();
        var podcastNameExpected;
        common_mobile.launchBrowser(browser);
        common_mobile.enableCookies(browser);
        common_mobile.signOutIfSignedIn(browser);
        common_mobile.connectWithFacebook(browser, browser.globals.emailID_Fb, browser.globals.pass_Fb);
        header.clickOnPodcast();
        browser.execute(function() { window.scrollBy(0, 1000); }, [])
        podcastPage.waitForElementPresent('@allEpisodesButton',2000)
        podcastPage.clickOnAllEpisode_OpenRun();
        podcastCard.waitForElementPresent('@episodeName_first',2000)
        podcastCard.getText('@episodeName_first',function(result){
            var input = result.value;
            podcastNameExpected = input.toLowerCase();
        });
        podcastCard.clickOnfacebook_first()
        browser.perform(function(){
            commonMethods.assertFacebookShareContent(browser,podcastNameExpected);
        })
    },

    // /**
    //  * Test case is to validate that user is able to tweet podcast episode
    //  */
    // 'UN 33: Signed with Twitter::Tweet podcast Episode' : function tweetPodcast_Signed(browser) {
    //     var header = browser.page.header();
    //     var podcastPage = browser.page.podcastPage();
    //     var podcastCard = browser.page.podcastCardPage();
    //     var tweet = browser.page.tweetPage();
    //     var podcastID;
    //     common_mobile.launchBrowser(browser);
    //     common_mobile.enableCookies(browser);
    //     common_mobile.signOutIfSignedIn(browser);
    //     common_mobile.connectWithTwitter(browser, browser.globals.email_twitter, browser.globals.pass_twitter);
    //     header.clickOnPodcast();
    //     browser.execute(function() { window.scrollBy(0, 1000); }, [])
    //     podcastPage.waitForElementPresent('@allEpisodesButton',2000)
    //     podcastPage.clickOnAllEpisode_OpenRun();
    //     podcastCard.waitForElementPresent('@podcastEpisodeID',2000)
    //     podcastCard.getAttribute('@podcastEpisodeID','id',function(result){
    //         podcastID = result.value;
    //     })
    //     podcastCard.clickOntwitter_first();
    //     browser
    //         .window_handles(function (winHandle) {
    //         var handle = winHandle.value[1];
    //         browser.switchWindow(handle)
    //         tweet.waitForElementVisible('@shareText',5000);
    //         browser.perform(function(){
    //             tweet.assertContent(podcastID);
    //         })
    //         browser.closeWindow();
    //         browser.switchWindow(winHandle.value[0]);
    //     });       
    // },

    /**
     * Test case is to validate user is able to click and Subsribe button and 
     * can view the following in dropdown::
     * Apple Podcast
     */
    'UN 34: Subscribe Podcast' : function subscribePodcast(browser) {
        var header = browser.page.header();
        var podcastPage = browser.page.podcastPage();
        var podcastCard = browser.page.podcastCardPage();
        common_mobile.launchBrowser(browser);
        common_mobile.enableCookies(browser);
        header.clickOnPodcast();
        browser.execute(function() { window.scrollBy(0, 1000); }, [])
        podcastPage.waitForElementPresent('@allEpisodesButton',2000)
        podcastPage.clickOnAllEpisodes();
        podcastCard.waitForElementVisible('@subscribeButton_mob',2000)
        podcastCard.clickOnSubscribeButton_mob();
        podcastCard.waitForElementVisible('@subscribeDropdown',1000)
        podcastCard.getAttribute('@applePodcastList','alt',function(result){
            browser.assert.equal(result.value,browser.globals.applePodcast);
        });
    },

    /**
     * Test case is to validate that user is able to share podcast show on Facebook
     */
    'UN 37: Signed with Facebook::Share podcast' : function sharePodcast_Signed(browser) {
        var header = browser.page.header();
        var podcastPage = browser.page.podcastPage();
        var podcastCard = browser.page.podcastCardPage();
        var podcastNameExpected;
        common_mobile.launchBrowser(browser);
        common_mobile.enableCookies(browser);
        common_mobile.signOutIfSignedIn(browser);
        common_mobile.connectWithFacebook(browser, browser.globals.emailID_Fb, browser.globals.pass_Fb);
        header.clickOnPodcast();
        browser.execute(function() { window.scrollBy(0, 1000); }, [])
        podcastPage
            .waitForElementVisible('@allEpisodesButton',2000)
            .getText('@podcastNameText',function myData(result) {
                var input = result.value;
                podcastNameExpected = input.toLowerCase();
        })
            .clickOnAllEpisodes();
        podcastCard.clickOnShare_facebook()
        browser.perform(function(){
            commonMethods.assertFacebookShareContent(browser,podcastNameExpected);
        })
    },

    // /**
    //  * Test case is to validate that user is able to tweet podcast show
    //  */
    // 'UN 38: Signed with Twitter::Tweet podcast' : function tweetPodcast_Signed(browser) {
    //     var header = browser.page.header();
    //     var podcastPage = browser.page.podcastPage();
    //     var podcastCard = browser.page.podcastCardPage();
    //     common_mobile.launchBrowser(browser);
    //     common_mobile.enableCookies(browser);
    //     common_mobile.signOutIfSignedIn(browser);
    //     common_mobile.connectWithTwitter(browser, browser.globals.email_twitter, browser.globals.pass_twitter);
    //     header.clickOnPodcast();
    //     browser.execute(function() { window.scrollBy(0, 1000); }, [])
    //     podcastPage
    //         .waitForElementVisible('@allEpisodesButton',2000)
    //         .clickOnAllEpisodes();
    //     podcastCard
    //         .waitForElementPresent('@tweetButton',2000)
    //         .clickOnTweetButton()
    //     commonMethods.assertTwitterTweetContent(browser);
    // },

    /**
     * Test case is to validate that user is able to pperform following action on Sticky player:
     * Play
     * Pause
     * Volume On
     * Volume Off
     */
    'UN 39: Sticky Player': function stickyPlayer(browser) {
        var header = browser.page.header();
        var podcastPage = browser.page.podcastPage();
        var play_Sticky, pause_Sticky, volumeOn_Sticky, volumeOff_Sticky;
        common_mobile.launchBrowser(browser);
        common_mobile.enableCookies(browser);
        header.clickOnPodcast();
        podcastPage.waitForElementVisible('@headingText',2000);
        podcastPage.playPodcast();
        browser.pause(8000);
        podcastPage.getAttribute('@stickyPlayerState','xlink:href',function(result){
            pause_Sticky = result.value;
        });
        browser.perform(function(){
            browser.assert.equal(pause_Sticky,browser.globals.pauseSticky)
        });
        podcastPage.clickOnPlayPause_Sticky();
        podcastPage.getAttribute('@stickyPlayerState','xlink:href',function(result){
            play_Sticky = result.value;
        });
        browser.perform(function(){
            browser.assert.equal(play_Sticky,browser.globals.playSticky)
        });
        podcastPage.getAttribute('@stickyPlayerVolumeState','xlink:href',function(result){
            volumeOn_Sticky = result.value;
        });
        browser.perform(function(){
            browser.assert.equal(volumeOn_Sticky,browser.globals.volumeOnSticky)
        });
        podcastPage.clickOnVolume_Sticky();
        podcastPage.getAttribute('@stickyPlayerVolumeState','xlink:href',function(result){
            volumeOff_Sticky = result.value;
        });
        browser.perform(function(){
            browser.assert.equal(volumeOff_Sticky,browser.globals.volumeOffSticky)
        });
    }

}