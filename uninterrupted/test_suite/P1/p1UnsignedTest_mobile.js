var conf = require('../../../nightwatch.conf.js');
var commonMethods= require('../../common/commonMethods');
var common_mobile= require('../../common/common_mobile');

module.exports = {

    /**
     * Test case is to validate that user gets facebook login pop up while sharing video 
     * if he is not logged in with facebook
     */
    'UN19: Unsigned::Share Video On Facebook' : function shareVideo_Unsigned(browser) {
        var homepage = browser.page.homepage();
        var videoPage = browser.page.videoPage();
        var facebookLogin = browser.page.facebookLoginPage();
        common_mobile.launchBrowser(browser);
        common_mobile.enableCookies(browser);
        common_mobile.signOutIfSignedIn(browser);
        homepage
            .waitForElementPresent('@firstCarouselSlide',2000)
            .clcikOnFirstCarouselBar()
            .clickOnFirstWatch();
        browser.pause(2000);
        videoPage.clickOnFacebookShareButton_mob();
        commonMethods.assertFacebookLoginPopup(browser);
    },

    /**
     * Test case is to validate that user gets twitter login pop up while twitting video 
     * if he is not logged in with twitter
     */
    'UN20: Unsigned::Tweet Video' : function tweetVideo_unsigned(browser) {
        var homepage = browser.page.homepage();
        var videoPage = browser.page.videoPage();
        var twitterLogin = browser.page.twitterLoginPage();
        common_mobile.launchBrowser(browser);
        common_mobile.enableCookies(browser);
        common_mobile.signOutIfSignedIn(browser);
        homepage
            .waitForElementPresent('@firstCarouselSlide',2000)
            .clcikOnFirstCarouselBar()
            .clickOnFirstWatch();
        browser.pause(2000);
        videoPage.clickOnTweetButton_mob();
        commonMethods.assertTwitterLoginPopup(browser);
    },

    /**
     * Test case is to validate that user gets facebook login pop up while sharing podcast episode 
     * if he is not logged in with facebook
     */
    'UN 30: Unsigned::Share podcast Episode On Facebook' : function sharePodcastEpisode_Unsigned(browser) {
        var header = browser.page.header();
        var videoPage = browser.page.videoPage();
        var facebookLogin = browser.page.facebookLoginPage();
        var podcastPage = browser.page.podcastPage();
        var podcastCard = browser.page.podcastCardPage();
        common_mobile.launchBrowser(browser);
        common_mobile.enableCookies(browser);
        common_mobile.signOutIfSignedIn(browser);
        header.clickOnPodcast();
        browser.execute(function() { window.scrollBy(0, 1000); }, [])
        podcastPage.waitForElementPresent('@allEpisodesButton',2000)
            .clickOnAllEpisodes();
        browser.pause(1000);
        podcastCard.waitForElementPresent('@facebookIcon_first',2000);
        podcastCard.clickOnfacebook_first()
        commonMethods.assertFacebookLoginPopup(browser);
    },

    /**
     * Test case is to validate that user gets twitter login pop up while twitting podcast episode 
     * if he is not logged in with twitter
     */
    'UN 31: Unsigned::Tweet Podcast Episode' : function tweetPodcastEpisode_Unsigned(browser) {
        var header = browser.page.header();
        var twitterLogin = browser.page.twitterLoginPage();
        var podcastPage = browser.page.podcastPage();
        var podcastCard = browser.page.podcastCardPage();
        common_mobile.launchBrowser(browser);
        common_mobile.enableCookies(browser);
        common_mobile.signOutIfSignedIn(browser);
        header.clickOnPodcast();
        browser.execute(function() { window.scrollBy(0, 1000); }, [])
        podcastPage.waitForElementPresent('@allEpisodesButton',2000)
            .clickOnAllEpisodes();
        browser.pause(500);
        podcastCard.waitForElementVisible('@twitterIcon_first',2000)
        podcastCard.clickOntwitter_first();
        commonMethods.assertTwitterLoginPopup(browser);
    },

    /**
     * Test case is to validate that user gets facebook login pop up while sharing podcast show 
     * if he is not logged in with facebook
     */
    'UN 35: Unsigned::Share podcast On Facebook' : function sharePodcast_Unsigned(browser) {
        var header = browser.page.header();
        var videoPage = browser.page.videoPage();
        var facebookLogin = browser.page.facebookLoginPage();
        var podcastPage = browser.page.podcastPage();
        var podcastCard = browser.page.podcastCardPage();
        common_mobile.launchBrowser(browser);
        common_mobile.enableCookies(browser);
        common_mobile.signOutIfSignedIn(browser);
        header.clickOnPodcast();
        browser.execute(function() { window.scrollBy(0, 1000); }, [])
        podcastPage.waitForElementPresent('@allEpisodesButton',2000)
            .clickOnAllEpisodes();
        podcastCard
            .waitForElementPresent('@share_facebook',2000)
            .clickOnShare_facebook()
        commonMethods.assertFacebookLoginPopup(browser);
    },

    /**
     * Test case is to validate that user gets twitter login pop up while twitting podcast show 
     * if he is not logged in with twitter
     */
    'UN 36: Unsigned::Tweet podcast' : function tweetPodcast_Unsigned(browser) {
        var header = browser.page.header();
        var twitterLogin = browser.page.twitterLoginPage();
        var podcastPage = browser.page.podcastPage();
        var podcastCard = browser.page.podcastCardPage();
        common_mobile.launchBrowser(browser);
        common_mobile.enableCookies(browser);
        common_mobile.signOutIfSignedIn(browser);
        header.clickOnPodcast();
        browser.execute(function() { window.scrollBy(0, 1000); }, [])
        podcastPage.waitForElementPresent('@allEpisodesButton',2000)
            .clickOnAllEpisodes();
        podcastCard
            .waitForElementPresent('@tweetButton',2000)
            .clickOnTweetButton()
        commonMethods.assertTwitterLoginPopup(browser);
    }
}