var conf = require('../../../nightwatch.conf.js');
var commonMethods= require('../../common/commonMethods');
var common_mobile= require('../../common/common_mobile');

module.exports = {

    /**
     * Test case is to validate that podcast text color changes
     * when mouse hover over the text
     */
    'UN01: Header: Podcasts Text Color' : function color_Podcast(browser) {
        var header = browser.page.header();
        common_mobile.launchBrowser(browser);
        header.moveToElement('@podcastLink',0,0,function(){
            header.getCssProperty('@podcastLink','color',function(color){
                browser.assert.equal(color.value,browser.globals.redColor);
            });
        });
    },

    /**
     * Test case is to validate that originals text color changes
     * when mouse hover over the text
     */
    'UN03: Header: Originals Text Color' : function color_Podcast(browser) {
        var header = browser.page.header();
        common_mobile.launchBrowser(browser);
        header.moveToElement('@originalsLink',0,0,function(){
            header.getCssProperty('@originalsLink','color',function(color){
                browser.assert.equal(color.value,browser.globals.redColor);
            });
        });
    },

    /**
     * Test case is to validate that search text color changes
     * when mouse hover over the text
     */
    'UN04: Header: Search Text Color' : function color_Podcast(browser) {
        var header = browser.page.header();
        common_mobile.launchBrowser(browser);
        header.moveToElement('@searchLink',0,0,function(){
            header.getCssProperty('@searchLink','color',function(color){
                browser.assert.equal(color.value,browser.globals.redColor);
            });
        });
    },

    /**
     * Test case is to validate that carousel bar is sync with the 
     * caraousel span
     */
    'UN08 Bar sync with Span' : function(browser) {
        var homePage = browser.page.homepage();
        common_mobile.launchBrowser(browser);
        commonMethods.enableCookies(browser);
        homePage
            .clcikOnSecCarouselBar();
        homePage.getAttribute('@carouselSpan','style',function(result){
            browser.assert.equal(result.value,browser.globals.style_SecCarousel)
        })
    },

    /**
     * Test case is to validate that carousel bar is sync with the 
     * video played from the carousel.
     */
    'UN10 Bar sync with Video Played' : function(browser) {
        var homePage = browser.page.homepage();
        var videoPage = browser.page.videoPage();
        var videoNameExpected, videoNameActual
        common_mobile.launchBrowser(browser);
        commonMethods.enableCookies(browser);
        homePage
            .clcikOnSecCarouselBar()
        homePage.waitForElementVisible('@secVideoName_Carousel',2000)
        homePage.getText('@secVideoName_Carousel',function(result){
                var input = result.value;
                videoNameExpected = input.toLowerCase();
        })
            .clickOnSecWatch();
        browser.pause(3000);
        videoPage.getText('@playedVideoName',function (result) {
            var input= result.value;
            videoNameActual = input.toLowerCase();
        });
        browser.perform(function () {
            browser.assert.equal(videoNameActual,videoNameExpected)
        });
    },

    /**
     * Test case is to validate that play icon of container 1 changes 
     * when mouse hover over the play icon
     */
    'UN11 Container 1: Play Icon Color': function(browser){
        var homePage = browser.page.homepage();
        common_mobile.launchBrowser(browser);
        commonMethods.enableCookies(browser);
        browser.execute(function() { window.scrollBy(0, 500); }, [])
        homePage.moveToElement('@playVideo_firstContainer',0,0,function(){
            homePage.getCssProperty('@playIcon_firstContainer','background-color',function(color){
                browser.assert.equal(color.value,browser.globals.playIconBGColor);
            });
        })
    },

    /**
     * Test case is to validate that user is able to add tag of container 1 to the feed
     */
    'UN 13 Signed:Container 1: Add tag to feed': function(browser){
        var homePage = browser.page.homepage();
        var header = browser.page.header();
        var selectTagClass = "src-components-card-label-__CardLabelAnimated_tag_1ssof    ";
        common_mobile.launchBrowser(browser);
        common_mobile.signOutIfSignedIn(browser);
        commonMethods.enableCookies(browser);
        common_mobile.connectWithTwitter(browser, browser.globals.email_twitter, browser.globals.pass_twitter);
        browser.pause(3000)
        header.clickOnHomePageLink()
        browser.execute(function() { window.scrollBy(0, 500); }, [])
        homePage.getAttribute('@tag_firstContainer','class',function(result){
            if (selectTagClass != result.value){
                homePage.clickOnAddTag_firstContainer();
            }
        })
        homePage.clickOnAddTag_firstContainer();
        homePage.getAttribute('@tag_firstContainer','class',function(result){
            browser.assert.notEqual(selectTagClass,result.value)
        })
    },

    /**
     * Test case is to validate that user is able to remove the added 
     * tag of container 1 to the feed.
     */
    'UN 14 Signed:Container 1: Remove tag from feed': function(browser){
        var homePage = browser.page.homepage();
        var header = browser.page.header();
        var selectTagClass = "src-components-card-label-__CardLabelAnimated_tag_1ssof    ";
        common_mobile.launchBrowser(browser);
        common_mobile.signOutIfSignedIn(browser);
        common_mobile.connectWithTwitter(browser, browser.globals.email_twitter, browser.globals.pass_twitter);
        browser.pause(2000)
        header.clickOnHomePageLink()
        browser.execute(function() { window.scrollBy(0, 500); }, [])
        homePage.getAttribute('@tag_firstContainer','class',function(result){
            if (selectTagClass == result.value){
                homePage.clickOnAddTag_firstContainer();
            }
        })
        homePage.clickOnAddTag_firstContainer();
        homePage.getAttribute('@tag_firstContainer','class',function(result){
            browser.assert.equal(selectTagClass,result.value)
        })
    },

    /**
     * Test case is to validate that user is able to play video by clicking on 
     * watch preview of Originals container.
     */
    'UN 15 Originals Container: Watch Preview': function(browser){
        var homePage = browser.page.homepage();
        var videoNameExpected;
        var videoprename = "Trailer | ";
        common_mobile.launchBrowser(browser);
        browser.execute(function() { window.scrollBy(0, 500); }, [])
        homePage.getText('@heading_Originals',function(result){
            videoNameExpected = result.value;
        })
        homePage.clickOnWatchPreview();
        browser.pause(7500,function(){
            common_mobile.assertVideoPlayedName(browser,videoprename+videoNameExpected.toUpperCase());
        })
    },

    /**
     * Test case is to validate background color of More Originals changes 
     * when mouse hover over the More Originals text.
     */
    'UN17 Original Container : More Originals Color': function(browser){
        var homePage = browser.page.homepage();
        common_mobile.launchBrowser(browser);
        commonMethods.enableCookies(browser);
        browser.execute(function() { window.scrollBy(0, 1000); }, [])
        homePage.moveToElement('@moreOriginals',0,0,function(){
            homePage.getCssProperty('@moreOriginals','background-color',function(color){
                browser.assert.equal(color.value,browser.globals.redBGColor);
            });
        })
    },

    /**
     * Test case is to validate that user is able to click on right arrow in 
     * originals container to view next originals
     */
    'UN18 Original Container : View next originals': function(browser){
        var homePage = browser.page.homepage();
        common_mobile.launchBrowser(browser);
        commonMethods.enableCookies(browser);
        browser.execute(function() { window.scrollBy(0, 1000); }, [])
        homePage.assert.elementNotPresent('@leftArrow_originals')
        homePage.clickOnRightArrow_originals();
        browser.pause(2000)
        homePage.assert.elementPresent('@leftArrow_originals')
    },

    /**
     * Test case is to validate that user is able to click on left arrow in 
     * originals container to view previous originals
     */
    'UN19 Original Container : View previous originals': function(browser){
        var homePage = browser.page.homepage();
        common_mobile.launchBrowser(browser);
        commonMethods.enableCookies(browser);
        browser.execute(function() { window.scrollBy(0, 1000); }, [])
        homePage.clickOnRightArrow_originals();
        browser.pause(2000);
        homePage.assert.elementPresent('@leftArrow_originals')
        homePage.clickOnLeftArrow_originals();
        browser.pause(2000);
        homePage.assert.elementNotPresent('@leftArrow_originals')
    },

    /**
     * Test case is to validate background color of left arrow in originals container
     * changes when mouse hover over left arrow.
     */
    'UN20 Original Container : left arrow color': function(browser){
        var homePage = browser.page.homepage();
        common_mobile.launchBrowser(browser);
        commonMethods.enableCookies(browser);
        browser.execute(function() { window.scrollBy(0, 1500); }, [])
        browser.pause(1500);
        homePage.clickOnRightArrow_originals()
            .waitForElementVisible('@leftArrow_originals',1500)
            .moveToElement('@leftArrow_originals',1,1,function(){
            homePage.getCssProperty('@leftArrow_originals','background-color',function(color){
                browser.assert.equal(color.value,browser.globals.bgColor_arrow);
            });
        })
    },

    /**
     * Test case is to validate background color of right arrow in originals container
     * changes when mouse hover over right arrow.
     */
    'UN21 Original Container : right arrow color': function(browser){
        var homePage = browser.page.homepage();
        common_mobile.launchBrowser(browser);
        commonMethods.enableCookies(browser);
        browser.execute(function() { window.scrollBy(0, 1500); }, [])
        browser.pause(1500);
        homePage.moveToElement('@rightArrow_originals',0,0,function(){
            homePage.getCssProperty('@rightArrow_originals','background-color',function(color){
                browser.assert.equal(color.value,browser.globals.bgColor_arrow);
            });
        });
    },

    /**
     * Test case is to validate that user is able to play podcast from
     * Podcast container.
     */
    'UN22 Podcast Container : play podcast': function(browser){
        var homePage = browser.page.homepage();
        common_mobile.launchBrowser(browser);
        commonMethods.enableCookies(browser);
        browser.execute(function() { window.scrollBy(0, 3000); }, [])
        browser.pause(3000);
        homePage.playPodcast_first();
        browser.pause(6000);
        homePage.assertStickyPlayerPresent()
            .assertPauseIconPresent_first()
            .assertPodcastBgColor(browser.globals.podcastBGColor);
    },

    /**
     * Test case is to validate headphone color of Podcast in Podcast container changes
     * when mouse hover over the headphone.
     */
    'UN23 Podcast Container : Headphone color': function(browser){
        var homePage = browser.page.homepage();
        common_mobile.launchBrowser(browser);
        commonMethods.enableCookies(browser);
        browser.execute(function() { window.scrollBy(0, 2000); }, [])
        browser.pause(1000);
        homePage.moveToElement('@headphone_first',0,0,function(){
            homePage.assertHeadphoneColor(browser.globals.playIconBGColor);
        });
    },

    /**
     * Test case is to validate All Episode background color of Podcast in Podcast container
     * changes when mouse hover over the All Episode text.
     */
    'UN24 Podcast Container : All Episodes Color': function(browser){
        var homePage = browser.page.homepage();
        common_mobile.launchBrowser(browser);
        commonMethods.enableCookies(browser);
        browser.execute(function() { window.scrollBy(0, 2000); }, [])
        browser.pause(1000);
        homePage.moveToElement('@allEpisodes_podcast',0,0,function(){
            homePage.assertAllEpisodesBgColor(browser.globals.redBGColor)
        });
    },

    /**
     * Test case is to validate More Podcasts background color of Podcast in Podcast container
     * changes when mouse hover over the More Podcasts text.
     */
    'UN25 Podcast Container : More Podcasts color': function(browser){
        var homePage = browser.page.homepage();
        common_mobile.launchBrowser(browser);
        commonMethods.enableCookies(browser);
        browser.execute(function() { window.scrollBy(0, 1000); }, [])
        browser.execute(function() { window.scrollBy(0, 2000); }, [])
        browser.pause(1000);
        homePage.moveToElement('@morePodcasts',0,0,function(){
            homePage.assertMorePodcastsColor(browser.globals.redBGColor);
        });
    },

    /**
     * Test case is to validate that user is able to switch podcast while playing one.
     */
    'UN26 Podcast Container : Switch Podcast': function(browser){
        var homePage = browser.page.homepage();
        common_mobile.launchBrowser(browser);
        commonMethods.enableCookies(browser);
        browser.execute(function() { window.scrollBy(0, 1500); }, [])
        browser.pause(1000);
        browser.execute(function() { window.scrollBy(0, 1500); }, [])
        browser.pause(1000);
        homePage.playPodcast_first();
        browser.pause(5000);
        homePage.assertPauseIconPresent_first()
            homePage.playPodcast_sec()
        browser.pause(5000);
        homePage.assertPauseIconPresent_sec()
    },

    /**
     * Test case is to validate that podcast is sync in homepage and podcast page
     */
    'UN27 Podcast Container: Homepage Podcast and Podcast Page': function(browser){
        var homePage = browser.page.homepage();
        var header = browser.page.header();
        var podcastLocator;
        common_mobile.launchBrowser(browser);
        commonMethods.enableCookies(browser);
        browser.execute(function() { window.scrollBy(0, 1500); }, [])
        browser.pause(3000);
        browser.execute(function() { window.scrollBy(0, 1500); }, [])
        browser.pause(3000);
        homePage.getText('@podcastDetails_first',function(result){
            input = result.value
            podcastDetail = input.slice(0,40);
            podcastLocator = "(//*[contains(text(),'"+podcastDetail+"')])[1]"
        })
        homePage.playPodcast_first();
        browser.pause(6000);
        homePage.assertStickyPlayerPresent()
            .assertPauseIconPresent_first()
            .assertPodcastBgColor(browser.globals.podcastBGColor);
        browser.execute(function() { window.scrollBy(0, -3000); }, [])
        header.clickOnPodcast();
        browser.execute(function() { window.scrollBy(0, 500); }, [])
        browser.pause(2000);
        browser.perform(function(){
            browser.useXpath().assert.cssProperty(podcastLocator,'background-color',browser.globals.bgColor_podcastDetails);
        })
    },

    /**
     * Test case is to validate that sticky player and podcast card progress bar is in sync.
     */
    'UN28 Podcast Container: Sticky and Card progress bar': function(browser){
        var homePage = browser.page.homepage();
        var header = browser.page.header();
        var style_sticky, style_card;
        common_mobile.launchBrowser(browser);
        commonMethods.enableCookies(browser);
        browser.execute(function() { window.scrollBy(0, 1500); }, [])
        browser.pause(1000);
        browser.execute(function() { window.scrollBy(0, 1500); }, [])
        browser.pause(1000);
        homePage.playPodcast_first();
        browser.pause(5000);
        homePage.moveToElement('@progressBar_sticky',10,0,function(){
            homePage.clickonProgressbar();
        }).clickOnPlayPause_Sticky();
        browser.perform(function(){
            homePage.getAttribute('@progressbarWidth_sticky','style',function(result){
                style_sticky = result.value;
            })
            homePage.getAttribute('@progressbarWidth_card','style',function(result){
                style_card = result.value;
            })
        })
        browser.perform(function(){
            browser.assert.equal(style_sticky,style_card)
        })
    },

    /**
     * Test case is to validate that user is able to add tag of Latest Stories Container to the feed
     */
    'UN 30 Latest Stories Container: Add tag to feed': function(browser){
        var homePage = browser.page.homepage();
        var header = browser.page.header();
        var selectTagClass = "src-components-card-label-__CardLabelAnimated_tag_1ssof    ";
        common_mobile.launchBrowser(browser);
        common_mobile.signOutIfSignedIn(browser);
        commonMethods.enableCookies(browser);
        common_mobile.connectWithTwitter(browser, browser.globals.email_twitter, browser.globals.pass_twitter);
        browser.pause(2000)
        header.clickOnHomePageLink()
        browser.execute(function() { window.scrollBy(0, 1500); }, [])
        browser.pause(1000)
        browser.execute(function() { window.scrollBy(0, 1500); }, [])
        homePage.getAttribute('@tag_latestStories','class',function(result){
            if (selectTagClass != result.value){
                homePage.clickOnTag_latestStories();
            }
        })
        homePage.clickOnTag_latestStories();
        homePage.getAttribute('@tag_latestStories','class',function(result){
            browser.assert.notEqual(selectTagClass,result.value)
        })
    },

    /**
     * Test case is to validate that user is able to remove tag of Latest Stories Container from the feed
     */
    'UN 31 Latest Stories Container: Remove tag from feed': function(browser){
        var homePage = browser.page.homepage();
        var header = browser.page.header();
        var selectTagClass = "src-components-card-label-__CardLabelAnimated_tag_1ssof    ";
        common_mobile.launchBrowser(browser);
        common_mobile.signOutIfSignedIn(browser);
        commonMethods.enableCookies(browser);
        common_mobile.connectWithTwitter(browser, browser.globals.email_twitter, browser.globals.pass_twitter);
        browser.pause(1000)
        header.clickOnHomePageLink()
        browser.execute(function() { window.scrollBy(0, 1500); }, [])
        browser.pause(500)
        browser.execute(function() { window.scrollBy(0, 1500); }, [])
        homePage.getAttribute('@tag_latestStories','class',function(result){
            if (selectTagClass == result.value){
                homePage.clickOnTag_latestStories();
            }
        })
        homePage.clickOnTag_latestStories();
        homePage.getAttribute('@tag_latestStories','class',function(result){
            browser.assert.equal(selectTagClass,result.value)
        })
    },

    /**
     * Test case is to validate that user is able to click on Load More and card count increases
     */
    'UN32 Latest Stories Container: Load More card count': function(browser){
        var homePage = browser.page.homepage();
        var cardLocator = "//div[@class = 'src-components-video-card-__VideoCard_host_3R5zG src-components-video-items-grid-__VideoItemsGrid_item_oCMrX']"
        var totalCardBfrLoadMore,totalCardAfterLoadMore;
        common_mobile.launchBrowser(browser);
        commonMethods.enableCookies(browser);
        browser.execute(function() { window.scrollBy(0, 2000); }, [])
        browser.elements('xpath',cardLocator,function(result){
            totalCardBfrLoadMore = result.value.length
        })
        homePage.clickOnLoadMore_latestStories();
        browser.pause(2000)
        browser.elements('xpath',cardLocator,function(result){
            totalCardAfterLoadMore = result.value.length
        })
        browser.perform(function(){
            browser.assert.ok(totalCardAfterLoadMore >= totalCardBfrLoadMore);
        })
    },

    /**
     * Test case is to validate that background color of Load More in Latest Stories container
     * changes when mouse hover over the Load More
     */
    'UN33 Latest Stories Container: Load More Color': function(browser){
        var homePage = browser.page.homepage();
        common_mobile.launchBrowser(browser);
        commonMethods.enableCookies(browser);
        browser.execute(function() { window.scrollBy(0, 2000); }, [])
        homePage.moveToElement('@loadMore_latestStories',0,0,function(){
            homePage.getCssProperty('@loadMore_latestStories','background-color',function(color){
                browser.assert.equal(color.value,browser.globals.redBGColor);
            });
        })
    },

    /**
     * Test case is to validate that play and pause button of sticky player 
     * is sync with podcast card in Podcast page.
     */
    'UN34 Podcast Page: Link Podcast From Sticky Player': function(browser){
        var header = browser.page.header();
        var podcastPage = browser.page.podcastPage();
        var podcastCard = browser.page.podcastCardPage();
        var classOnSticky, class_Headphn;
        common_mobile.launchBrowser(browser);
        commonMethods.enableCookies(browser);
        header.clickOnPodcast();
        browser.pause(1000);
        podcastPage.playPodcast();
        browser.pause(5000);
        podcastPage.getAttribute('@cardPlayerState','xlink:href',function(result){
            class_Headphn = result.value;
        });
        podcastCard.getAttribute('@stickyPlayerState','xlink:href',function(result){
            classOnSticky = result.value;
        });
        browser.perform(function(){
            browser.assert.equal(class_Headphn,classOnSticky)
        });
        podcastPage.clickOnPlayPause_Sticky();
        podcastPage.getAttribute('@cardPlayerState','xlink:href',function(result){
            class_Headphn = result.value;
        });
        podcastCard.getAttribute('@stickyPlayerState','xlink:href',function(result){
            classOnSticky = result.value;
        });
        browser.perform(function(){
            browser.assert.equal(class_Headphn,classOnSticky)
        });
    },

    /**
     * Test case is to validate that user is able to switch podcast while playing.
     */
    'UN 35 Podcast Page : Switch Podcast': function(browser){
        var header = browser.page.header();
        var podcastPage = browser.page.podcastPage();
        common_mobile.launchBrowser(browser);
        commonMethods.enableCookies(browser);
        header.clickOnPodcast();
        browser.pause(3000);
        podcastPage.playPodcast();
        browser.pause(6000);
        podcastPage.assertPauseIconPresent_first()
        browser.pause(500,function(){
            browser.execute(function() { window.scrollBy(0, 1000); }, [])
            podcastPage.playPodcast_sec()
            browser.pause(6000);
            podcastPage.assertPauseIconPresent_sec(browser.globals.pause_className)
        })
    },

    /**
     * Test case is to validate All Episode background color of Podcast in Podcast page
     * changes when mouse hover over the All Episode text.
     */
    'UN 36 Podcast Page : All Episodes color': function(browser){
        var header = browser.page.header();
        var podcastPage = browser.page.podcastPage();
        common_mobile.launchBrowser(browser);
        commonMethods.enableCookies(browser);
        header.clickOnPodcast();
        browser.pause(1000);
        podcastPage.moveToElement('@allEpisodesButton',0,0,function(){
            podcastPage.assertAllEpisodesBgColor(browser.globals.redBGColor)
        })
    },

    /**
     * Test case is to validate headphone color of Podcast in Podcast container changes
     * when mouse hover over the headphone.
     */
    'UN 37 Podcast Page : Headphone color': function(browser){
        var header = browser.page.header();
        var podcastPage = browser.page.podcastPage();
        common_mobile.launchBrowser(browser);
        commonMethods.enableCookies(browser);
        header.clickOnPodcast();
        podcastPage.waitForElementPresent('@headphoneIcon',2000)
        podcastPage.moveToElement('@headphoneIcon',0,0,function(){
            podcastPage.assertHeadphoneColor(browser.globals.headphoneBGColor);
        });
    },

    /**
     * Test case is to validate that progress bar of sticky player 
     * is sync with podcast card in Podcast page.
     */
    'UN38 Podcast Page: Sticky and Card progress bar': function(browser){
        var homePage = browser.page.homepage();
        var header = browser.page.header();
        var podcastPage = browser.page.podcastPage();
        var style_sticky, style_card;
        common_mobile.launchBrowser(browser);
        commonMethods.enableCookies(browser);
        header.clickOnPodcast();
        browser.pause(1000);
        podcastPage.playPodcast();
        browser.pause(5000);
        homePage.moveToElement('@progressBar_sticky',10,0,function(){
            homePage.clickonProgressbar();
        }).clickOnPlayPause_Sticky();
        browser.perform(function(){
            homePage.getAttribute('@progressbarWidth_sticky','style',function(result){
                style_sticky = result.value;
            })
            podcastPage.getAttribute('@progressbarWidth_card','style',function(result){
                style_card = result.value;
            })
        })
        browser.perform(function(){
            browser.assert.equal(style_sticky,style_card)
        })      
    },

    /**
     * Test case is to validate headphone color of Podcast in Podcast container changes
     * when mouse hover over the headphone.
     */
    'UN39 Podcast Page : Headphone color': function(browser){
        var header = browser.page.header();
        var podcastPage = browser.page.podcastPage();
        common_mobile.launchBrowser(browser);
        commonMethods.enableCookies(browser);
        header.clickOnPodcast();
        podcastPage.waitForElementPresent('@headphoneIcon',2000)
        podcastPage.moveToElement('@headphoneIcon',0,0,function(){
            podcastPage.assertHeadphoneColor(browser.globals.headphoneBGColor);
        });
    },

    /**
     * Test case is to validate All Episode background color of Podcast in Podcast page
     * changes when mouse hover over the All Episode text.
     */
    'UN40 Podcast Page : All Episodes Color': function(browser){
        var header = browser.page.header();
        var podcastPage = browser.page.podcastPage();
        common_mobile.launchBrowser(browser);
        commonMethods.enableCookies(browser);
        header.clickOnPodcast();
        podcastPage.waitForElementPresent('@headphoneIcon',2000)
        podcastPage.moveToElement('@allEpisodesButton',0,0,function(){
            podcastPage.assertAllEpisodesBgColor(browser.globals.redBGColor);
        });
    },
    
    /**
     * Test case is to validate that user is able to add show tag to the Feed in Podcast page.
     */
    'UN42 Podcast Page : Add Show tag': function(browser){
        var header = browser.page.header();
        var podcastPage = browser.page.podcastPage();
        var podcastCard = browser.page.podcastCardPage();
        var selectTagClass = "src-components-card-label-__CardLabelAnimated_tag_1ssof    ";
        common_mobile.launchBrowser(browser);
        commonMethods.enableCookies(browser);
        common_mobile.signOutIfSignedIn(browser);
        common_mobile.connectWithTwitter(browser, browser.globals.email_twitter, browser.globals.pass_twitter);
        header.clickOnPodcast();
        podcastPage.waitForElementPresent('@headphoneIcon',2000)
            .clickOnAllEpisodes();
        browser.pause(1000);
        podcastCard.getAttribute('@tag_show','class',function(result){
            if (selectTagClass != result.value){
                podcastCard.clickOnShowTag();
            }
        })
        podcastCard.clickOnShowTag();
        podcastCard.getAttribute('@tag_show','class',function(result){
            browser.assert.notEqual(selectTagClass,result.value)
        })
    },
    
    /**
     * Test case is to validate that user is able to remove show tag from the Feed in Podcast page.
     */
    'UN43 Podcast Page : Remove Show tag': function(browser){
        var header = browser.page.header();
        var podcastPage = browser.page.podcastPage();
        var podcastCard = browser.page.podcastCardPage();
        var selectTagClass = "src-components-card-label-__CardLabelAnimated_tag_1ssof    ";
        common_mobile.launchBrowser(browser);
        commonMethods.enableCookies(browser);
        common_mobile.signOutIfSignedIn(browser);
        common_mobile.connectWithTwitter(browser, browser.globals.email_twitter, browser.globals.pass_twitter);
        header.clickOnPodcast();
        podcastPage.waitForElementPresent('@headphoneIcon',2000)
            .clickOnAllEpisodes();
        browser.pause(1000)
        podcastCard.getAttribute('@tag_show','class',function(result){
            if (selectTagClass == result.value){
                podcastCard.clickOnShowTag();
            }
        })
        podcastCard.clickOnShowTag();
        podcastCard.getAttribute('@tag_show','class',function(result){
            browser.assert.equal(selectTagClass,result.value)
        })
    },

    /**
     * Test case is to validate that user is able to add episode tag to the Feed in Podcast page.
     */
    'UN45 Podcast Page : Add Episode tag': function(browser){
        var header = browser.page.header();
        var podcastPage = browser.page.podcastPage();
        var podcastCard = browser.page.podcastCardPage();
        var selectTagClass = "src-components-card-label-__CardLabelAnimated_tag_1ssof    ";
        common_mobile.launchBrowser(browser);
        commonMethods.enableCookies(browser);
        common_mobile.signOutIfSignedIn(browser);
        common_mobile.connectWithTwitter(browser, browser.globals.email_twitter, browser.globals.pass_twitter);
        header.clickOnPodcast();
        podcastPage.waitForElementPresent('@headphoneIcon',2000)
            .clickOnAllEpisodes();
        browser.pause(1000);
        podcastCard.getAttribute('@tag_episode','class',function(result){
            if (selectTagClass != result.value){
                podcastCard.clickonEpisodeTag();
            }
        })
        podcastCard.clickonEpisodeTag();
        podcastCard.getAttribute('@tag_episode','class',function(result){
            browser.assert.notEqual(selectTagClass,result.value)
        })
    },

    /**
     * Test case is to validate that user is able to remove episode tag from the Feed in Podcast page.
     */
    'UN46 Podcast Page : Remove Episode tag': function(browser){
        var header = browser.page.header();
        var podcastPage = browser.page.podcastPage();
        var podcastCard = browser.page.podcastCardPage();
        var selectTagClass = "src-components-card-label-__CardLabelAnimated_tag_1ssof    ";
        common_mobile.launchBrowser(browser);
        commonMethods.enableCookies(browser);
        common_mobile.signOutIfSignedIn(browser);
        common_mobile.connectWithTwitter(browser, browser.globals.email_twitter, browser.globals.pass_twitter);
        header.clickOnPodcast();
        podcastPage.waitForElementPresent('@headphoneIcon',2000)
            .clickOnAllEpisodes();
        browser.pause(1000);
        podcastCard.getAttribute('@tag_episode','class',function(result){
            if (selectTagClass == result.value){
                podcastCard.clickonEpisodeTag();
            }
        })
        podcastCard.clickonEpisodeTag();
        podcastCard.getAttribute('@tag_episode','class',function(result){
            browser.assert.equal(selectTagClass,result.value)
        })
    },

    /**
     * Test case is to validate that episodes in podcast page is ordered by date.
     */
    'UN47 Podcast Episode : Order by date': function(browser){
        var header = browser.page.header();
        var podcastPage = browser.page.podcastPage();
        var podcastCard = browser.page.podcastCardPage();
        var homepage = browser.page.homepage();
        var dateEpisode_last, dateEpisode_secLast;
        common_mobile.launchBrowser(browser);
        commonMethods.enableCookies(browser);
        header.clickOnPodcast();
        podcastPage.waitForElementPresent('@allEpisodesButton',2000)
            .clickOnAllEpisodes();
        browser.pause(1000);
        podcastCard.getText('@dateEpisode_last',function(result1){
            dateEpisode_last = new Date(result1.value)
        })
            .getText('@dateEpisode_secLast',function(result2){
            dateEpisode_secLast = new Date(result2.value)
        });
        browser.perform(function(){
            browser.assert.ok(dateEpisode_last >= dateEpisode_secLast);
        });
    },

    /**
     * Test case is to validate that 1st episode on the podcast page should be same as the total episode.
     */
    'UN48 Podcast Episode : Episode Number': function(browser){
        var header = browser.page.header();
        var podcastPage = browser.page.podcastPage();
        var podcastCard = browser.page.podcastCardPage();
        var homepage = browser.page.homepage();
        var noOfEpisode, firstEpisode;
        common_mobile.launchBrowser(browser);
        commonMethods.enableCookies(browser);
        header.clickOnPodcast();
        podcastPage.waitForElementPresent('@allEpisodesButton',2000)
            .clickOnAllEpisodes();
        browser.pause(1000);
        podcastCard.getText('@totalNoOfEpisode',function(result1){
                noOfEpisode = result1.value.split(" ",1);
        })
            .getText('@firstEpisodeOnPage',function(result2){
                len = (result2.value).length
                firstEpisode = result2.value.slice(8,len);
        });
        browser.perform(function(){
            browser.assert.equal(noOfEpisode,firstEpisode);
        });
    },

    /**
     * Test case is to validate that Progress bar of episode thumbnail and sticky player is in sync
     */
    'UN49 Podcast Episode : Sticky and Episode progress bar': function(browser){
        var header = browser.page.header();
        var podcastPage = browser.page.podcastPage();
        var podcastCard = browser.page.podcastCardPage();
        var homePage = browser.page.homepage();
        var style_sticky, style_Episode;
        common_mobile.launchBrowser(browser);
        commonMethods.enableCookies(browser);
        header.clickOnPodcast();
        podcastPage.waitForElementPresent('@allEpisodesButton',2000)
            .clickOnAllEpisodes();
        browser.pause(1000);
        podcastCard
            .clickOnHeadphone_first();
        browser.pause(5000);
        homePage.moveToElement('@progressBar_sticky',10,0,function(){
            homePage.clickonProgressbar();
        }).clickOnPlayPause_Sticky();
        browser.perform(function(){
            homePage.getAttribute('@progressbarWidth_sticky','style',function(result){
                style_sticky = result.value;
            })
            podcastCard.getAttribute('@progressBarEpisode','style',function(result){
                style_Episode = result.value;
            })
        })
        browser.perform(function(){
            browser.assert.equal(style_sticky,style_Episode)
        })
    },

    /**
     * Test case is to validate that Check user is able to view next episodes of a podcast
     * on clicking "Load More" if total episodes are greater than or equals to 30
     */
    'UN51 Podcast Episode : No of Episodes': function(browser){
        var header = browser.page.header();
        var podcastPage = browser.page.podcastPage();
        var podcastCard = browser.page.podcastCardPage();
        var noOfEpisode;
        common_mobile.launchBrowser(browser);
        commonMethods.enableCookies(browser);
        header.clickOnPodcast();
        podcastPage.waitForElementPresent('@allEpisodesButton',2000)
            .clickOnAllEpisodes_Sec();
        browser.pause(2000)
        podcastCard.getText('@totalNoOfEpisode',function(result){
            noOfEpisode = (result.value).split(" ",1);
        })
        browser.perform(function(){
            if (parseInt(noOfEpisode)>= 20){
                browser.execute(function() { window.scrollBy(0, 2000); }, [])
                podcastCard.clickOnLoadMore();
                browser.execute(function() { window.scrollBy(0, 500); }, [])
            }
            browser.perform(function(){
                if (parseInt(noOfEpisode)< 30){
                    var episodeLocate = "(//p[@class = 'src-components-podcast-episode-list-__PodcastEpisode_meta__title_2-eH5'])["+noOfEpisode+"]"
                    browser.useXpath().getText(episodeLocate,function(result){
                        len = (result.value).length
                        episode = (result.value).slice(8,len);
                        browser.assert.equal(1,episode)
                    })
                } else {
                    podcastCard.getText('@lastEpisodeOnPage2',function(result){
                        len = (result.value).length
                        episode = (result.value).slice(8,len)
                        browser.assert.equal(parseInt(noOfEpisode)-29,parseInt(episode))
                    })
                }
            })
        })
    },

    /**
     * Test case is to validate that twitter icon in podcast card are highlighted when 
     * hovering or clicking on it
     */
    'UN52 Podcast Card Page : Twitter Icon Color': function(browser){
        var header = browser.page.header();
        var podcastPage = browser.page.podcastPage();
        var podcastCard = browser.page.podcastCardPage();
        common_mobile.launchBrowser(browser);
        commonMethods.enableCookies(browser);
        header.clickOnPodcast();
        podcastPage.waitForElementPresent('@allEpisodesButton',2000)
            .clickOnAllEpisodes();
        browser.pause(1000);
        browser.execute(function() { window.scrollBy(0, 500); }, [])
        podcastCard.moveToElement('@twitterIcon_first',0,0,function(){
            browser.pause(3000)
            podcastCard.assertTwitterIcon_firstColor(browser.globals.FBTwitterIconFill);
        });
    },

    /**
     * Test case is to validate that facebook icon in podcast card are highlighted when 
     * hovering or clicking on it
     */
    'UN53 Podcast Card Page : Facebook Icon Color': function(browser){
        var header = browser.page.header();
        var podcastPage = browser.page.podcastPage();
        var podcastCard = browser.page.podcastCardPage();
        common_mobile.launchBrowser(browser);
        commonMethods.enableCookies(browser);
        header.clickOnPodcast();
        podcastPage.waitForElementPresent('@allEpisodesButton',2000)
            .clickOnAllEpisodes();
        browser.pause(1000);
        browser.execute(function() { window.scrollBy(0, 500); }, [])
        podcastCard.moveToElement('@facebookIcon_first',0,0,function(){
            browser.pause(3000)
            podcastCard.assertFacebookIcon_firstColor(browser.globals.FBTwitterIconFill);
        });
    },


    /**
     * Test case is to validate that user is able to navigate to the show on clicking the thumbnail on sticky player.
     */
    'UN60 Sticky Player : Thumbnail': function(browser){
        var homePage = browser.page.homepage();
        var podcastCardPage = browser.page.podcastCardPage();
        var athleteNameExpected
        common_mobile.launchBrowser(browser);
        commonMethods.enableCookies(browser);
        browser.execute(function() { window.scrollBy(0, 2000); }, [])
        browser.pause(3000);
        homePage.getText('@atheleteName_podcast',function(result){
            athleteNameExpected=result.value
        })
        homePage.playPodcast_first();
        browser.pause(5000)
        homePage.assertStickyPlayerPresent()
            .clickOnthumbnail_sticky();
            browser.pause(2000);
        browser.perform(function(){
            podcastCardPage.assertathlete(athleteNameExpected)
        })        
    },


    /**
     * Test case is to validate that if user is able to view show page on clicking any Show Card
     */
    'UN69 Originals : Navigate to Show Page': function(browser){
        var header = browser.page.header();
        var originals = browser.page.originalsPage();
        var originalCard = browser.page.originalCardPage();
        var originalsName;
        common_mobile.launchBrowser(browser);
        commonMethods.enableCookies(browser);
        header.clickOnOriginal();
        browser.pause(2000)
        originals.getText('@originalCardName_first',function(result){
            originalsName=result.value
        })
        originals.clickOnOriginalCard_first();
        browser.perform(function(){
            originalCard.waitForElementVisible('@originalAthlete',2000)
                .assertOriginalAthleteName(originalsName);
        })
    },

    /**
     * Test case is to validate that if user is able to click on "Load More" in originals page.
     */
    'UN70 Originals : Load More': function(browser){
        var header = browser.page.header();
        var originals = browser.page.originalsPage();
        common_mobile.launchBrowser(browser);
        commonMethods.enableCookies(browser);
        header.clickOnOriginal();
        browser.pause(1000);
        originals.clickOnLoadMore()
            .assertlastOriginalCard_page2Present()
    },
    
    /**
     * Test case is to validate that background color of "Load More" in originals page changes on 
     * hovering cursor over the Load More.
     */
    'UN71 Originals : Load More Color': function(browser){
        var header = browser.page.header();
        var originals = browser.page.originalsPage();
        common_mobile.launchBrowser(browser);
        commonMethods.enableCookies(browser);
        header.clickOnOriginal();
        browser.pause(2000);
        originals.moveToElement('@loadMore',0,0,function(){
            browser.pause(1000);
            originals.assertLoadMoreBgColor(browser.globals.redBGColor)
        })
    },

    /**
     * Test case is to validate that if user is able to click on watch Now in Originals page.
     */
    'UN72 Originals Card : Watch Now': function(browser){
        var header = browser.page.header();
        var originals = browser.page.originalsPage();
        var originalCard = browser.page.originalCardPage();
        common_mobile.launchBrowser(browser);
        commonMethods.enableCookies(browser);
        header.clickOnOriginal();
        originals.clickOnOriginalCard_sec();
        browser.pause(2000)
        originalCard.clickOnWatchNow();
        // browser.refresh()
        //     .pause(4000);
        browser.pause(6000);
        browser.execute(commonMethods.getJwPlayerState, function (version) {
            browser.assert.equal(version.value,browser.globals.state_play_jwplayer);
        });
    },

    /**
     * Test case is to validate that color of play icon changes on hovering cursor over the video thumbnail.
     */
    'UN73 Originals Card : Play Icon color': function(browser){
        var header = browser.page.header();
        var originals = browser.page.originalsPage();
        var originalCard = browser.page.originalCardPage();
        common_mobile.launchBrowser(browser);
        commonMethods.enableCookies(browser);
        header.clickOnOriginal();
        originals.clickOnOriginalCard_first();
        browser.pause(1000);
        originalCard
            .moveToElement('@episode_first',0,0,function(){
            originalCard.assertPlayButtonBgColor_first(browser.globals.playIconBGColor)
        })
    },

    /**
     * Test case is to validate that if user is able to play video from the originals episode
     */
    'UN74 Originals Card : Watch Episode': function(browser){
        var header = browser.page.header();
        var originals = browser.page.originalsPage();
        var originalCard = browser.page.originalCardPage();
        var showName;
        common_mobile.launchBrowser(browser);
        commonMethods.enableCookies(browser)
        header.clickOnOriginal();
        originals.clickOnOriginalCard_first();
        browser.execute(function() { window.scrollBy(0, 500); }, [])
        browser.pause(2000)
        originalCard.getText('@episodeName_first',function(result){
            showName = result.value
        })
        originalCard.clickOnPlayIcon_first();
        browser.pause(5000,function(){
            common_mobile.assertVideoPlayedName(browser,showName)
        })
    },

    /**
     * Test case is to validate that no of episode mentioned should match with the count of episode on the page
     */
    'UN75 Originals Card : Episodes Count': function(browser){
        var header = browser.page.header();
        var originals = browser.page.originalsPage();
        var originalCard = browser.page.originalCardPage();
        var totalEpisode, totalCard;
        var cardLocator = "//*[@class = 'icon icon-play']"
        common_mobile.launchBrowser(browser);
        commonMethods.enableCookies(browser)
        header.clickOnOriginal();
        originals.clickOnOriginalCard_first();
        browser.pause(5000)
        originalCard.getText('@totalEpisode',function(result){
            var count = result.value
            totalEpisode = count.split(" ",1)
        })
        browser.perform(function(){
            browser.elements('xpath',cardLocator,function(result){
                totalCard = result.value.length
            })
        })
        browser.perform(function(){
            browser.assert.equal(totalCard,totalEpisode)
        })
    },

    /**
     * Test case is to validate that if user is able to add originals show tag.
     */
    'UN81 Originals Page : Add Show tag': function(browser){
        var header = browser.page.header();
        var originals = browser.page.originalsPage();
        var originalCardPage = browser.page.originalCardPage();
        var selectTagClass = "src-components-card-label-__CardLabelAnimated_tag_1ssof    ";
        common_mobile.launchBrowser(browser);
        commonMethods.enableCookies(browser);
        common_mobile.signOutIfSignedIn(browser);
        common_mobile.connectWithTwitter(browser, browser.globals.email_twitter, browser.globals.pass_twitter);
        header.clickOnOriginal();
        originals.clickOnOriginalCard_first();
        originalCardPage.waitForElementVisible('@tag_show',2000)
        originalCardPage.getAttribute('@tag_show','class',function(result){
            if (selectTagClass != result.value){
                originalCardPage.clickOnShowTag();
            }
        })
        originalCardPage.clickOnShowTag();
        originalCardPage.getAttribute('@tag_show','class',function(result){
            browser.assert.notEqual(selectTagClass,result.value)
        })
    },
    
    /**
     * Test case is to validate that if user is able to remove originals show tag.
     */
    'UN82 Originals Page : Remove Show tag': function(browser){
        var header = browser.page.header();
        var originals = browser.page.originalsPage();
        var originalCardPage = browser.page.originalCardPage();
        var selectTagClass = "src-components-card-label-__CardLabelAnimated_tag_1ssof    ";
        common_mobile.launchBrowser(browser);
        commonMethods.enableCookies(browser);
        common_mobile.signOutIfSignedIn(browser);
        common_mobile.connectWithTwitter(browser, browser.globals.email_twitter, browser.globals.pass_twitter);
        header.clickOnOriginal();
        originals.clickOnOriginalCard_first();
        originalCardPage.getAttribute('@tag_show','class',function(result){
            if (selectTagClass == result.value){
                originalCardPage.clickOnShowTag();
            }
        })
        originalCardPage.clickOnShowTag();
        originalCardPage.getAttribute('@tag_show','class',function(result){
            browser.assert.equal(selectTagClass,result.value)
        })
    },

    /**
     * Test case is to validate that if user is able to add tag from originals episode.
     */
    'UN84 Originals Page : Add Episode tag': function(browser){
        var header = browser.page.header();
        var originals = browser.page.originalsPage();
        var originalCardPage = browser.page.originalCardPage();
        var selectTagClass = "src-components-card-label-__CardLabelAnimated_tag_1ssof    ";
        common_mobile.launchBrowser(browser);
        commonMethods.enableCookies(browser);
        common_mobile.signOutIfSignedIn(browser);
        common_mobile.connectWithTwitter(browser, browser.globals.email_twitter, browser.globals.pass_twitter);
        header.clickOnOriginal();
        originals.clickOnOriginalCard_first();
        browser.pause(1000)
        originalCardPage.getAttribute('@tag_episode','class',function(result){
            if (selectTagClass != result.value){
                originalCardPage.clickOnEpisodeTag_first();
            }
        })
        originalCardPage.clickOnEpisodeTag_first();
        originalCardPage.getAttribute('@tag_episode','class',function(result){
            browser.assert.notEqual(selectTagClass,result.value)
        })
    },
    
    /**
     * Test case is to validate that if user is able to remove tag from originals episode.
     */
    'UN85 Originals Page : Remove Episode tag': function(browser){
        var header = browser.page.header();
        var originals = browser.page.originalsPage();
        var originalCardPage = browser.page.originalCardPage();
        var selectTagClass = "src-components-card-label-__CardLabelAnimated_tag_1ssof    ";
        common_mobile.launchBrowser(browser);
        commonMethods.enableCookies(browser);
        common_mobile.signOutIfSignedIn(browser);
        common_mobile.connectWithTwitter(browser, browser.globals.email_twitter, browser.globals.pass_twitter);
        header.clickOnOriginal();
        originals.clickOnOriginalCard_first();
        browser.pause(1000)
        originalCardPage.getAttribute('@tag_episode','class',function(result){
            if (selectTagClass == result.value){
                originalCardPage.clickOnEpisodeTag_first();
            }
        })
        originalCardPage.clickOnEpisodeTag_first();
        originalCardPage.getAttribute('@tag_episode','class',function(result){
            browser.assert.equal(selectTagClass,result.value)
        })
    },

    /**
     * Test case is to validate that if user is able to click on "More Originals".
     */
    'UN86 Originals Card : More Originals': function(browser){
        var header = browser.page.header();
        var originals = browser.page.originalsPage();
        var originalCard = browser.page.originalCardPage();
        common_mobile.launchBrowser(browser);
        commonMethods.enableCookies(browser);
        header.clickOnOriginal();
        originals.clickOnOriginalCard_first();
        browser.execute(function() { window.scrollBy(0, 2000); }, [])
        originalCard.assertMoreOriginalPresent();
    },

    /**
     * Test case is to validate that if user is able to navigate to Originals by 
     * clicking on "More Originals" Originals card.
     */
    'UN87 More Originals : Navigate to Originals': function(browser){
        var header = browser.page.header();
        var originals = browser.page.originalsPage();
        var originalCard = browser.page.originalCardPage();
        var atheleteName;
        common_mobile.launchBrowser(browser);
        commonMethods.enableCookies(browser);
        header.clickOnOriginal();
        originals.clickOnOriginalCard_first();
        browser.execute(function() { window.scrollBy(0, 2000); }, [])
        browser.pause(1500)
        originalCard.getText('@originalAthlete_moreOriginals',function(result){
            atheleteName = result.value
        }).clickOnOriginalCard_moreOriginals();
        browser.pause(1000)
        browser.perform(function(){
            originalCard.assertOriginalAthleteName(atheleteName);
        })
    },

    /**
     * Test case is to validate that if user is able to rewind video.
     */
    'UN88 Video Page : Rewind': function(browser){
        var homePage = browser.page.homepage();
        var videoPage = browser.page.videoPage();
        var positionBefore, positionAfter;
        common_mobile.launchBrowser(browser);
        commonMethods.enableCookies(browser);
        homePage.clcikOnSecCarouselBar()
            .clickOnSecWatch();
        browser.refresh().pause(2000)
        videoPage.moveToElement('@videoScreen',100,100,function () {
            videoPage.moveToElement('@progressBar',300,0)
            browser.mouseButtonClick();
            browser.execute(commonMethods.getJwPlayerPosition,function(result){
                positionBefore = result.value
            })
            videoPage.clickOnRewindButton();
            browser.execute(commonMethods.getJwPlayerPosition,function(result2){
                positionAfter = result2.value
            })
        })
        browser.perform(function(){
            browser.assert.ok(Math.abs(positionBefore-positionAfter) <=10);
        })
    },

    /**
     * Test case is to validate that if user is able to change the settings.
     */
    'UN89 Video Page : Settings': function(browser){
        var homePage = browser.page.homepage();
        var videoPage = browser.page.videoPage();
        var hdQuality;
        common_mobile.launchBrowser(browser);
        commonMethods.enableCookies(browser);
        homePage.clcikOnSecCarouselBar()
            .clickOnSecWatch();
        browser
            .pause(2000)
            .refresh()
        videoPage.moveToElement('@videoScreen',100,100,function () {
            videoPage.clickOnSettingsButton();
            videoPage.getText('@settings2ndHDPQuality',function(quality){
                hdQuality = quality.value;
            })
            videoPage.clickOnSettings2ndHDQuality();
        })
        browser.execute(commonMethods.getJwPlayerQuality, function (quality) {
            browser.assert.equal((quality.value)[1].label,hdQuality)
        })
        browser.execute(commonMethods.getJwPlayerState, function (playerState) {
            browser.assert.equal(playerState.value,browser.globals.state_play_jwplayer);
        })
    },

    /**
     * Test case is to validate that if user is able to change the volume.
     */
    'UN90 Video Page : JW Player Volume': function(browser){
        var homePage = browser.page.homepage();
        var videoPage = browser.page.videoPage();
        var volumeBefore, volumeAfter;
        common_mobile.launchBrowser(browser);
        commonMethods.enableCookies(browser);
        homePage.clcikOnSecCarouselBar()
            .clickOnSecWatch();
        browser.refresh()
            .pause(4000);            
        browser.execute(commonMethods.getJwPlayerVolume,function(result){
            volumeBefore = result.value
        })
        videoPage.moveToElement('@videoScreen',100,100,function () {
            videoPage.moveToElement('@muteButton',10,10,function () {
                videoPage.moveToElement('@volumeBar',0,50,function(){
                    browser.mouseButtonClick();
                })
            })
        })
        browser.execute(commonMethods.getJwPlayerVolume,function(result){
            volumeAfter = result.value
        })
        browser.perform(function(){
            browser.assert.notEqual(volumeBefore,volumeAfter)
        })
    },

    /**
     * Test case is to validate that if user is able to pause/resume video while playing.
     */
    'UN91 Video Page : Pause/Resume': function(browser){
        var homePage = browser.page.homepage();
        var videoPage = browser.page.videoPage();
        common_mobile.launchBrowser(browser);
        commonMethods.enableCookies(browser);
        homePage.clcikOnSecCarouselBar()
            .clickOnSecWatch();
        browser
            .refresh()
            .pause(2000);
        videoPage.moveToElement('@videoScreen',100,100,function () {
            browser.pause(3000);
                browser
                    .execute(commonMethods.getJwPlayerState, function (playerState) {
                    browser.assert.equal(playerState.value,browser.globals.state_pause_jwplayer);
            })
            browser.pause(2000);
            videoPage.clickOnPlayButton_mob();
            videoPage.clickOnPlayButton_mob();
            browser.pause(3000);
                browser
                    .execute(commonMethods.getJwPlayerState, function (playerState) {
                    browser.assert.equal(playerState.value,browser.globals.state_play_jwplayer);
            })
        }) 
    },

    /**
     * Test case is to validate that if user is able to view playlist by clicking on Playlist icon.
     */
    'UN92 Video Page : Click on Playlist': function(browser){
        var homePage = browser.page.homepage();
        var videoPage = browser.page.videoPage();
        common_mobile.launchBrowser(browser);
        commonMethods.enableCookies(browser);
        homePage.clcikOnSecCarouselBar()
                .clickOnSecWatch();
        browser.pause(2000);
        videoPage.moveToElement('@videoScreen',100,100,function () { 
           videoPage.clickOnPlaylistButton()
           browser.pause(2000);
            videoPage.assertPlaylistPresentMobile();
        }) 
    },

    /**
     * Test case is to validate that if user is able to play video from 
     * playlist by clicking on Playlist icon.
     */
    'UN93 Video Page : Playlist Video': function(browser){
        var homePage = browser.page.homepage();
        var videoPage = browser.page.videoPage();
        var videoNameExpected;
        common_mobile.launchBrowser(browser);
        commonMethods.enableCookies(browser);
        homePage.clcikOnSecCarouselBar()
            .clickOnSecWatch();
        browser.pause(3000)
        videoPage.moveToElement('@videoScreen',0,0,function () {
            videoPage.clickOnPlaylistButton();
            browser.pause(4000);
            videoPage.waitForElementVisible('@jwPlayerPlaylistTextMobile',4000)
            videoPage.getText('@jwPlayerPlaylist_firstMobile',function(result){
                videoNameExpected = result.value
            })
        })
        //.clickOnjwPlayerPlaylist_mob();
        browser.perform(function(){
            browser.pause(3000);
            commonMethods.assertVideoPlayedName(browser,videoNameExpected)
        })
    },

    /**
     * Test case is to validate that if Next video is playing on completion of a video.
     */
    'UN94 Video Page : Next Video after completion': function(browser){
        var homePage = browser.page.homepage();
        var videoPage = browser.page.videoPage();
        var duration, position, videoNameExpected, waitTime;
        common_mobile.launchBrowser(browser);
        commonMethods.enableCookies(browser);
        homePage.clcikOnSecCarouselBar()
            .clickOnSecWatch();
        browser.refresh().pause(2000)
        browser.execute(commonMethods.getJwPlayerDuration,function(result){
            duration = result.value
        })
        videoPage.moveToElement('@videoScreen',100,100,function () {
            videoPage.moveToElement('@progressBar',1300,0)
            browser.mouseButtonClick();
            browser.execute(commonMethods.getJwPlayerPosition,function(result2){
                position = result2.value;
                waitTime = (Math.round(Math.abs((duration-position)*1000)))+1000;
            })
        })
        videoPage.waitForElementPresent('@jwPlayerNextButton',2000,function(){
            videoPage.getText('@jwPlayerNextButton',function(result){
                videoNameExpected = result.value
            })
        })
        browser.perform(function(){
            browser.pause(waitTime)
            common_mobile.assertVideoPlayedName(browser,videoNameExpected)
        })
    },

    /**
     * Test case is to validate that user is able to scroll progress bar while playing.
     */
    'UN95 Video Page : scroll Progress bar': function(browser){
        var homePage = browser.page.homepage();
        var videoPage = browser.page.videoPage();
        var positionBefore, positionAfter;
        common_mobile.launchBrowser(browser);
        commonMethods.enableCookies(browser);
        homePage.clcikOnSecCarouselBar()
            .clickOnSecWatch();
        browser.refresh().pause(2000)
        videoPage.moveToElement('@videoScreen',100,100,function () {
            browser.execute(commonMethods.getJwPlayerPosition,function(result){
                positionBefore = result.value;
            })
            videoPage.moveToElement('@progressBar',300,0)
            browser.mouseButtonClick();
            browser.execute(commonMethods.getJwPlayerPosition,function(result2){
                positionAfter = result2.value
            })
        })
        // browser.perform(function(){
        //     browser.assert.ok(positionBefore < positionAfter);
        // })
    },

    /**
     * Test case is to validate that  user is able to add shows tag to feed in feed page
     */
    'UN103 Your Feed : Add Shows Tag': function(browser){
        var feedPage = browser.page.feedPage();
        var header = browser.page.header();
        common_mobile.launchBrowser(browser);
        commonMethods.enableCookies(browser);
        common_mobile.signOutIfSignedIn(browser);
        common_mobile.connectWithFacebook(browser, browser.globals.emailID_Fb, browser.globals.pass_Fb);
        browser.pause(2000)
        header.clickOnHamburger()
            .waitForElementVisible('@customize_mob',1000)
            .clickOnCustomize_mob();
        var addedShowTag = "//div[@class = 'src-components-tag-selector-__Group_host_2bHWn'] //child::div[text()='";
        var showTag = "(//div[@class = 'src-components-tag-selector-__Tag_tag_Bmgxn src-components-tag-selector-__Tag_dark_5sOoL' and @data-type = 'shows'])'][1]";
        var showName, showNamePath, input;
        browser.element('xpath',showTag,function(result){
            showTagPresent = result.status
            if (result.status == -1){
                feedPage.removeShowTag();
            }
        })
        feedPage.getText('@showTag',function(result){
            var input = result.value
            showName = commonMethods.lowerCaseAllWordsExceptFirstLetters(commonMethods.upperCaseFirstLetter(input))
            showNamePath = addedShowTag+""+showName+"']"
        })
        feedPage.addShowTag();
        browser.pause(1000);
        // browser.perform(function(){
        //     browser.useXpath().assert.cssProperty(showNamePath,'background-color',browser.globals.whiteColor)
        // })
    },

    /**
     * Test case is to validate that  user is able to add hashtags to feed in feed page.
     */
    'UN104 Your Feed : Add Hashtags Tag': function(browser){
        var feedPage = browser.page.feedPage();
        var header = browser.page.header();
        common_mobile.launchBrowser(browser);
        commonMethods.enableCookies(browser);
        common_mobile.signOutIfSignedIn(browser);
        common_mobile.connectWithFacebook(browser, browser.globals.emailID_Fb, browser.globals.pass_Fb);
        browser.pause(2000)
        header.clickOnHamburger()
            .clickOnCustomize_mob();
        var addedHashTag = "//div[@class = 'src-components-tag-selector-__Group_host_2bHWn']//child::div[text()='";
        var hashTag = "(//div[@class = 'src-components-tag-selector-__Tag_tag_Bmgxn src-components-tag-selector-__Tag_dark_5sOoL undefined' and @data-type = 'hashtags'])[1]";
        var showMore = "(//a[text() = 'Show More'])[2]";
        var hashtagName, hashtagNamePath;
        browser.element('xpath',hashTag,function(result){
            if (result.status == -1){
                feedPage.removeHashTag();
            }
        })
        browser.execute(function() { window.scrollBy(0, 1000); }, [])
            .pause(1000)
        feedPage.getAttribute('@hashTag','data-value',function(result){
            hashtagName = result.value
            hashtagNamePath = addedHashTag+""+hashtagName+"']"
        })
        feedPage.addHashTag();
        browser.perform(function(){
            browser.useXpath().assert.cssProperty(hashtagNamePath,'background-color',browser.globals.whiteColor)
        })
    },

    /**
     * Test case is to validate that  user is able to add athletes to feed in feed page.
     */
    'UN105 Your Feed : Add Athletes Tag': function(browser){
        var feedPage = browser.page.feedPage();
        var header = browser.page.header();
        common_mobile.launchBrowser(browser);
        commonMethods.enableCookies(browser);
        common_mobile.signOutIfSignedIn(browser);
        common_mobile.connectWithFacebook(browser, browser.globals.emailID_Fb, browser.globals.pass_Fb);
        browser.pause(2000)
        header.clickOnHamburger()
            .clickOnCustomize_mob();
        var addedAthleteTag = "//div[@class = 'src-components-tag-selector-__Group_host_2bHWn']//child::div[text()='";
        var athleteTag = "(//div[@class = 'src-components-tag-selector-__Tag_tag_Bmgxn src-components-tag-selector-__Tag_dark_5sOoL' and @data-type = 'athletes'])[1]";
        var showMore = "(//a[text() = 'Show More'])[3]";
        var athletetagName, athletestagNamePath;
        browser.element('xpath',athleteTag,function(result){
            if (result.status == -1){
                feedPage.removeAthletesTag();
            }
        })
        browser.execute(function() { window.scrollBy(0, 1500); }, [])
            .pause(1000)
        feedPage.getAttribute('@athletesTag','data-value',function(result){
            athletetagName = result.value
            athletestagNamePath = addedAthleteTag+""+athletetagName+"']"
        })
        feedPage.addAthletesTag();
        browser.perform(function(){
            browser.useXpath().assert.cssProperty(athletestagNamePath,'background-color',browser.globals.whiteColor)
        })
    },

    /**
     * Test case is to validate that  user is able to add sports tag to feed in feed page.
     */
    'UN106 Your Feed : Add Sports Tag': function(browser){
        var feedPage = browser.page.feedPage();
        var header = browser.page.header();
        common_mobile.launchBrowser(browser);
        commonMethods.enableCookies(browser);
        common_mobile.signOutIfSignedIn(browser);
        common_mobile.connectWithFacebook(browser, browser.globals.emailID_Fb, browser.globals.pass_Fb);
        browser.pause(2000)
        header.clickOnHamburger()
            .clickOnCustomize_mob();
        var addedSportsTag = "//div[@class = 'src-components-tag-selector-__Group_host_2bHWn']//child::div[text()='";
        var sportsTag = "(//div[@class = 'src-components-tag-selector-__Tag_tag_Bmgxn src-components-tag-selector-__Tag_dark_5sOoL' and @data-type = 'sports'])[1]";
        var sportsTagName, sportsTagNamePath;
        browser.element('xpath',sportsTag,function(result){
            if (result.status == -1){
                feedPage.removeSportsTag();
            }
        })
        browser.execute(function() { window.scrollBy(0, 2000); }, [])
            .pause(1000)
        feedPage.getAttribute('@sportsTag','data-value',function(result){
            sportsTagName = result.value
            sportsTagNamePath = addedSportsTag+""+sportsTagName+"']"
        })
        feedPage.addSportsTag();
        browser.perform(function(){
            browser.useXpath().assert.cssProperty(sportsTagNamePath,'background-color',browser.globals.whiteColor)
        })
    },

    /**
     * Test case is to validate that  user is able to remove shows tag from feed in feed page.
     */
    'UN107 Your Feed : Remove Shows Tag': function(browser){
        var feedPage = browser.page.feedPage();
        var header = browser.page.header();
        common_mobile.launchBrowser(browser);
        commonMethods.enableCookies(browser);
        common_mobile.signOutIfSignedIn(browser);
        common_mobile.connectWithFacebook(browser, browser.globals.emailID_Fb, browser.globals.pass_Fb);
        browser.pause(2000)
        header.clickOnHamburger()
            .clickOnCustomize_mob();
        var addedShowTag = "(//div[@class = 'src-components-tag-selector-__Tag_tag_Bmgxn src-components-tag-selector-__Tag_dark_5sOoL src-components-tag-selector-__Tag_active_R3iTJ' and @data-type = 'shows'])[1]";
        var showTag = "//div[@class = 'src-components-tag-selector-__Group_host_2bHWn'] child::div[text()='"
        var showName, showNamePath, input;
        browser.element('xpath',addedShowTag,function(result){
            if(result.status == -1){
                feedPage.addShowTag();
            }
        })
        feedPage.getText('@addedShowTag',function(result){
            input = result.value
            showName = commonMethods.lowerCaseAllWordsExceptFirstLetters(commonMethods.upperCaseFirstLetter(input))
            showNamePath = showTag+""+showName+"']"
        })
        feedPage.removeShowTag();
        // browser.perform(function(){
        //     browser.pause(1000);
        //         browser.useXpath().assert.cssProperty(showNamePath,'background-color',browser.globals.whiteColor)
        // })
    },

    /**
     * Test case is to validate that  user is able to remove hashtags from feed in feed page.
     */
    'UN108 Your Feed : Remove Hash Tag': function(browser){
        var feedPage = browser.page.feedPage();
        var header = browser.page.header();
        var addedHashTag = "(//div[@class = 'src-components-tag-selector-__Tag_tag_Bmgxn src-components-tag-selector-__Tag_dark_5sOoL src-components-tag-selector-__Tag_active_R3iTJ undefined' and @data-type = 'hashtags'])[1]";
        var hashTag = "//div[@class = 'src-components-tag-selector-__Group_host_2bHWn']//child::div[text()='"
        var hashTagName, hashTagNamePath, input;
        common_mobile.launchBrowser(browser);
        commonMethods.enableCookies(browser);
        common_mobile.signOutIfSignedIn(browser);
        common_mobile.connectWithFacebook(browser, browser.globals.emailID_Fb, browser.globals.pass_Fb);
        browser.pause(2000)
        header.clickOnHamburger()
            .clickOnCustomize_mob();
        browser.execute(function() { window.scrollBy(0, 1000); }, [])
            .element('xpath',addedHashTag,function(result){
            if(result.status == -1){
                feedPage.addHashTag();
            }
        })
        feedPage.getAttribute('@addedHashTag','data-value',function(result){
            hashtagName = result.value
            hashTagNamePath = hashTag+""+hashtagName+"']"
        })
        feedPage.removeHashTag();
        browser.perform(function(){
                browser.useXpath().assert.cssProperty(hashTagNamePath,'background-color',browser.globals.whiteColor)
        })
    },

    /**
     * Test case is to validate that  user is able to remove athletes tag from feed in feed page.
     */
    'UN109 Your Feed : Remove Athlete Tag': function(browser){
        var header = browser.page.header();
        var feedPage = browser.page.feedPage();
        var addedAthletestag = "(//div[@class = 'src-components-tag-selector-__Tag_tag_Bmgxn src-components-tag-selector-__Tag_dark_5sOoL src-components-tag-selector-__Tag_active_R3iTJ' and @data-type = 'athletes'])[1]";
        var athletesTag = "//div[@class = 'src-components-tag-selector-__Group_host_2bHWn']//child::div[text()='"
        var athletesTagName, athletesTagNamePath, input;
        common_mobile.launchBrowser(browser);
        commonMethods.enableCookies(browser);
        common_mobile.signOutIfSignedIn(browser);
        common_mobile.connectWithFacebook(browser, browser.globals.emailID_Fb, browser.globals.pass_Fb);
        browser.pause(2000)
        header.clickOnHamburger()
            .clickOnCustomize_mob();
        browser.execute(function() { window.scrollBy(0, 1500); }, [])
            .element('xpath',addedAthletestag,function(result){
            if(result.status == -1){
                feedPage.addAthletesTag();
            }
        })
        feedPage.getAttribute('@addedAthletesTag','data-value',function(result){
            athletesTagName = result.value
            athletesTagNamePath = athletesTag+""+athletesTagName+"']"
        })
        feedPage.removeAthletesTag();
        browser.perform(function(){
                browser.useXpath().assert.cssProperty(athletesTagNamePath,'background-color',browser.globals.whiteColor)
        })
    },

    /**
     * Test case is to validate that  user is able to remove sports tag from feed in feed page.
     */
    'UN110 Your Feed : Remove Sports Tag': function(browser){
        var header = browser.page.header();
        var feedPage = browser.page.feedPage();
        var addedSportstag = "(//div[@class = 'src-components-tag-selector-__Tag_tag_Bmgxn src-components-tag-selector-__Tag_dark_5sOoL src-components-tag-selector-__Tag_active_R3iTJ' and @data-type = 'sports'])[1]";
        var sportsTag = "//div[@class = 'src-components-tag-selector-__Group_host_2bHWn']//child::div[text()='"
        var sportsTagName, sportsTagNamePath, input;
        common_mobile.launchBrowser(browser);
        commonMethods.enableCookies(browser);
        common_mobile.signOutIfSignedIn(browser);
        common_mobile.connectWithFacebook(browser, browser.globals.emailID_Fb, browser.globals.pass_Fb);
        browser.pause(2000)
        header.clickOnHamburger()
        .clickOnCustomize_mob();
        browser
            .execute(function() { window.scrollBy(0, 2000); }, [])
            .element('xpath',addedSportstag,function(result){
            if(result.status == -1){
                feedPage.addSportsTag();
            }
        })
        feedPage.getAttribute('@addedSportsTag','data-value',function(result){
            sportsTagName = result.value
            sportsTagNamePath = sportsTag+""+sportsTagName+"']"
        })
        feedPage.removeSportsTag();
        browser.perform(function(){
                browser.useXpath().assert.cssProperty(sportsTagNamePath,'background-color',browser.globals.whiteColor)
        })
    },

    /**
     * Test case is to validate that  user is able to navigate to search page.
     */
    'UN125 Search: Search Page': function(browser){
        var header = browser.page.header();
        var searchPage = browser.page.searchPage();
        common_mobile.launchBrowser(browser);
        commonMethods.enableCookies(browser);
        header.clickOnSearch()
            .enterSearchKeyword(browser.globals.searchKeyword)
            .clickOnSearch()
        browser.pause(1000)
            .refresh();
        searchPage.waitForElementVisible('@itemLists')
            .assertPageTitle("UNINTERRUPTED - Search results")
    },

    /**
     * Test case is to validate that user is shown the suggestion list for the keyword entered.
     */
    'UN126 Search: Suggestion List': function(browser){
        var header = browser.page.header();
        var searchPage = browser.page.searchPage();
        suggestionList = "//div[@class ='src-components-search-bar-__SearchBar_autocomplete__resultWrapper_3jG-8']"
        common_mobile.launchBrowser(browser);
        commonMethods.enableCookies(browser);
        header.clickOnSearch()
            .enterSearchKeyword(browser.globals.searchText)
        browser.pause(2000)
        browser.elements('xpath', suggestionList, function (elements) {
            for(var i=0;i<elements.value.length;i++){
                searchPage.assertSuggestionText(browser.globals.searchText)
            }
        })
    },

    /**
     * Test case is to validate that search result matches with the search keyword
     */
    'UN127 Search: Search Result': function(browser){
        var header = browser.page.header();
        var searchPage = browser.page.searchPage();
        var keyword = browser.globals.longSearchText
        common_mobile.launchBrowser(browser);
        commonMethods.enableCookies(browser);
        header.clickOnSearch()
            .enterSearchKeyword(browser.globals.longSearchText)
            .clickOnSearch()
        browser.pause(1000)
        browser.perform(function(){
            urlKeyword = keyword.replace(/ /g,"%20")
            searchPage.assertURLContains(urlKeyword)
        })
    },

    /**
     * Test case is to validate that matched search keywords in suggestion list is underlined
     */
    'UN128 Search: Underline Keyword': function(browser){
        var header = browser.page.header();
        var searchPage = browser.page.searchPage();
        var expectedKeyword = (browser.globals.searchText).toLowerCase();
        var actualKeyword;
        common_mobile.launchBrowser(browser);
        commonMethods.enableCookies(browser);
        header.clickOnSearch()
            .enterSearchKeyword(browser.globals.searchText)
        browser.pause(1000);
        searchPage.getText('@underlinedKeyword',function(result){
                actualKeyword = (result.value).toLowerCase()
        })
        browser.perform(function(){
            browser.assert.equal(actualKeyword,expectedKeyword);
        })
    },

    /**
     * Test case is to validate that Videos, Podcasts, athletes and Photos tabs are displayed.
     */
    'UN129 Search: Search Navigation ': function(browser){
        var header = browser.page.header();
        var searchPage = browser.page.searchPage();
        common_mobile.launchBrowser(browser);
        commonMethods.enableCookies(browser);
        header.clickOnSearch()
            .enterSearchKeyword(browser.globals.longSearchText)
            .clickOnSearch()
        browser.pause(1000)
        browser.perform(function(){
            searchPage.assertNavBar_Videos()
                .assertNavBar_Podcasts()
                .assertNavBar_Athletes()
                .assertNavBar_Photos()
        })
    },

    /**
     * Test case is to validate that on searching any keyword, if no result is available then 
     * text color for tab should be grayed
     */
    'UN130 Search: Text Color ': function(browser){
        var header = browser.page.header();
        var searchPage = browser.page.searchPage();
        common_mobile.launchBrowser(browser);
        commonMethods.enableCookies(browser);
        header.clickOnSearch()
            .enterSearchKeyword(browser.globals.longSearchText)
            .clickOnSearch()
        browser.pause(1000)
        browser.perform(function(){
            searchPage.assertNavBar_AthletesText(browser.globals.greyColor)
        })
    },

    /**
     * Test case is to validate that user is able to click on Video below the search bar
     */
    'UN131 Search: Click On Video ': function(browser){
        var header = browser.page.header();
        var searchPage = browser.page.searchPage();
        common_mobile.launchBrowser(browser);
        commonMethods.enableCookies(browser);
        header.clickOnSearch()
            .enterSearchKeyword(browser.globals.searchText)
            .clickOnSearch()
        browser.pause(4000)
        searchPage
            .clickOnNavBar_Podcasts();
        searchPage.waitForElementVisible('@navBar_Podcasts',4000)
        searchPage
            .clickOnNavBar_Videos()
        browser.pause(4000)
        searchPage.getAttribute('@navBar_Videos','class',function(result){
            searchPage.assertSelectedNavBar(result.value);
        })
    },

    /**
     * Test case is to validate that user is able to click on Podcast below the search bar
     */
    'UN132 Search: Click On Podcasts ': function(browser){
        var header = browser.page.header();
        var searchPage = browser.page.searchPage();
        common_mobile.launchBrowser(browser);
        commonMethods.enableCookies(browser);
        header.clickOnSearch()
            .enterSearchKeyword(browser.globals.searchText)
            .clickOnSearch()
        browser.pause(2000)
        searchPage
            .clickOnNavBar_Podcasts()
        browser.pause(2000)
        searchPage.getAttribute('@navBar_Podcasts','class',function(result){
            searchPage.assertSelectedNavBar(result.value);
        })
    },

    /**
     * Test case is to validate that user is able to click on Photos below the search bar.
     */
    'UN134 Search: Click On Photos ': function(browser){
        var header = browser.page.header();
        var searchPage = browser.page.searchPage();
        common_mobile.launchBrowser(browser);
        commonMethods.enableCookies(browser);
        header.clickOnSearch()
            .enterSearchKeyword(browser.globals.searchText)
            .clickOnSearch()
        browser.pause(2000)
        searchPage
            .clickOnNavBar_Photos()
        browser.pause(2000)
        searchPage.getAttribute('@navBar_Photos','class',function(result){
            searchPage.assertSelectedNavBar(result.value);
        })
    },

    /**
     * Test case is to validate that user is able to play video from the search page.
     */
    'UN135 Search: Play Video ': function(browser){
        var header = browser.page.header();
        var searchPage = browser.page.searchPage();
        common_mobile.launchBrowser(browser);
        commonMethods.enableCookies(browser);
        var cardNameExpected;
        header.clickOnSearch()
            .enterSearchKeyword(browser.globals.searchText)
            .clickOnSearch()
        browser.pause(2000)
        searchPage.getText('@cardName_first',function(result){
            cardNameExpected = result.value;
        })
        searchPage.clickOnCard_first();
        browser.pause(1000,function(){
            common_mobile.assertVideoPlayedName(browser,cardNameExpected);
        })
    },

    /**
     * Test case is to validate that user is able to play podcasts from the search page.
     */
    'UN136 Search: Play Podcasts ': function(browser){
        var header = browser.page.header();
        var homePage = browser.page.homepage();
        var searchPage = browser.page.searchPage();
        common_mobile.launchBrowser(browser);
        commonMethods.enableCookies(browser);
        var cardNameExpected;
        header.clickOnSearch()
            .enterSearchKeyword(browser.globals.searchText)
            .clickOnSearch()
        browser.pause(2000)
        searchPage.clickOnNavBar_Podcasts()
            .waitForElementVisible('@cardName_first',2000)
        searchPage.getText('@cardName_first',function(result){
            cardNameExpected = result.value;
        })
        searchPage.clickOnCard_first();
        browser.pause(7000,function(){
            homePage.assertStickyPlayerPresent()
                .assert.containsText('@podcastName_sticky',cardNameExpected);
        })
    },

    /**
     * Test case is to validate that user is able to view photos from the search page.
     */
    'UN137 Search: Pictorials ': function(browser){
        var header = browser.page.header();
        var homePage = browser.page.homepage();
        var searchPage = browser.page.searchPage();
        common_mobile.launchBrowser(browser);
        commonMethods.enableCookies(browser);
        var cardNameExpected;
        header.clickOnSearch()
            .enterSearchKeyword(browser.globals.searchText)
            .clickOnSearch()
        browser.pause(2000)
        searchPage.clickOnNavBar_Photos()
            .waitForElementVisible('@cardName_first',2000)
        searchPage.getText('@cardName_first',function(result){
            cardNameExpected = result.value;
        })
        searchPage.clickOnCard_first();
        browser.execute(function() { window.scrollBy(0, 500); }, [])
        browser.pause(100,function(){
            searchPage.assertPictorialName(cardNameExpected)
        })
    },

    /**
     * Test case is to validate that color of play icon changes on hovering cursor over the 
     * video thumbnail in video section of Search Page
     */
    'UN138 Search: Video Play Icon Color ': function(browser){
        var header = browser.page.header();
        var homePage = browser.page.homepage();
        var searchPage = browser.page.searchPage();
        common_mobile.launchBrowser(browser);
        commonMethods.enableCookies(browser);
        header.clickOnSearch()
            .enterSearchKeyword(browser.globals.searchText)
            .clickOnSearch()
        searchPage
            .waitForElementVisible('@card_first',2000)
            .moveToElement('@card_first',0,0,function(){
            searchPage.assertPlayBGColor(browser.globals.redColor)
        });
    },

    /**
     * Test case is to validate that color of headphone icon changes on hovering cursor over the 
     * podcast thumbnail in podcasts section of Search Page
     */
    'UN139 Search: Podcast Play Icon Color ': function(browser){
        var header = browser.page.header();
        var homePage = browser.page.homepage();
        var searchPage = browser.page.searchPage();
        common_mobile.launchBrowser(browser);
        commonMethods.enableCookies(browser);
        header.clickOnSearch()
            .enterSearchKeyword(browser.globals.searchText)
            .clickOnSearch()
        browser.pause(2000)
        searchPage.clickOnNavBar_Podcasts()
            .waitForElementVisible('@card_first',2000)
        searchPage.moveToElement('@card_first',0,0,function(){
            searchPage.assertPlayBGColor(browser.globals.redColor)
        });
    },

    /**
     * Test case is to validate that navigating back to SERP is preserving tab choice
     */
    'UN140 Search: Preserve Search Input ': function(browser){
        var header = browser.page.header();
        var homePage = browser.page.homepage();
        var searchPage = browser.page.searchPage();
        common_mobile.launchBrowser(browser);
        commonMethods.enableCookies(browser);
        header.clickOnSearch()
            .enterSearchKeyword(browser.globals.searchText)
            .clickOnHomePageLink()
            .clickOnSearch()
            .clickOnSearchBar()
            .assertSearchEnabled();
    },


     /**
     * Test case is to validate that search input is cleared once it is cleared from serp or any page
     */
    'UN141 Search: Cleared Search Input ': function(browser){
        var header = browser.page.header();
        var homePage = browser.page.homepage();
        var searchPage = browser.page.searchPage();
        common_mobile.launchBrowser(browser);
        commonMethods.enableCookies(browser);
        header.clickOnSearch()
            .enterSearchKeyword(browser.globals.searchText)
            .clearSearchInput()
            .clickOnHomePageLink()
            .clickOnSearch()
            .assertSearchNotEnabled();
    },

    /**
     * Test case is to validate that "No results found" is displayed 
     * if user has entered keywords which do not match any content in the website
     */
    'UN142 Search: Invalid Character Keyword ': function(browser){
        var header = browser.page.header();
        var searchPage = browser.page.searchPage();
        common_mobile.launchBrowser(browser);
        commonMethods.enableCookies(browser);
        header.clickOnSearch()
            .enterSearchKeyword(browser.globals.invalidCharKeyword)
            .clickOnSearch()
        browser.pause(1000)
        searchPage.assertNoResultsTextPresent();
    },

    /**
     * Test case is to validate that "No results found" is displayed 
     * if user has entered special characters search keywords.
     */
    'UN143 Search: Special Character Keyword ': function(browser){
        var header = browser.page.header();
        var searchPage = browser.page.searchPage();
        common_mobile.launchBrowser(browser);
        commonMethods.enableCookies(browser);
        header.clickOnSearch()
            .enterSearchKeyword(browser.globals.specialCharKeyword)
            .clickOnSearch()
        browser.pause(1000)
        searchPage.assertNoResultsTextPresent();
    },

    /**
     * Test case is to validate that user is shown the correct counts for Videos tab in SERP
     */
    'UN145 Search: Videos Count ': function(browser){
        var header = browser.page.header();
        var searchPage = browser.page.searchPage();
        var cardLocator = "//div[@class = 'src-components-search-results-__SearchResultsGrid_host_1y09Y']/div";
        var searchCount,noOfCards;
        common_mobile.launchBrowser(browser);
        commonMethods.enableCookies(browser);
        header.clickOnSearch()
            .enterSearchKeyword(browser.globals.videoPodcastKeyword)
            .clickOnSearch()
        browser.pause(2000)
            .elements('xpath',cardLocator,function(result){
            noOfCards = result.value.length
        })
        searchPage.getText('@searchCount',function(result){
            var text = result.value;
            searchCount = text.split(" ",1);
        })
        browser.perform(function(){
            browser.assert.equal(searchCount,noOfCards);
        })
    },

    /**
     * Test case is to validate that user is shown the correct counts for podcasts tab in SERP
     */
    'UN146 Search: Podcasts Count ': function(browser){
        var header = browser.page.header();
        var searchPage = browser.page.searchPage();
        var cardLocator = "//div[@class = 'src-components-search-results-__SearchResultsGrid_host_1y09Y']/div";
        var searchCount,noOfCards;
        common_mobile.launchBrowser(browser);
        commonMethods.enableCookies(browser);
        header.clickOnSearch()
            .enterSearchKeyword(browser.globals.videoPodcastKeyword)
            .clickOnSearch()
        browser.pause(2000)
        searchPage.clickOnNavBar_Podcasts()
        browser.pause(1000)
        searchPage.getText('@searchCount',function(result){
            var text = result.value;
            searchCount = text.split(" ",1);
        }) 
        browser.elements('xpath',cardLocator,function(result){
            noOfCards = result.value.length
        })
        browser.perform(function(){
            browser.assert.equal(searchCount,noOfCards);
        })
    },

    /**
     * Test case is to validate that user is shown the correct counts for photos tab in SERP
     */
    'UN147 Search: Photos Count ': function(browser){
        var header = browser.page.header();
        var searchPage = browser.page.searchPage();
        var cardLocator = "//div[@class = 'src-components-search-results-__SearchResultsGrid_host_1y09Y']/div";
        var searchCount,noOfCards;
        common_mobile.launchBrowser(browser);
        commonMethods.enableCookies(browser);
        header.clickOnSearch()
            .enterSearchKeyword(browser.globals.lebronJames)
            .clickOnSearch()
        browser.pause(2000)
        searchPage.clickOnNavBar_Photos()
        browser.pause(1000)
            .elements('xpath',cardLocator,function(result){
            noOfCards = (result.value).length
        })
        searchPage.getText('@searchCount',function(result){
            var text = result.value;
            searchCount = text.split(" ",1);
        })
        browser.perform(function(){
            browser.assert.equal(searchCount,noOfCards);
        })
    },

    /**
     * Test case is to validate that for empty search the search icon should be greyed out.
     */
    'UN148 Search: Empty Search Input ': function(browser){
        var header = browser.page.header();
        common_mobile.launchBrowser(browser);
        commonMethods.enableCookies(browser);
        header.clickOnSearch()
            .enterSearchKeyword(browser.globals.blankData)
            .assertSearchNotEnabled();
    },

    /**
     * Test case is to validate that user is able to add tag for Video tab.
     */
    'UN152 Search: Add Video Tag ': function(browser){
        var header = browser.page.header();
        var searchPage = browser.page.searchPage();
        var selectTagClass = "src-components-card-label-__CardLabelAnimated_tag_1ssof    ";
        common_mobile.launchBrowser(browser);
        commonMethods.enableCookies(browser);
        common_mobile.signOutIfSignedIn(browser);
        common_mobile.connectWithTwitter(browser, browser.globals.email_twitter, browser.globals.pass_twitter);
        header.clickOnSearch()
            .enterSearchKeyword(browser.globals.searchText)
            .clickOnSearch()
        browser.pause(2000)
        searchPage.getAttribute('@tag_card','class',function(result){
            if (selectTagClass != result.value){
                searchPage.clickOnTag_first();
            }
        })
        searchPage
            .clickOnTag_first();
        searchPage.getAttribute('@tag_card','class',function(result){
            // browser.assert.notEqual(selectTagClass,result.value)
        })
    },

    /**
     * Test case is to validate that user is able to add tag for Podcast tab.
     */
    'UN153 Search: Add Podcasts Tag ': function(browser){
        var header = browser.page.header();
        var searchPage = browser.page.searchPage();
        var selectTagClass = "src-components-card-label-__CardLabelAnimated_tag_1ssof    ";
        common_mobile.launchBrowser(browser);
        commonMethods.enableCookies(browser);
        common_mobile.signOutIfSignedIn(browser);
        common_mobile.connectWithTwitter(browser, browser.globals.email_twitter, browser.globals.pass_twitter);
        header.clickOnSearch()
            .enterSearchKeyword(browser.globals.searchText)
            .clickOnSearch()
        browser.pause(2000)
        searchPage.clickOnNavBar_Podcasts()
        browser.pause(1000)
        searchPage.getAttribute('@tag_card','class',function(result){
            if (selectTagClass != result.value){
                searchPage.clickOnTag_first();
            }
        })
        searchPage
            .clickOnTag_first();
        searchPage.getAttribute('@tag_card','class',function(result){
            browser.assert.notEqual(selectTagClass,result.value)
        })
    },

    /**
     * Test case is to validate that user is able to add tag for Photos tab.
     */
    'UN154 Search: Add Photos Tag ': function(browser){
        var header = browser.page.header();
        var searchPage = browser.page.searchPage();
        var selectTagClass = "src-components-card-label-__CardLabelAnimated_tag_1ssof    ";
        common_mobile.launchBrowser(browser);
        commonMethods.enableCookies(browser);
        common_mobile.signOutIfSignedIn(browser);
        common_mobile.connectWithTwitter(browser, browser.globals.email_twitter, browser.globals.pass_twitter);
        header.clickOnSearch()
            .enterSearchKeyword(browser.globals.searchText)
            .clickOnSearch()
        browser.pause(2000)
        searchPage.clickOnNavBar_Photos()
        browser.pause(1000)
        searchPage.getAttribute('@tag_card','class',function(result){
            if (selectTagClass != result.value){
                searchPage.clickOnTag_first();
            }
        })
        searchPage
            .clickOnTag_first();
        searchPage.getAttribute('@tag_card','class',function(result){
            browser.assert.notEqual(selectTagClass,result.value)
        })
    },

    /**
     * Test case is to validate that user is able to remove tag for Video tab.
     */
    'UN155 Search: Remove Video Tag ': function(browser){
        var header = browser.page.header();
        var searchPage = browser.page.searchPage();
        var selectTagClass = "src-components-card-label-__CardLabelAnimated_tag_1ssof    ";
        common_mobile.launchBrowser(browser);
        commonMethods.enableCookies(browser);
        common_mobile.signOutIfSignedIn(browser);
        common_mobile.connectWithTwitter(browser, browser.globals.email_twitter, browser.globals.pass_twitter);
        header.clickOnSearch()
            .enterSearchKeyword(browser.globals.searchText)
            .clickOnSearch()
        browser.pause(2000)
        searchPage.getAttribute('@tag_card','class',function(result){
            if (selectTagClass == result.value){
                searchPage.clickOnTag_first();
            }
        })
        searchPage
            .clickOnTag_first();
        searchPage.getAttribute('@tag_card','class',function(result){
            browser.assert.equal(selectTagClass,result.value)
        })
    },

    /**
     * Test case is to validate that user is able to remove tag for Podcasts tab.
     */
    'UN156 Search: Remove Podcasts Tag ': function(browser){
        var header = browser.page.header();
        var searchPage = browser.page.searchPage();
        var selectTagClass = "src-components-card-label-__CardLabelAnimated_tag_1ssof    ";
        common_mobile.launchBrowser(browser);
        commonMethods.enableCookies(browser);
        common_mobile.signOutIfSignedIn(browser);
        common_mobile.connectWithTwitter(browser, browser.globals.email_twitter, browser.globals.pass_twitter);
        header.clickOnSearch()
            .enterSearchKeyword(browser.globals.searchText)
            .clickOnSearch()
        browser.pause(2000)
        searchPage.clickOnNavBar_Podcasts()
        browser.pause(1000)
        searchPage.getAttribute('@tag_card','class',function(result){
            if (selectTagClass == result.value){
                searchPage.clickOnTag_first();
            }
        })
        searchPage
            .clickOnTag_first();
        searchPage.getAttribute('@tag_card','class',function(result){
            browser.assert.equal(selectTagClass,result.value)
        })
    },

    /**
     * Test case is to validate that user is able to remove tag for Photos tab.
     */
    'UN157 Search: Remove Photos Tag ': function(browser){
        var header = browser.page.header();
        var searchPage = browser.page.searchPage();
        var selectTagClass = "src-components-card-label-__CardLabelAnimated_tag_1ssof    ";
        common_mobile.launchBrowser(browser);
        commonMethods.enableCookies(browser);
        common_mobile.signOutIfSignedIn(browser);
        common_mobile.connectWithTwitter(browser, browser.globals.email_twitter, browser.globals.pass_twitter);
        header.clickOnSearch()
            .enterSearchKeyword(browser.globals.searchText)
            .clickOnSearch()
        browser.pause(2000)
        searchPage.clickOnNavBar_Photos()
        browser.pause(1000)
        searchPage.getAttribute('@tag_card','class',function(result){
            if (selectTagClass == result.value){
                searchPage.clickOnTag_first();
            }
        })
        searchPage
            .clickOnTag_first();
        searchPage.getAttribute('@tag_card','class',function(result){
            browser.assert.equal(selectTagClass,result.value)
        })
    },

    /**
     * Test case is to validate that user is able to close connect pop up 
     * without clicking on close icon
     */
    'UN07 Close Connect popup' : function SignUp_Newsletter(browser) {
        var header = browser.page.header();
        var homePage = browser.page.homepage();
        common_mobile.launchBrowser(browser);
        common_mobile.signOutIfSignedIn(browser);
        commonMethods.enableCookies(browser);
        header.clickOnConnectIcon_mobile();
        homePage
            .waitForElementVisible('@connectWithUsText',2000)
            .assertConnectPopUp(browser.globals.connectUs);
        browser.keys([browser.Keys.CONTROL, browser.Keys.ESCAPE])
        homePage.assertSignUpModalNotPresent();
    },
// 90, 93, 126, 131, 132, 135, 136
}