var conf = require('../../../nightwatch.conf.js');
var commonMethods= require('../../common/commonMethods');

module.exports = {

    /**
     * Test case is to validate that user is not able to add tag of container 1 to the feed if not signed in.
     */
    'UN 12 Unsigned:Container 1: Add tag to feed': function(browser){
        var homePage = browser.page.homepage();
        commonMethods.launchBrowser(browser);
        commonMethods.signOutIfSignedIn(browser);
        browser.execute(function() { window.scrollBy(0, 500); }, [])
        homePage.clickOnAddTag_firstContainer()
            .assertSignUpModalPresent();
    },

    /**
     * Test case is to validate that user is not able to add tag of Latest Stories container to the feed if not signed in
     */
    'UN 29 Unsigned Latest Stories Container: Add tag to feed': function(browser){
        var homePage = browser.page.homepage();
        commonMethods.launchBrowser(browser);
        browser.execute(function() { window.scrollBy(0, 2000); }, [])
        browser.pause(1000);
        browser.execute(function() { window.scrollBy(0, 1500); }, [])
        browser.pause(1000);
        homePage.clickOnTag_latestStories()
            .assertSignUpModalPresent();
    },

    /**
     * Test case is to validate that user is not able to add tag of podcast show to the feed if not signed in.
     */
    'UN41 Unsigned Podcast Show : Add Tag to Feed': function(browser){
        var header = browser.page.header();
        var podcastPage = browser.page.podcastPage();
        var podcastCard = browser.page.podcastCardPage();
        var homepage = browser.page.homepage();
        commonMethods.launchBrowser(browser);
        commonMethods.signOutIfSignedIn(browser);
        commonMethods.enableCookies(browser)
        header.clickOnPodcast();
        podcastPage.waitForElementPresent('@headphoneIcon',2000)
            .clickOnAllEpisodes();
        podcastCard.clickOnShowTag()
        homepage.assertSignUpModalPresent();
    },
    
    /**
     * Test case is to validate that user is not able to add tag of podcast episode to the feed if not signed in.
     */
    'UN44 Unsigned Podcast Episode : Add Tag to Feed': function(browser){
        var header = browser.page.header();
        var podcastPage = browser.page.podcastPage();
        var podcastCard = browser.page.podcastCardPage();
        var homepage = browser.page.homepage();
        commonMethods.launchBrowser(browser);
        commonMethods.signOutIfSignedIn(browser);
        commonMethods.enableCookies(browser)
        header.clickOnPodcast();
        podcastPage.waitForElementPresent('@headphoneIcon',2000)
            .clickOnAllEpisodes();
        podcastCard.clickonEpisodeTag()
        homepage.assertSignUpModalPresent();
    },

    /**
     * Test case is to validate that user is not able to share show on facebook if not signed in.
     */
    'UN76 Originals Card : Share Show on Facebook': function(browser){
        var header = browser.page.header();
        var originals = browser.page.originalsPage();
        var originalCard = browser.page.originalCardPage();
        commonMethods.launchBrowser(browser);
        commonMethods.signOutIfSignedIn(browser);
        commonMethods.enableCookies(browser)
        header.clickOnOriginal();
        originals.clickOnOriginalCard_first();
        originalCard.clickOnShare_facebook();        
        commonMethods.assertFacebookLoginPopup(browser);
    },

    /**
     * Test case is to validate that user is not able to tweet show if not signed in.
     */
    'UN77 Originals Card : Tweet Show': function(browser){
        var header = browser.page.header();
        var originals = browser.page.originalsPage();
        var originalCard = browser.page.originalCardPage();
        commonMethods.launchBrowser(browser);
        commonMethods.enableCookies(browser);
        commonMethods.signOutIfSignedIn(browser);
        header.clickOnOriginal();
        originals.clickOnOriginalCard_first();
        originalCard.clickOnTweetButton();        
        commonMethods.assertTwitterLoginPopup(browser);
    },

    /**
     * Test case is to validate that user is not able to add show tag of originals card to the feed if not signed in
     */
    'UN78 Unsigned Originals Card : Add show tag': function(browser){
        var header = browser.page.header();
        var homePage = browser.page.homepage();
        var originals = browser.page.originalsPage();
        var originalCard = browser.page.originalCardPage();
        commonMethods.launchBrowser(browser);
        commonMethods.enableCookies(browser);
        commonMethods.signOutIfSignedIn(browser);
        header.clickOnOriginal();
        originals.clickOnOriginalCard_first();
        originalCard.clickOnShowTag();
        homePage.assertSignUpModalPresent();
    },

    /**
     * Test case is to validate that user is not able to add originals episode tag to the feed if not signed in.
     */
    'UN83 Unsigned Originals Card : Add Episode tag': function(browser){
        var header = browser.page.header();
        var homePage = browser.page.homepage();
        var originals = browser.page.originalsPage();
        var originalCard = browser.page.originalCardPage();
        commonMethods.launchBrowser(browser);
        commonMethods.enableCookies(browser);
        commonMethods.signOutIfSignedIn(browser);
        header.clickOnOriginal();
        originals.clickOnOriginalCard_first();
        browser.execute(function() { window.scrollBy(0, 500); }, [])
        originalCard.waitForElementVisible('@episodeTag_first',2000)
            .clickOnEpisodeTag_first();
        homePage.assertSignUpModalPresent();
    },

    /**
     * Test case is to validate that user is not able to add tag to the feed in video page if not signed in.
     */
    'UN100 Video Page : Add Tag': function(browser){
        var homePage = browser.page.homepage();
        var videoPage = browser.page.videoPage();
        var feedLocator = "(//div[@class = 'src-components-card-label-__CardLabelAnimated_text_16gHw'])[1]"
        commonMethods.launchBrowser(browser);
        commonMethods.enableCookies(browser);
        commonMethods.signOutIfSignedIn(browser);
        homePage.clcikOnFirstCarouselBar()
            .clickOnFirstWatch();
        browser
            .refresh()
            .pause(5000)
        videoPage.moveToElement('@videoScreen',100,100,function () {
            videoPage.clickOnPlayButton()  
        })
        browser.element('xpath',feedLocator,function (result) {
            if(result.status != -1){
                videoPage.clickOnTag();
                browser.perform(function(){
                    browser.pause(2000)
                    videoPage.assertSignUpModalPresent();
                })
            }
        })
    },

    /**
     * Test case is to validate that user is not able to add video tag to the feed in search page if not signed in.
     */
    'UN149 Search: Add Video Tag ': function(browser){
        var header = browser.page.header();
        var homePage = browser.page.homepage();
        var searchPage = browser.page.searchPage();
        commonMethods.launchBrowser(browser);
        commonMethods.enableCookies(browser);
        commonMethods.signOutIfSignedIn(browser);
        header.clickOnSearch()
            .enterSearchKeyword(browser.globals.videoPodcastKeyword)
            .clickOnSearch()
        browser.pause(2000)
        browser.execute(function() { window.scrollBy(0, 500); }, [])
        searchPage
            .clickOnTag_first();
        homePage.assertSignUpModalPresent();
    },

    /**
     * Test case is to validate that user is not able to add podcast tag to the feed in search page if not signed in.
     */
    'UN150 Search: Add Podcasts Tag ': function(browser){
        var header = browser.page.header();
        var homePage = browser.page.homepage();
        var searchPage = browser.page.searchPage();
        commonMethods.launchBrowser(browser);
        commonMethods.enableCookies(browser);
        commonMethods.signOutIfSignedIn(browser);
        header.clickOnSearch()
            .enterSearchKeyword(browser.globals.videoPodcastKeyword)
            .clickOnSearch()
        browser.pause(2000)
        searchPage.clickOnNavBar_Podcasts()
        browser.pause(1000)
        browser.execute(function() { window.scrollBy(0, 500); }, [])
        searchPage
            .clickOnTag_first();
        homePage.assertSignUpModalPresent();
    },

    /**
     * Test case is to validate that user is not able to add photos tag to the feed in search page if not signed in.
     */
    'UN151 Search: Add Photos Tag ': function(browser){
        var header = browser.page.header();
        var homePage = browser.page.homepage();
        var searchPage = browser.page.searchPage();
        commonMethods.launchBrowser(browser);
        header.clickOnSearch()
            .enterSearchKeyword(browser.globals.searchKeyword)
            .clickOnSearch()
        browser.pause(2000)
        searchPage.clickOnNavBar_Photos()
        browser.pause(1000)
        browser.execute(function() { window.scrollBy(0, 500); }, [])
        searchPage
            .clickOnTag_first();
        homePage.assertSignUpModalPresent();
    },

}