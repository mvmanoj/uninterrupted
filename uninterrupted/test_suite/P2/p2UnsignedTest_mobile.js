var conf = require('../../../nightwatch.conf.js');
var commonMethods= require('../../common/commonMethods');
var common_mobile= require('../../common/common_mobile');

module.exports = {

    /**
     * Test case is to validate that user is not able to add tag of container 1 to the feed if not signed in.
     */
    'UN 12 Unsigned:Container 1: Add tag to feed': function(browser){
        var homePage = browser.page.homepage();
        common_mobile.launchBrowser(browser);
        common_mobile.signOutIfSignedIn(browser);
        browser.execute(function() { window.scrollBy(0, 500); }, [])
        homePage.clickOnAddTag_firstContainer()
            .assertSignUpModalPresent();
    },

    /**
     * Test case is to validate that user is not able to add tag of Latest Stories container to the feed if not signed in
     */
    'UN 29 Unsigned Latest Stories Container: Add tag to feed': function(browser){
        var homePage = browser.page.homepage();
        common_mobile.launchBrowser(browser);
        browser.execute(function() { window.scrollBy(0, 2000); }, [])
        browser.pause(1000);
        browser.execute(function() { window.scrollBy(0, 1500); }, [])
        browser.pause(1000);
        homePage.clickOnTag_latestStories()
            .assertSignUpModalPresent();
    },

    /**
     * Test case is to validate that user is not able to add tag of podcast show to the feed if not signed in.
     */
    'UN41 Unsigned Podcast Show : Add Tag to Feed': function(browser){
        var header = browser.page.header();
        var podcastPage = browser.page.podcastPage();
        var podcastCard = browser.page.podcastCardPage();
        var homepage = browser.page.homepage();
        common_mobile.launchBrowser(browser);
        common_mobile.signOutIfSignedIn(browser);
        common_mobile.enableCookies(browser)
        header.clickOnPodcast();
        podcastPage.waitForElementPresent('@headphoneIcon',2000)
            .clickOnAllEpisodes();
        podcastCard.clickOnShowTag()
        homepage.assertSignUpModalPresent();
    },
    
    /**
     * Test case is to validate that user is not able to add tag of podcast episode to the feed if not signed in.
     */
    'UN44 Unsigned Podcast Episode : Add Tag to Feed': function(browser){
        var header = browser.page.header();
        var podcastPage = browser.page.podcastPage();
        var podcastCard = browser.page.podcastCardPage();
        var homepage = browser.page.homepage();
        common_mobile.launchBrowser(browser);
        common_mobile.signOutIfSignedIn(browser);
        common_mobile.enableCookies(browser)
        header.clickOnPodcast();
        podcastPage.waitForElementPresent('@headphoneIcon',2000)
            .clickOnAllEpisodes();
        podcastCard.clickonEpisodeTag()
        homepage.assertSignUpModalPresent();
    },

    /**
     * Test case is to validate that user is not able to add show tag of originals card to the feed if not signed in
     */
    'UN78 Unsigned Originals Card : Add show tag': function(browser){
        var header = browser.page.header();
        var homePage = browser.page.homepage();
        var originals = browser.page.originalsPage();
        var originalCard = browser.page.originalCardPage();
        common_mobile.launchBrowser(browser);
        common_mobile.enableCookies(browser);
        common_mobile.signOutIfSignedIn(browser);
        header.clickOnOriginal();
        originals.clickOnOriginalCard_first();
        originalCard.waitForElementVisible('@tag_show',2000)
        originalCard.clickOnShowTag();
        homePage.assertSignUpModalPresent();
    },

    /**
     * Test case is to validate that user is not able to add originals episode tag to the feed if not signed in.
     */
    'UN83 Unsigned Originals Card : Add Episode tag': function(browser){
        var header = browser.page.header();
        var homePage = browser.page.homepage();
        var originals = browser.page.originalsPage();
        var originalCard = browser.page.originalCardPage();
        common_mobile.launchBrowser(browser);
        common_mobile.enableCookies(browser);
        common_mobile.signOutIfSignedIn(browser);
        header.clickOnOriginal();
        originals.clickOnOriginalCard_first();
        browser.execute(function() { window.scrollBy(0, 500); }, [])
        originalCard.waitForElementVisible('@episodeTag_first',2000)
            .clickOnEpisodeTag_first();
        homePage.assertSignUpModalPresent();
    },

    /**
     * Test case is to validate that user is not able to add video tag to the feed in search page if not signed in.
     */
    'UN149 Search: Add Video Tag ': function(browser){
        var header = browser.page.header();
        var homePage = browser.page.homepage();
        var searchPage = browser.page.searchPage();
        common_mobile.launchBrowser(browser);
        common_mobile.enableCookies(browser);
        common_mobile.signOutIfSignedIn(browser);
        header.clickOnSearch()
            .enterSearchKeyword(browser.globals.videoPodcastKeyword)
            .clickOnSearch()
        browser.pause(2000)
        browser.execute(function() { window.scrollBy(0, 500); }, [])
        searchPage
            .clickOnTag_first();
        homePage.assertSignUpModalPresent();
    },

    /**
     * Test case is to validate that user is not able to add podcast tag to the feed in search page if not signed in.
     */
    'UN150 Search: Add Podcasts Tag ': function(browser){
        var header = browser.page.header();
        var homePage = browser.page.homepage();
        var searchPage = browser.page.searchPage();
        common_mobile.launchBrowser(browser);
        common_mobile.enableCookies(browser);
        common_mobile.signOutIfSignedIn(browser);
        header.clickOnSearch()
            .enterSearchKeyword(browser.globals.videoPodcastKeyword)
            .clickOnSearch()
        browser.pause(2000)
        searchPage.clickOnNavBar_Podcasts()
        browser.pause(1000)
        browser.execute(function() { window.scrollBy(0, 500); }, [])
        searchPage
            .clickOnTag_first();
        homePage.assertSignUpModalPresent();
    },

    /**
     * Test case is to validate that user is not able to add photos tag to the feed in search page if not signed in.
     */
    'UN151 Search: Add Photos Tag ': function(browser){
        var header = browser.page.header();
        var homePage = browser.page.homepage();
        var searchPage = browser.page.searchPage();
        common_mobile.launchBrowser(browser);
        header.clickOnSearch()
            .enterSearchKeyword(browser.globals.searchKeyword)
            .clickOnSearch()
        browser.pause(2000)
        searchPage.clickOnNavBar_Photos()
        browser.pause(1000)
        browser.execute(function() { window.scrollBy(0, 500); }, [])
        searchPage
            .clickOnTag_first();
        homePage.assertSignUpModalPresent();
    },
}