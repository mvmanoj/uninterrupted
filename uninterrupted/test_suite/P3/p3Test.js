var conf = require('../../../nightwatch.conf.js');
var commonMethods= require('../../common/commonMethods');

module.exports = {

    /**
     * This test case is to validate that user is able to navigate 
     * facebook page on clicking facebook icon in header
     */
    'UN 01: Header: facebook' : function navigateToFB(browser) {
        var header = browser.page.header();
        commonMethods.launchBrowser(browser);
        commonMethods.enableCookies(browser);
        header.clickOnFacebookIcon();
        commonMethods.switchAssertWindowTitle(browser, browser.globals.uninterrupted_facebook);
    },

    /**
     * This test case is to validate that user is able to navigate 
     * twitter page on clicking twitter icon in header
     */
    'UN 02: Header: twitter' : function navigateToTwitter(browser) {
        var header = browser.page.header();
        commonMethods.launchBrowser(browser);
        commonMethods.enableCookies(browser);
        header.clickOnTwitterIcon();
        commonMethods.switchAssertWindowTitle(browser, browser.globals.uninterrupted_twitter);
    },

    /**
     * This test case is to validate that user is able to navigate 
     * youtube page on clicking youtube icon in header
     */
    'UN 03: Header: youtube' : function navigateToYoutube(browser) {
        var header = browser.page.header();
        commonMethods.launchBrowser(browser);
        commonMethods.enableCookies(browser);
        header.clickOnYoutubeIcon();
        commonMethods.switchAssertWindowTitle(browser, browser.globals.uninterrupted_youtube);
    },

    /**
     * This test case is to validate that user is able to navigate 
     * instagram page on clicking instagram icon in header
     */
    'UN 04: Header: instagram' : function navigateToInstagram(browser) {
        var header = browser.page.header();
        commonMethods.launchBrowser(browser);
        commonMethods.enableCookies(browser);
        header.clickOnInstagramIcon();
        commonMethods.switchAssertWindowTitle(browser, browser.globals.uninterrupted_instagram);
    },

    /**
     * This test case is to validate that user is able to navigate to homepage
     * on clicking uninterrupted icon in header
     */
    'UN 05: Header: Navigate to Homepage' : function navigateToHomepage(browser) {
        var header = browser.page.header();
        commonMethods.launchBrowser(browser);
        commonMethods.enableCookies(browser);
        header.clickOnPodcast();
        header.clickOnHomePageLink();
        browser.refresh()
            .assert.title(browser.globals.uninterrupted_connectTitle);
    },

    /**
     * This test case is to validate that user is able to navigate 
     * facebook page on clicking facebook icon in footer
     */    
    'UN 16: Footer: facebook' : function followUsFB(browser) {
        var footer = browser.page.footer();
        commonMethods.launchBrowser(browser);
        commonMethods.enableCookies(browser);
        footer.clickOnFacebookIcon();
        commonMethods.switchAssertWindowTitle(browser, browser.globals.uninterrupted_facebook);
    },

    /**
     * This test case is to validate that user is able to navigate 
     * twitter page on clicking twitter icon in footer
     */
    'UN 17: Footer: twitter' : function followUsTwitter(browser) {
        var footer = browser.page.footer();
        commonMethods.launchBrowser(browser);
        commonMethods.enableCookies(browser);
        footer.clickOnTwitterIcon();
        commonMethods.switchAssertWindowTitle(browser, browser.globals.uninterrupted_twitter);
    },

    /**
     * This test case is to validate that user is able to navigate 
     * youtube page on clicking youtube icon in header
     */    
    'UN 18: Footer: youtube' : function followUsYoutube(browser) {
        var footer = browser.page.footer();
        commonMethods.launchBrowser(browser);
        commonMethods.enableCookies(browser);
        footer.clickOnYoutubeIcon();
        commonMethods.switchAssertWindowTitle(browser, browser.globals.uninterrupted_youtube);
    },

    /**
     * This test case is to validate that user is able to navigate 
     * instagram page on clicking instagram icon in header
     */
    'UN 19: Footer: instagram' : function followUsInstagram(browser) {
        var footer = browser.page.footer();
        commonMethods.launchBrowser(browser);
        commonMethods.enableCookies(browser);
        footer.clickOnInstagramIcon();
        commonMethods.switchAssertWindowTitle(browser, browser.globals.uninterrupted_instagram);
    },

    /**
     * This test case is to validate that user is able to sign up for newsletter in footer
     */
    'UN 20: Footer: Sign Up for Newsletter' : function newsletterSignUp(browser) {
        var footer = browser.page.footer();
        commonMethods.launchBrowser(browser);
        commonMethods.enableCookies(browser);
        footer.enterSignUpEmail(browser.globals.email_newsletter)
            .clickOnSignUpButton()
            .assertSignUpSuccessMessagePresent();
    },

    /**
     * This test case is to validate that user is not able to sign up for newsletter using invalid emailID in footer
     */
    'UN 21: Footer: Sign Up for Newsletter with Invalid email ID' : function invalidEmailNewsletterSignUp(browser) {
        var footer = browser.page.footer();
        commonMethods.launchBrowser(browser);
        commonMethods.enableCookies(browser);
        footer.enterSignUpEmail(browser.globals.invalidEmail_newsletter)
            .clickOnSignUpButton()
            .assertSuccessMessageNotPresent();
    },

    /**
     * This test case is to validate that user is able to navigate to Terms And Conditions Page in footer
     */
    'UN 22: Footer: Terms And Conditions' : function termsAndConditions(browser) {
        var footer = browser.page.footer();
        var footerNavigation = browser.page.footerNavigationPage();
        commonMethods.launchBrowser(browser);
        commonMethods.enableCookies(browser);
        footer.clickOnTermsAndConditions();
        footerNavigation.termsAndConditionsPresent();
    },

    /**
     * This test case is to validate that user is able to navigate to Privacy Page in footer
     */
    'UN 23: Footer: Privacy' : function privacy(browser) {
        var footer = browser.page.footer();
        var footerNavigation = browser.page.footerNavigationPage();
        commonMethods.launchBrowser(browser);
        commonMethods.enableCookies(browser);
        footer.clickOnPrivacy();
        footerNavigation.assertPrivacyHeading();
    },

    /**
     * This test case is to validate that user is able to navigate to Contact Popup in footer
     */
    'UN 24: Footer: Contact' : function contact(browser) {
        var footer = browser.page.footer();
        var footerNavigation = browser.page.footerNavigationPage();
        commonMethods.launchBrowser(browser);
        commonMethods.enableCookies(browser);
        footer.clickOnContact();
        footerNavigation.assertGetInTouchPopupPresent();
    },

    /**
     * This test case is to validate that user is able to navigate to Career Page in footer
     */
    'UN 25: Footer: Career' : function career(browser) {
        var footer = browser.page.footer();
        var footerNavigation = browser.page.footerNavigationPage();
        commonMethods.launchBrowser(browser);
        commonMethods.enableCookies(browser);
        footer.clickOnCareers();
        footerNavigation.assertCareersPresent();
    },

    /**
     * This test case is to validate that user is able to fill Contact Popup form in footer
     */
    'UN 26: Contact: Get in Touch' : function getInTouch(browser) {
        var footer = browser.page.footer();
        var footerNavigation = browser.page.footerNavigationPage();
        commonMethods.launchBrowser(browser);
        commonMethods.enableCookies(browser);
        footer.clickOnContact();
        footerNavigation.enterName_getInTouch(browser.globals.name_getInTouch)
            .enterPhone_getInTouch(browser.globals.phone_getInTouch)
            .enterEmail_getInTouch(browser.globals.email_getInTouch)
            .enterCompany_getInTouch(browser.globals.company_getInTouch)
            .selectInterest_getInTouch()
            .enterMessage_getInTouch(browser.globals.message_getInTouch)
            .clickOnSend_getInTouch()
            .assertCaptchaMessagePresent();
    },

    /**
     * This test case is to validate that user is not able to enter more than 250 characters 
     * in message fields of "Get In Touch" pop up
     */
    'UN 27: Get in Touch: Exceed Character in Message field' : function getInTouch_exceedMessage(browser) {
        var footer = browser.page.footer();
        var footerNavigation = browser.page.footerNavigationPage();
        commonMethods.launchBrowser(browser);
        commonMethods.enableCookies(browser);
        footer.clickOnContact();
        footerNavigation.enterMessage_getInTouch(browser.globals.exceededMessage_getInTouch)
            .assertcharactersLeftMessage(browser.globals.exceededCharMessage)
    },

    /**
     * This test case is to validate that user is not able to send details for "Get In Touch" if enters invalid email ID
     */
    'UN 28: Get in Touch: Enter invalid email ID' : function getInTouch_InvalidID(browser) {
        var footer = browser.page.footer();
        var footerNavigation = browser.page.footerNavigationPage();
        commonMethods.launchBrowser(browser);
        commonMethods.enableCookies(browser);
        footer.clickOnContact();
        footerNavigation.enterName_getInTouch(browser.globals.name_getInTouch)
            .enterPhone_getInTouch(browser.globals.phone_getInTouch)
            .enterEmail_getInTouch(browser.globals.invalidEmail_getInTouch)
            .selectInterest_getInTouch()
            .enterMessage_getInTouch(browser.globals.message_getInTouch)
            .clickOnSend_getInTouch()
            .assertCaptchaMessageNotPresent();
    },

    /**
     * This test case is to validate that user is not able to send details for "Get In Touch" if does not fill any mandatory field
     */
    'UN 29: Get in Touch: Mandatory Fields' : function getInTouch_MandatoryFields(browser) {
        var footer = browser.page.footer();
        var footerNavigation = browser.page.footerNavigationPage();
        commonMethods.launchBrowser(browser);
        commonMethods.enableCookies(browser);
        footer.clickOnContact();
        footerNavigation
            .clickOnSend_getInTouch()
            .assertCaptchaMessageNotPresent();
    },

    /**
     * This test case is to validate that user is able to navigate homepage from any page on clicking Uninterrupted logo in footer
     */
    'UN 31: Footer: Uninterrupted Logo' : function footer_uninterruptedLogo(browser){
        var header = browser.page.header();
        var footer = browser.page.footer();
        commonMethods.launchBrowser(browser);
        commonMethods.enableCookies(browser);
        header.clickOnPodcast();
        footer.clickOnUninterruptedLogo();
        browser.refresh()
            .assert.urlEquals(browser.globals.uninterrupted_url);
    },

    /**
     * This test case is to validate that User is able to open and close the embed pop up in Video page
     */
    'UN 32: Video Page: Embed the Video' : function embedVideo(browser){
        var homepage = browser.page.homepage();
        var videoPage = browser.page.videoPage();
        var currURL;
        commonMethods.launchBrowser(browser);
        commonMethods.enableCookies(browser);
        homepage.clickOnPlay_firstContainer();
        videoPage.clickOnEmbedVideo()
            .assertembedPopUpTextPresent();
        browser.url(function(result) {
            currURL = result.value;
        })
        videoPage.getAttribute('@embedPopUpLink','value',function(result){
            browser.assert.equal(currURL,result.value)
        })
    },

    /**
     * This test case is to validate that User is able to open Edit your Account sidebar
     */
    'UN 33: Feed Page: Edit Account' : function EditYourAccount(browser){
        var header = browser.page.header();
        var feedPage = browser.page.feedPage();
        commonMethods.launchBrowser(browser);
        commonMethods.enableCookies(browser);
        commonMethods.signOutIfSignedIn(browser);
        // commonMethods.connectWithFacebook_EditAccount(browser);
        commonMethods.connectWithFacebook(browser,browser.globals.facebook_emailID, browser.globals.password)
        browser.pause(2000);
        header.clickOnThreeDots()
            .clickOnEditAccount();
        browser.pause(2000)
        feedPage.assertEditAccountText(browser.globals.editAccountText);
    },

    /**
     * This test case is to validate that User is able to close Edit your Account sidebar
     */    
    'UN 34: Feed Page: Close Edit Account Sidebar' : function CloseEditAccountSidebar(browser){
        var header = browser.page.header();
        var feedPage = browser.page.feedPage();
        commonMethods.launchBrowser(browser);
        commonMethods.enableCookies(browser);
        commonMethods.signOutIfSignedIn(browser);
        commonMethods.connectWithFacebook(browser,browser.globals.facebook_emailID, browser.globals.password)
        browser.pause(2000);
        header.clickOnThreeDots()
            .clickOnEditAccount();
        feedPage.waitForElementVisible('@closeEditAccount',2000)
        feedPage.clickOnCloseEditAccount()
        browser.pause(1000)
        feedPage.assertEditAccountNotPresent();
    },

    /**
     * This test case is to validate that User is able to Remove Account
     */
    'UN 37: Feed Page: Remove Account' : function removeAccount(browser){
        var header = browser.page.header();
        var feedPage = browser.page.feedPage();
        commonMethods.launchBrowser(browser);
        commonMethods.enableCookies(browser);
        commonMethods.signOutIfSignedIn(browser);
        commonMethods.connectWithFacebook(browser,browser.globals.facebook_emailID, browser.globals.password)
        browser.pause(2000);
        header.clickOnThreeDots()
            .clickOnEditAccount();
        browser.pause(1000)
        feedPage.clickOnRemoveAccount()
            .assertRemoveAccountMsgPresent()
        feedPage.moveToElement('@removeAccountMsg',0,0,function(){
            feedPage.waitForElementVisible('@yes_removeAccount',2000)
            feedPage.moveToElement('@yes_removeAccount',0,0,function(){
                feedPage.clickOnYesRemoveAccount();
            })
            browser.refresh();
        })
    },

    /**
     * This test case is to validate that user is able to Connect with Google while connected to Facebook
     */
    'UN 38: Signed with Facebook: Connect with google' : function connectWithGoogle(browser){
        var header = browser.page.header();
        var feedPage = browser.page.feedPage();
        commonMethods.launchBrowser(browser);
        commonMethods.enableCookies(browser);
        commonMethods.signOutIfSignedIn(browser);
        commonMethods.connectWithFacebook(browser,browser.globals.facebook_emailID, browser.globals.password)
        browser.pause(2000);
        header.clickOnThreeDots()
            .clickOnEditAccount();
        feedPage.waitForElementVisible('@connectWithGoogle',2000)
            .getText('@connectWithGoogle',function(result){
            if(result.value == "CONNECTED"){
                feedPage.clickConnectWithGoogle();
            }
        })
            .clickConnectWithGoogle();
        commonMethods.loginWithGoogle(browser,browser.globals.google_emailID, browser.globals.password);
        browser.pause(5000)
        feedPage.assertGoogleConnect(browser.globals.connectedText)
        commonMethods.removeAccount(browser);
    },

    /**
     * This test case is to validate that user is able to Connect with Twitter while connected to Facebook
     */
    'UN 39: Signed with Facebook: Connect with twitter' : function connectWithTwitter(browser){
        var header = browser.page.header();
        var feedPage = browser.page.feedPage();
        commonMethods.launchBrowser(browser);
        commonMethods.enableCookies(browser);
        commonMethods.signOutIfSignedIn(browser);
        commonMethods.connectWithFacebook(browser,browser.globals.facebook_emailID, browser.globals.password)
        browser.pause(1000);
        header.clickOnThreeDots()
            .clickOnEditAccount();
        browser.pause(2000)
        feedPage.waitForElementVisible('@connectWithTwitter',2000)
            .getText('@connectWithTwitter',function(result){
            if(result.value == "CONNECTED"){
                feedPage.clickConnectWithTwitter();
            }
        })
            .clickConnectWithTwitter();
        commonMethods.loginWithTwitter(browser,browser.globals.twitter_emailID,browser.globals.password);
        browser.pause(7000)
        feedPage.assertTwitterConnect(browser.globals.connectedText)
        commonMethods.removeAccount(browser);
    },

    /**
     * This test case is to validate that user is able to Connect with Google and Twitter while connected to Facebook
     */
    'UN 40: Signed with Facebook: Connect with twitter and Google' : function connectWithTwitterGoogle(browser){
        var header = browser.page.header();
        var feedPage = browser.page.feedPage();
        commonMethods.launchBrowser(browser);
        commonMethods.enableCookies(browser);
        commonMethods.signOutIfSignedIn(browser);
        commonMethods.connectWithFacebook(browser,browser.globals.facebook_emailID, browser.globals.password)
        browser.pause(2000);
        header.clickOnThreeDots()
            .clickOnEditAccount();
        browser.pause(2000)
        feedPage.waitForElementVisible('@connectWithGoogle',2000)
            .getText('@connectWithGoogle',function(result){
            if(result.value == "CONNECTED"){
                feedPage.clickConnectWithGoogle();
            }
        })
            .getText('@connectWithTwitter',function(result){
            if(result.value == "CONNECTED"){
                feedPage.clickConnectWithTwitter();
            }
        })
            .clickConnectWithTwitter();
        commonMethods.loginWithTwitter(browser,browser.globals.twitter_emailID,browser.globals.password);
        browser.pause(2000)
        feedPage.clickConnectWithGoogle();
        commonMethods.loginWithGoogle(browser,browser.globals.google_emailID, browser.globals.password);
        browser.pause(5000)
        feedPage.assertTwitterConnect(browser.globals.connectedText)
            .assertGoogleConnect(browser.globals.connectedText)
        commonMethods.removeAccount(browser);
    },

    /**
     * This test case is to validate that user is able to sign out from account
     */
    'UN 41: Signed In: Sign out' : function signout(browser){
        var header = browser.page.header();
        var feedPage = browser.page.feedPage();
        commonMethods.launchBrowser(browser);
        commonMethods.enableCookies(browser);
        commonMethods.signOutIfSignedIn(browser);
        // commonMethods.connectWithFacebook_EditAccount(browser);
        commonMethods.connectWithFacebook(browser,browser.globals.facebook_emailID, browser.globals.password)
        browser.pause(2000);
        header.clickOnThreeDots()
            .clickOnSignOut();
    },
}