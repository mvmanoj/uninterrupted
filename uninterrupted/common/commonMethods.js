var uninterrupted_url = "https://www.uninterrupted.com/?automatedTest=true";
// var uninterrupted_url = "https://un1-dev-pr-376.herokuapp.com";
// var uninterrupted_url = "https://un1-dev.herokuapp.com";
module.exports = {

    getJwPlayerState: function () {
        return jwplayer().getState();
    },

    getJwPlayerMuteState: function () {
        return jwplayer().getMute();
    },

    getJwPlayerFullScreenState: function () {
        return jwplayer().getFullscreen();
    },

    getJwPlayerQuality: function () {
        return jwplayer().getQualityLevels();
    },

    getJwPlayerPlaylist: function () {
        return jwplayer().getPlaylist();
    },

    getJwPlayerDuration: function () {
        return jwplayer().getDuration();
    },

    getJwPlayerPosition: function(){
        return jwplayer().getPosition();
    },

    getJwPlayerVolume: function(){
        return jwplayer().getVolume();
    },
    
    upperCaseFirstLetter: function (word) {
        return word.charAt(0).toUpperCase() + word.slice(1);
    },

    lowerCaseAllWordsExceptFirstLetters: function (string) {
        return string.replace(/\w\S*/g, function (word) {
            return word.charAt(0) + word.slice(1).toLowerCase();
        });
    },
    signOutIfSignedIn: function(browser) {
        var feedLocator = "//a[contains(text(),'Your Feed')]"
        browser.pause(1000);
        browser.element('xpath',feedLocator,function (result) {
            if(result.status != -1){
                browser.page.header().clickOnThreeDots()
                    .clickOnSignOut();
                browser.pause(2000);
            }
        })
    },
    launchBrowser: function(browser){
        browser
            .windowMaximize()
            .url(uninterrupted_url);
    },
    assertVideoPlayedName: function(browser,videoName) {
        var videoPage = browser.page.videoPage();
        browser.element('xpath',"//span[@class = 'src-components-video-layout-__VideoLayout_arrow_1TWWa src-components-video-layout-__VideoLayout_up_2Eom3']",function(result){
            if (result.status != -1) {
                videoPage.clickOnUpArrow();
            }
        })
        browser.pause(500)
            .perform(function () {
            videoPage.assertVideoName(videoName);
        });
    },
    connectWithFacebook: function(browser, email, password){
        var homePage = browser.page.homepage()
        var feedPage = browser.page.feedPage();
        var header = browser.page.header();
        header.waitForElementVisible('@connectButton',3000)
            .clickOnConnect();
        homePage
            .clickOnFacebook();
        this.loginWithFacebook(browser, email, password);
        feedPage.waitForElementVisible('@yourFeed',10000)
    },
    connectWithGoogle: function(browser, email, password){
        var homePage = browser.page.homepage()
        var feedPage = browser.page.feedPage();
        var header = browser.page.header();
        header
            .clickOnConnect();
        homePage
            .waitForElementVisible('@connectWithUsText',2000)
            .assertConnectPopUp(browser.globals.connectUs)
            .clickOnGoogle();
        this.loginWithGoogle(browser, email, password);
        feedPage.waitForElementVisible('@yourFeed',6000)
    },
    
    connectWithTwitter: function(browser, email, password){
        var homePage = browser.page.homepage();
        var header = browser.page.header();
        var feedPage = browser.page.feedPage();
        browser.pause(1000)
        header.waitForElementVisible('@connectButton',2000)
            .clickOnConnect();
        browser.pause(1000)
        homePage
            .clickOnTwitter();
        browser.pause(4000)
        this.loginWithTwitter(browser, email, password);
        browser.pause(4000)
        feedPage.waitForElementVisible('@yourFeed',10000)
    },
    enableCookies: function(browser){
        podcastCardPage = browser.page.podcastCardPage();
        browser.element('xpath',"//button[contains(text(),'Ok')]",function(result){
            if (result.status != -1) {
                podcastCardPage.clickOnOkCookies();
            }
        });
    },
    upArrowVideoPage: function(browser){
        videoPage = browser.page.videoPage();
        browser.element('xpath',"//span[@class = 'src-components-video-layout-__VideoLayout_arrow_1TWWa src-components-video-layout-__VideoLayout_up_2Eom3']",function(result){
            if (result.status != -1) {
                videoPage.clickOnUpArrow();
            }
        })
    },
    assertTwitterLoginPopup: function(browser){
        var twitterLogin = browser.page.twitterLoginPage()
        browser.getTitle(function(title){
            browser
                .window_handles(function (result) {
                var handle = result.value[1];
                browser.switchWindow(handle)
                twitterLogin.waitForElementVisible('@loginAndTweet',10000)
                twitterLogin.assertEmailIDVisible()
                    .assertPasswordVisible();
                browser.closeWindow();
                browser.switchWindow(result.value[0]);
            })
            .assert.title(title);
        })
    },
    assertFacebookLoginPopup: function(browser){
        var facebookLogin = browser.page.facebookLoginPage();
        browser.getTitle(function(title){
            browser
                .window_handles(function (result) {
                var handle = result.value[1];
                browser.switchWindow(handle)
                facebookLogin.waitForElementVisible('@loginButton',10000)
                facebookLogin.assertEmailIDVisible()
                    .assertPasswordVisible();
                browser.closeWindow();
                browser.switchWindow(result.value[0]);
            })
        })
    },
    assertFacebookShareContent: function(browser, expectedVal){
        var postToFacebook = browser.page.postToFacebookPage();
        var actualVal;
        browser
            .window_handles(function (winHandle) {
            var handle = winHandle.value[1];
            browser.switchWindow(handle);
            postToFacebook.waitForElementVisible('@postButton',5000)
            browser.perform(function(){
                browser.pause(1000);
                postToFacebook.getText('@content',function(result){
                    var input = result.value;
                    actualVal = input.toLowerCase();
                });
                browser.perform(function(){
                    if (actualVal != "uninterrupted - home") {
                        browser.assert.equal(actualVal,expectedVal)
                    }
                });
            });
            browser.closeWindow();
            browser.switchWindow(winHandle.value[0]);
        });
    },
    assertTwitterTweetContent: function(browser){
        tweet = browser.page.tweetPage();
        var currURL
        browser.url(function(result){
            currURL = result.value;
        })
        browser
            .window_handles(function (winHandle) {
            var handle = winHandle.value[1];
            browser.switchWindow(handle)
                .pause(7000)
                .perform(function(){
                tweet.assertContent(currURL);
            })
            browser.closeWindow();
            browser.switchWindow(winHandle.value[0]);
        });
    },

    switchAssertWindowTitle(browser, title){
        browser
        .window_handles(function (winHandle) {
            var handle = winHandle.value[1];
            browser.switchWindow(handle);
            browser.assert.title(title);
            browser.closeWindow()
            .switchWindow(winHandle.value[0]);
        })
    },

    loginWithFacebook: function(browser, email, password){
        facebookLogin = browser.page.facebookLoginPage();
        browser
            .pause(8000)
            .window_handles(function (handle) {
            var winHandle = handle.value[1];
            browser.switchWindow(winHandle);
            browser.element('css selector','#u_0_0',function(result){
                if (result.status != -1){
                    facebookLogin
                        .enterUserCredentials(email, password)
                        .login();
                }
            })
            browser
                .switchWindow(handle.value[0]);
        })
    },

    loginWithGoogle: function(browser, email, password){
        googleLogin = browser.page.googleLoginPage();
        browser.pause(6000);
        browser.window_handles(function (result) {
            var handle = result.value[1];
            browser.switchWindow(handle)
            browser.element('css selector', '#identifierId', function(result){
                if (result.status != -1){
                    googleLogin.waitForElementVisible('@emailID_Google',6000)
                    browser.assert.title(browser.globals.title_google);
                    googleLogin
                        .enterEmail(email)
                        .clickOnEmail_NextButton();
                    googleLogin.enterPassword(password)
                        .clickOnPass_NextButton();
                }
            })
            browser
            .switchWindow(result.value[0]);
        })
    },

    loginWithTwitter: function(browser,email, password){
        twitterLogin = browser.page.twitterLoginPage();
        browser
            .pause(5000);
        browser.window_handles(function (handle) {
            var winHandle = handle.value[1];
            browser.switchWindow(winHandle);
            browser.element('css selector',"#username_or_email",function(result){
                if (result.status != -1){
                    twitterLogin
                        .enterUserCredentials(email, password)
                        .login();
                }
            });
            browser
                .switchWindow(handle.value[0]);
        });
    },

    removeAccount: function(browser) {
        var header = browser.page.header();
        var feedPage = browser.page.feedPage();
        browser.pause(2000)
        feedPage.clickOnRemoveAccount()
        browser.pause(2000)
        feedPage.moveToElement('@removeAccountMsg',0,0,function(){
            feedPage.waitForElementVisible('@yes_removeAccount',2000)
            feedPage.moveToElement('@yes_removeAccount',0,0,function(){
                feedPage.clickOnYesRemoveAccount();
            })
            browser.refresh();
        })
    },

    convertToTAP: function(content){
        var reportContent = JSON.parse(content);
        var contentReturn = "";
        var err =0;
        var testName = reportContent.modules;
        var errMessages = reportContent.errmessages;
        Object.keys(testName).forEach(function(key){
            var objCompleted = testName[key].completed;
            var objSkipped = testName[key].skipped;
            var completed, skipped, result;
            if(Object.keys(objCompleted).length == 0 && Object.keys(objSkipped).length == 0) {
                contentReturn = contentReturn.concat("1.."+0 +" # Skipped: "+key+"\n");
            } else {
                contentReturn = contentReturn.concat("1.."+  (Object.keys(objCompleted).length + Object.keys(objSkipped).length)+" # TODO :"+key+"\n");
                var completedIndex = 1;
                Object.keys(objCompleted).forEach(function(key){
                    completed = JSON.stringify(objCompleted[key]);
                    completed = JSON.parse(completed)
                    if((completed.failed == 0 || isNaN(completed.failed)) && (completed.errors == 0) || isNaN(completed.errors)) {
                        contentReturn = contentReturn.concat("ok " + completedIndex++  +  " - " + key+"\n");
                    }
                    else {
                        var failureReason = "";
                        var assertionFailed = objCompleted[key].assertions;
                        Object.keys(assertionFailed).forEach(function(result){
                            if (assertionFailed[result].failure != false) {
                                failureReason = assertionFailed[result].failure;
                            }
                        })
                        if(completed.errors != 0) {
                            var failureRsn = errMessages[err];
                            position =  failureRsn.split("\n", 3).join("\n").length;
                            failureReason = failureRsn.slice(0, position);
                            err = err+ completed.errors;
                        }
                        contentReturn = contentReturn.concat("not ok " + completedIndex++  +  " - " + key+" # TODO :"+failureReason+"\n");
                    }
                });
            }
        })
        return contentReturn;
    }
}
