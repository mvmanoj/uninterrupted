var commonMethods= require('../common/commonMethods');
var uninterrupted_url = "https://www.uninterrupted.com/?automatedTest=true";
// var uninterrupted_url = "https://un1-dev-pr-376.herokuapp.com";
// var uninterrupted_url = "https://un1-dev.herokuapp.com";
module.exports = {

    signOutIfSignedIn: function(browser) {
        var hamburger = "(//*[@class= 'icon icon-dots-three-vertical'])[1]"
        browser.pause(1000);
        browser.element('xpath',hamburger,function (result) {   
            if(result.status != -1){
                browser.page.header().clickOnHamburger()
                    .clickOnSignOut_mob();
                browser.pause(2000);
            }
        })
    },
    launchBrowser: function(browser){
        browser
            .resizeWindow(414, 736)
            .url(uninterrupted_url);
    },
    assertVideoPlayedName: function(browser,videoName) {
        var videoPage = browser.page.videoPage();
        browser.perform(function () {
            videoPage.assertVideoName(videoName);
        });
    },
    connectWithGoogle: function(browser,email,password){
        var homePage = browser.page.homepage()
        var header = browser.page.header();
        header.waitForElementVisible('@connectIcon_mobile',2000)
            .clickOnConnectIcon_mobile();
        homePage
            .clickOnGoogle_mob();
        commonMethods.loginWithGoogle(browser,email,password);
        header.waitForElementVisible('@hamburger_mob',6000)
    },
    connectWithFacebook: function(browser, email, password){
        var homePage = browser.page.homepage()
        var header = browser.page.header();
        header.waitForElementVisible('@connectIcon_mobile',4000)
            .clickOnConnectIcon_mobile();
        homePage
            .clickOnFacebook_mob();
        commonMethods.loginWithFacebook(browser, email, password);
        header.waitForElementVisible('@hamburger_mob',6000);
    },
    connectWithTwitter: function(browser,email,password){
        var homePage = browser.page.homepage();
        var header = browser.page.header();
        header.waitForElementVisible('@connectIcon_mobile',2000)
            .clickOnConnectIcon_mobile();
        homePage
            .clickOnTwitter_mob();
        commonMethods.loginWithTwitter(browser,email,password);
        header.waitForElementVisible('@hamburger_mob',6000)
    },

    enableCookies: function(browser){
        podcastCardPage = browser.page.podcastCardPage();
        browser.element('xpath',"//button[contains(text(),'Ok')]",function(result){
            if (result.status != -1) {
                podcastCardPage.clickOnOkCookies();
            }
        });
    },

    closeFeedPage: function(browser){
        browser.pause(1500)
        feedPage = browser.page.feedPage();
        browser.element('xpath',"(//div[@class = 'src-components-sidebar-__Sidebar_close__icon_22c1a'])",function(result){
            if (result.status != -1) {
                feedPage.clickOnCloseEditAccount();
            }
        });
    },
}
