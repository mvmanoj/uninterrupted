var footer = {
    termsAndConditionsPresent: function(){
        return this
            .waitForElementVisible('@termsAndConditions')
            .assert.elementPresent('@termsAndConditions')
    },
    assertPrivacyHeading: function(){
        return this
            .waitForElementVisible('@privacy')
            .assert.elementPresent('@privacy')
    },
    assertCareersPresent: function(){
        return this
            .waitForElementVisible('@careers')
            .assert.elementPresent('@careers')
    },
    assertGetInTouchPopupPresent: function() {
        return this
            .waitForElementVisible('@getInTouchPopup')
            .assert.elementPresent('@getInTouchPopup')
    },
    enterName_getInTouch: function(name) {
        return this
            .waitForElementVisible('@name_getInTouch')
            .setValue('@name_getInTouch',name)
    },
    enterPhone_getInTouch: function(phone) {
        return this
            .waitForElementVisible('@phone_getInTouch')
            .setValue('@phone_getInTouch',phone)
    },
    enterEmail_getInTouch: function(email) {
        return this
            .waitForElementVisible('@email_getInTouch')
            .setValue('@email_getInTouch',email)
    },
    enterCompany_getInTouch: function(company) {
        return this
            .waitForElementVisible('@company_getInTouch')
            .setValue('@company_getInTouch',company)
    },
    selectInterest_getInTouch: function(){
        return this
            .waitForElementVisible('@interest_getInTouch')
            .click('@interest_getInTouch')
            .waitForElementVisible('option[value="others"]')
            .click('option[value="others"]')   
    },
    enterMessage_getInTouch: function(message) {
        return this
            .waitForElementVisible('@message_getInTouch')
            .setValue('@message_getInTouch',message)
    },
    clickOnSend_getInTouch: function() {
        return this
            .waitForElementVisible('@send_getInTouch')
            .click('@send_getInTouch')
    },
    assertCaptchaMessagePresent: function() {
        return this
            .waitForElementVisible('@captchaMessage')
            .assert.elementPresent('@captchaMessage')
    },
    assertCaptchaMessageNotPresent: function() {
        return this
            .assert.elementNotPresent('@captchaMessage')
    },
    assertcharactersLeftMessage: function(message) {
        return this
            .waitForElementPresent('@charactersLeftMessage')
            .assert.containsText('@charactersLeftMessage', message)
    }
   
}

module.exports = {
    commands: [footer],
    elements: {
        termsAndConditions: {
            selector: "//div[text()  = 'Terms and Conditions']",
            locateStrategy: "xpath"
        },
        privacy: {
            selector: "//div[text()  = 'Privacy policy']",
            locateStrategy: "xpath"
        },
        careers: {
            selector: "//div[text()  = 'Careers']",
            locateStrategy: "xpath"
        },
        getInTouchPopup: {
            selector: "//div[@class  = 'src-components-dialog-__Dialog_content_1c8lB']",
            locateStrategy: "xpath"
        },

        // Get In Touch
        name_getInTouch: {
            selector: "(//input[@name = 'name'])",
            locateStrategy: "xpath"
        },
        phone_getInTouch: {
            selector: "(//input[@name = 'phone'])",
            locateStrategy: "xpath"
        },
        email_getInTouch: {
            selector: "(//input[@placeholder = 'Email'])",
            locateStrategy: "xpath"
        },
        company_getInTouch: {
            selector: "//input[@name = 'company']",
            locateStrategy: "xpath"
        },
        interest_getInTouch: {
            selector: "//select[@name = 'interest']",
            locateStrategy: "xpath"
        },
        message_getInTouch: {
            selector: "(//textarea[@placeholder = 'Message'])",
            locateStrategy: "xpath"
        },
        send_getInTouch: {
            selector: "(//div[text()= 'Send' and @data-primary= 'true'])",
            locateStrategy: "xpath"
        },
        captchaMessage: {
            selector: "//p[text() = 'Please complete the captcha challenge']",
            locateStrategy: "xpath"
        },
        charactersLeftMessage: {
            selector: "(//div[@class = 'src-components-form-__Grid_item_3TMx5']//span)[2]",
            locateStrategy: "xpath"
        }
    }
}