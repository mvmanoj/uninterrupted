var Originals = {
    assertHeading: function (heading) {
        return this
            .waitForElementVisible('@headingText')
            .assert.containsText('@headingText',heading);
    },
    clickOnOriginalCard_first: function(){
        return this
            .waitForElementVisible('@originalCard_first')
            .click('@originalCard_first');
    },
    clickOnOriginalCard_sec: function(){
        return this
            .waitForElementVisible('@originalCard_sec')
            .click('@originalCard_sec');
    },
    clickOnLoadMore: function(){
        return this
            .click('@loadMore');
    },
    assertLoadMoreBgColor: function(color){
        return this
            .assert.cssProperty('@loadMore','background-color',color)
    },
    assertlastOriginalCard_page2Present: function(){
        return this
            .assert.elementPresent('@lastOriginalCard_page2')
    }
}

module.exports = {

    commands: [Originals],
    elements: {
        headingText: {
            selector: "(//div[@class='src-components-heading-__Heading_text_Ub-4e'])[1]",
            locateStrategy: "xpath"
        },
        originalCard_first: {
            selector: "(//div[@class = 'src-components-originals-__Card_backdrop_20u2U'])[1]",
            locateStrategy: "xpath"
        },
        originalCard_sec: {
            selector: "(//div[@class = 'src-components-originals-__Card_backdrop_20u2U'])[2]",
            locateStrategy: "xpath"
        },
        originalCardName_first: {
            selector: "(//div[@class = 'src-components-originals-__Card_athlete_sGFCY'])[1]/child::span",
            locateStrategy: "xpath"
        },
        lastOriginalCard_page1: {
            selector: "(//div[@class = 'src-components-originals-__Card_backdrop_20u2U'])[15]",
            locateStrategy: "xpath"
        },
        lastOriginalCard_page2: {
            selector: "(//div[@class = 'src-components-originals-__Card_backdrop_20u2U'])[21]",
            locateStrategy: "xpath"
        },
        loadMore: {
            selector: "(//div[text()='Load More'])[1]",
            locateStrategy: "xpath"
        }
    }
}