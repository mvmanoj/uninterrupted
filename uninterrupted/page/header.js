var header = {
    clickOnConnect: function () {
        return this
            .waitForElementVisible('@connectButton',1000)
            .click('@connectButton')
    },
    clickOnHomePageLink: function () {
        return this
            .waitForElementVisible('@homePageLink')
            .click('@homePageLink')
    },
    clickOnPodcast: function () {
        return this
            .waitForElementVisible('@podcastLink')
            .click('@podcastLink')
    },
    clickOnAthlete: function () {
        return this
            .waitForElementVisible('@athleteLink')
            .click('@athleteLink')
    },
    clickOnOriginal: function () {
        return this
            .waitForElementVisible('@originalsLink')
            .click('@originalsLink')
    },
    clickOnSearch_header: function () {
        return this
            .waitForElementVisible('@searchLink_header')
            .click('@searchLink_header')
    },
    clickOnSearch: function () {
        return this
            .waitForElementVisible('@searchLink')
            .click('@searchLink')
    },
    assertSearchGhostText: function(ghostTextValue){
        this.getAttribute('@searchBar','placeholder',function(ghostText){
            return this.assert.equal(ghostText.value,ghostTextValue);
        })
    },
    enterSearchKeyword: function (keyword) {
        return this
            .setValue('@searchBar',keyword);
    },
    clickOnSearchBar: function(){
        return this
            .click('@searchBar');
    },
    assertSearchEnabled: function(){
        return this
            .assert.elementPresent('@searchEnabled');
    },
    assertSearchNotEnabled: function(){
        return this
            .assert.elementNotPresent('@searchEnabled');
    },
    clearSearchInput: function() {
        return this
            .click('@clearInput')
    },
    clickOnThreeDots: function(){
        return this
            .waitForElementVisible('@threeDots')
            .click('@threeDots')
    },
    clickOnEditAccount: function() {
        return this
            .waitForElementVisible('@editAccount')
            .click('@editAccount')
    },
    clickOnSignOut: function() {
        return this
            .waitForElementVisible('@signOut')
            .click('@signOut')
    },
    clickOnFacebookIcon: function(){
        return this
        .waitForElementVisible('@facebookIcon')
        .click('@facebookIcon')
    },
    clickOnTwitterIcon: function(){
        return this
        .waitForElementVisible('@twitterIcon')
        .click('@twitterIcon')
    },
    clickOnYoutubeIcon: function(){
        return this
        .waitForElementVisible('@youtubeIcon')
        .click('@youtubeIcon')
    },
    clickOnInstagramIcon: function(){
        return this
        .waitForElementVisible('@instagramIcon')
        .click('@instagramIcon')
    },

    //Mobile
    clickOnConnectIcon_mobile: function() {
        return this
            .waitForElementVisible('@connectIcon_mobile')
            .click('@connectIcon_mobile')
    },
    assertHamburger_mob: function() {
        return this
            .assert.elementPresent('@hamburger_mob');
    },
    clickOnHamburger: function() {
        return this
            .click('@hamburger_mob')
    },
    clickOnSearch_mob: function() {
        return this
            .click('@searchLink_mob');
    },
    clickOnEditAccount_mob: function() {
        return this
            .waitForElementVisible('@editAccount_mob')
            .click('@editAccount_mob')
    },
    clickOnCustomize_mob: function() {
        return this
            .waitForElementVisible('@customize_mob')
            .click('@customize_mob')
    },
    clickOnSignOut_mob: function() {
        return this
            .waitForElementVisible('@signOut_mob')
            .click('@signOut_mob')
    },
}

module.exports = {
    commands: [header],
    elements: {
        connectButton: {
            selector: "(//a[text()='Connect'])[1]",
            locateStrategy : "xpath"
        },
        homePageLink: {
            selector: "(//a[@href = '/'])[1]",
            locateStrategy: "xpath"
        },
        podcastLink: {
            selector: "//a[contains(text(),'Podcasts')]",
            locateStrategy: "xpath"
        },
        athleteLink: {
            selector: "//a[contains(text(),'Athletes')]",
            locateStrategy: "xpath"
        },
        originalsLink: {
            selector: "//a[contains(text(),'Originals')]",
            locateStrategy: "xpath"
        },
        searchLink_header: {
            selector: "(//*[@class = 'icon icon-search'])[2]",
            locateStrategy: "xpath"
        },
        searchLink: {
            selector: "(//*[@class = 'icon icon-search'])[1]",
            locateStrategy: "xpath"
        },
        searchBar: {
            selector: "//input[@class='src-components-search-bar-__SearchBar_searchBar__input_2d72T']",
            locateStrategy: "xpath"
        },
        searchEnabled: {
            selector: "//a[@class = 'src-components-search-bar-__SearchBar_searchBar__searchIcon_iZLpS']",
            locateStrategy: "xpath"
        },
        clearInput: {
            selector: "//*[@class ='icon icon-close-30']",
            locateStrategy: "xpath"
        },
        threeDots: {
            selector: "(//*[@class= 'icon icon-dots-three-vertical'])[2]",
            locateStrategy: "xpath"
        },
        editAccount: {
            selector: "//a[@class = 'src-components-header-__Header_dropdown__item_2olJm' and text() ='Edit account']",
            locateStrategy: "xpath"
        },
        signOut: {
            selector: "//a[@class = 'src-components-header-__Header_dropdown__item_2olJm' and text() ='Sign out']",
            locateStrategy: "xpath"
        },
        facebookIcon: {
            selector: "(//a[@href='https://www.facebook.com/Uninterrupted' and @class = 'src-components-social-bar-__Item_host_3EfCs'])[1]",
            locateStrategy: "xpath"
        },
        twitterIcon: {
            selector: "(//a[@href='https://twitter.com/uninterrupted' and @class = 'src-components-social-bar-__Item_host_3EfCs'])[1]",
            locateStrategy: "xpath"
        },
        youtubeIcon: {
            selector: "(//a[@href='https://www.youtube.com/uninterrupted?sub_confirmation=1' and @class = 'src-components-social-bar-__Item_host_3EfCs'])[1]",
            locateStrategy: "xpath"
        },
        instagramIcon: {
            selector: "(//a[@href='https://www.instagram.com/uninterrupted' and @class = 'src-components-social-bar-__Item_host_3EfCs'])[1]",
            locateStrategy: "xpath"
        },

        //Mobile
        connectIcon_mobile: {
            selector: "//*[@class= 'icon icon-user']",
            locateStrategy : "xpath"
        },
        hamburger_mob: {
            selector: "(//*[@class= 'icon icon-user'])[1]",
            locateStrategy: "xpath"
        },
        searchLink_mob: {
            selector: "//*[@class = 'icon icon-search']",
            locateStrategy: "xpath"
        },
        editAccount_mob: {
            selector: "//a[@class = 'src-components-mobile-menu-__MobileMenu_menu__item_1ldKd' and text() ='Edit Account']",
            locateStrategy: "xpath"
        },
        customize_mob: {
            selector: "//div[@class = 'src-components-menu-item-__MenuItem_mobile_BLZyE' and text() ='Customize']",
            locateStrategy: "xpath"
        },
        signOut_mob: {
            selector: "//a[@class = 'src-components-mobile-menu-__MobileMenu_menu__item_1ldKd' and text() ='Sign Out']",
            locateStrategy: "xpath"
        }
    }
}