var connectWithTwitter = {
    login: function () {
        return this.waitForElementVisible('@loginButton',100)
            .click('@loginButton')
    },
    enterUserCredentials: function (username,password) {
        return this.setValue('@emailID_twitter',username)
            .setValue('@password_twitter',password)

    },
    assertEmailIDVisible: function(){
        return this
            .waitForElementVisible('@emailID_twitter')
            .isVisible('@emailID_twitter');
    },
    assertPasswordVisible: function(){
        return this
            .waitForElementVisible('@password_twitter')
            .isVisible('@password_twitter');
    }
}

module.exports = {
    commands: [connectWithTwitter],
    elements: {
        emailID_twitter: {
            selector: "#username_or_email"
        },
        password_twitter: {
            selector: "#password"
        },
        loginButton: {
            // selector: "//input[@type= 'submit' and @class= 'submit button selected']",
            selector: "(//input[@type= 'submit'])[1]",
            locateStrategy: "xpath"
        },
        loginAndTweet: {
            selector: "//input[@type= 'submit' and @value = 'Log in and Tweet']",
            locateStrategy: "xpath"
        }
    }
}