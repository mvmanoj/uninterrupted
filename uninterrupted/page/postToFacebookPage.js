var postToFacebook = {
    clickOnPost: function () {
        return this
            .waitForElementVisible('@postButton')
            .click('@postButton')
    },
    assertContent: function(expectedData) {
        return this
            .assert.containsText('@content',expectedData)
    }
}

module.exports = {
    commands: [postToFacebook],
    elements: {
        postButton: {
            selector: "#u_0_1u"
        },
        content: {
            selector: "//div[@class = 'mbs _6m6 _2cnj _5s6c']",
            locateStrategy: "xpath"
        }
    }
}