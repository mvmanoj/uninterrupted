
var  podcastCardPage= {

    clickOnShare_facebook: function () {
        return this
            .waitForElementVisible('@share_facebook','100')
            .click('@share_facebook');
    },
    clickOnTweetButton: function () {
        return this
            .waitForElementVisible('@tweetButton','1000')
            .click('@tweetButton');
    },
    clickOnSubscribeButton: function () {
        return this
            .waitForElementVisible('@subscribeButton','100')
            .click('@subscribeButton');
    },
    clickOnOkCookies: function () {
        return this
            .waitForElementVisible('@cookiesButton')
            .click('@cookiesButton');
    },
    clickOnUncheckedShowTag: function () {
        return this
            .waitForElementVisible('@showTagUnchecked')
            .click('@showTagUnchecked');
    },
    clickOnCheckedShowTag: function () {
        return this
            .waitForElementVisible('@showTagChecked')
            .click('@showTagChecked');
    },
    clickOnShowTag: function () {
        return this
            .waitForElementVisible('@showTagName')
            .click('@showTagName');
    },
    clickonEpisodeTag: function () {
        return this
            .waitForElementVisible('@episodeTagName')
            .click('@episodeTagName');
    },
    assertPodcast: function (podcastName) {
        return this
            .waitForElementPresent('@PodcastNameHeading',1000)
            .assert.containsText('@PodcastNameHeading', podcastName)
    },
    clickOnHeadphone_first: function() {
        return this
            .waitForElementVisible('@headphone_first')
            .click('@headphone_first');
    },
    clickOnHeadphone_sec: function() {
        return this
            .waitForElementVisible('@headphone_sec')
            .click('@headphone_sec');
    },
    clickOnfacebook_first: function(){
        return this
            .waitForElementVisible('@facebook_first',100)
            .click('@facebook_first');
    },
    assertFacebookIcon_firstColor: function(color){
        return this
            .assert.cssProperty('@facebookIcon_first','fill',color)
    },
    clickOntwitter_first: function(){
        return this
            .waitForElementVisible('@twitter_first',100)
            .click('@twitter_first');
    },
    assertTwitterIcon_firstColor: function(color){
        return this
            .assert.cssProperty('@twitterIcon_first','fill',color)
    },
    clickOnLoadMore: function(){
        return this
            .click('@loadMoreButton');
    },

    //Run Down
    clickOnRunDownIcon: function(){
        return this
            .click('@rundownIcon');
    },
    assertRunDownPlaylistPresent: function(){
        return this
            .assert.elementPresent('@runDownPlaylist');
    },
    assertRunDownPlaylistNotPresent: function(){
        return this
            .assert.elementNotPresent('@runDownPlaylist');
    },
    clickOnRundownEpisode: function(){
        return this
            .click('@rundownEpisode');
    },
    assertRunDownIconPresent: function(){
        return this
            .assert.elementPresent('@rundownIcon')
    },
    assertRunDownIconNotPresent: function(){
        return this
            .assert.elementNotPresent('@rundownIcon')
    },
    clickOnRundownIcon_sticky: function(){
        return this
            .click('@rundownIcon_sticky')
    },
    assertRunDownPlaylist_stickyPresent: function(){
        return this
            .assert.elementPresent('@runDownPlaylist_sticky');
    },
    assertRunDownPlaylist_stickyNotPresent: function(){
        return this
            .assert.elementNotPresent('@runDownPlaylist_sticky');
    },
    assertathlete: function(athleteFeat){
        return this
            .assert.containsText('@athleteFeat',athleteFeat);
    },




    //Mobile
    clickOnSubscribeButton_mob: function () {
        return this
            .waitForElementVisible('@subscribeButton_mob','100')
            .click('@subscribeButton_mob');
    },
}
module.exports = {
    commands: [podcastCardPage],
    elements: {
        share_facebook: {
            selector: "(//*[text()='Share'])[1]",
            locateStrategy: "xpath"
        },
        tweetButton: {
            selector: "(//div[text()='Tweet'])[1]",
            locateStrategy: "xpath"
        },
        subscribeButton: {
            selector: "(//div[text()='Subscribe'])[1]",
            locateStrategy: "xpath"
        },
        cookiesButton: {
            selector: "//button[contains(text(),'Ok')]",
            locateStrategy: "xpath"
        },
        showTagUnchecked: {
            selector: "//div[@class = 'src-components-card-label-__CardLabelAnimated_tag_1ssof    '  and @data-type = 'shows']",
            locateStrategy: "xpath"
        },
        showTagChecked: {
            selector: "(//div[@class = 'src-components-card-label-__CardLabelAnimated_tag_1ssof src-components-card-label-__CardLabelAnimated_active_3SfBg   '  and @data-type = 'shows'])",
            locateStrategy: "xpath"
        },
        PodcastNameHeading: {
            selector: "(//div[@class='src-components-heading-__Heading_text_Ub-4e'])[1]",
            locateStrategy: "xpath"
        },
        podcastDetails: {
            selector: "//div[@class = 'src-components-podcast-series-hero-__PodcastSeriesHero_bodyText_2GEVO']",
            locateStrategy: "xpath"
        },
        totalNoOfEpisode: {
            selector: "//li[(text()='Episodes')]",
            locateStrategy: "xpath"
        },
        lastEpisodeOnPage: {
            selector: "(//p[@class='src-components-podcast-episode-list-__PodcastEpisode_meta__title_2-eH5'])",
            locateStrategy: "xpath"
        },
        firstEpisodeOnPage: {
            selector: "(//p[@class='src-components-podcast-episode-list-__PodcastEpisode_meta__title_2-eH5'])[1]",
            locateStrategy: "xpath"
        },
        lastEpisodeOnPage2: {
            selector: "(//p[@class='src-components-podcast-episode-list-__PodcastEpisode_meta__title_2-eH5'])[30]",
            locateStrategy: "xpath"
        },
        showTagName: {
            selector: "(//div[@class='src-components-card-label-__CardLabelAnimated_text_16gHw'])[1]",
            locateStrategy: "xpath"
        },
        tag_show: {
            selector: "(//div[@class ='src-components-card-label-__CardLabelAnimated_text_16gHw'])[1]/parent::div[1]",
            locateStrategy: "xpath"
        },
        episodeTagName: {
            selector: "(//div[@class ='src-components-card-label-__CardLabelAnimated_text_16gHw'])[2]",
            locateStrategy: "xpath"
        },
        tag_episode: {
            selector: "(//div[@class ='src-components-card-label-__CardLabelAnimated_text_16gHw'])[2]/parent::div[1]",
            locateStrategy: "xpath"
        },
        playedPodcast_Sticky: {
            selector: "//a[@class = 'src-components-sticky-player-__StickyPlayer_currentlyPlayingTitleLink_6lQ2r']",
            locateStrategy: "xpath"
        },
        stickyPlayerState: {
            selector: "//button[@class = 'src-components-podcast-player-__Player_toggleButton_2IVvk']/div/child::*/child::*",
            locateStrategy: "xpath"
        },
        headphone_first: {
            selector: "(//div[@class = 'src-components-podcast-episode-list-__PodcastEpisode_thumbnail__icon_138_r'])[1]",
            locateStrategy: "xpath"
        },
        headphoneIcon_first: {
            selector: "((//div[@class = 'src-components-podcast-episode-list-__PodcastEpisode_thumbnail__icon_138_r'])[1]//child::*)[3]",
            locateStrategy: "xpath"
        },
        episodeName_first: {
            selector: "(//p[@class = 'src-components-podcast-episode-list-__PodcastEpisode_overview__title_35KFT'])[1]",
            locateStrategy: "xpath"
        },
        headphone_sec: {
            selector: "(//div[@class = 'src-components-podcast-episode-list-__PodcastEpisode_thumbnail__icon_138_r'])[2]",
            locateStrategy: "xpath"
        },
        headphoneIcon_sec: {
            selector: "((//div[@class = 'src-components-podcast-episode-list-__PodcastEpisode_thumbnail__icon_138_r'])[2]//child::*)[3]",
            locateStrategy: "xpath"
        },
        episodeName_sec: {
            selector: "(//p[@class = 'src-components-podcast-episode-list-__PodcastEpisode_overview__title_35KFT'])[2]",
            locateStrategy: "xpath"
        },
        facebook_first:{
            selector: "(//button[@class= 'src-components-podcast-episode-list-__PodcastEpisode_button_rmyui'])[1]",
            locateStrategy: "xpath"
        },
        facebookIcon_first:{
            selector: "(//*[@class='icon icon-facebook'])[4]",
            locateStrategy: "xpath"
        },
        twitter_first:{
            selector: "(//button[@class= 'src-components-podcast-episode-list-__PodcastEpisode_button_rmyui'])[2]",
            locateStrategy: "xpath"
        },
        twitterIcon_first:{
            selector: "(//*[@class='icon icon-twitter'])[4]",
            locateStrategy: "xpath"
        },
        podcastEpisodeID: {
            selector: "(//div[@class ='src-components-podcast-episode-list-__PodcastEpisode_meta_398Tb'])[1]/a",
            locateStrategy: "xpath"
        },
        progressBarEpisode: {
            selector:"//div[@class ='src-components-podcast-episode-list-__PodcastEpisode_meter__fill_2XFag']",
            locateStrategy: "xpath"
        },
        subscribeDropdown: {
            selector: "//ul[@class='src-components-podcast-series-hero-__PodcastSeriesHero_podcastMenu_2EIX_']",
            locateStrategy: "xpath"
        },
        applePodcastList: {
            selector:"//ul[@class = 'src-components-podcast-series-hero-__PodcastSeriesHero_podcastMenu_2EIX_']//img",
            locateStrategy: "xpath"
        },
        spotifyList: {
            selector: "//a[text() = 'Spotify']",
            locateStrategy: "xpath"
        },
        overcastList: {
            selector: "//a[text() = 'Overcast']",
            locateStrategy: "xpath"
        },
        googleplayList: {
            selector: "//a[text() = 'Google Play']",
            locateStrategy: "xpath"
        },
        loadMoreButton: {
            selector: "(//div[@class = 'src-components-button-__Button_host_2lQMs src-components-button-__Button_block_bFhbG'])[1]",
            locateStrategy: "xpath"
        },

        // Run down
        rundownIcon: {
            selector: "//*[@class='icon icon-playlist-play']",
            locateStrategy: "xpath"
        },
        runDownPlaylist: {
            selector: "//ul[@class='src-components-podcast-episode-list-__PodcastEpisode_mediaRundown_wL6rt']",
            locateStrategy: "xpath"
        },
        rundownEpisode: {
            selector: "(//p[text()='The Inauguration']/parent::div/parent::div//following-sibling::div)[1]",
            locateStrategy: "xpath"
        },
        rundownIcon_sticky: {
            selector: "//a[@href='#none']",
            locateStrategy: "xpath"
        },
        runDownPlaylist_sticky: {
            selector: "//ul[@class='src-components-sticky-player-__StickyPlayerV2_mediaRundownList_3VTS6']",
            locateStrategy: "xpath"
        },

        //

        athleteFeat: {
         //   selector: "//span[text()='Feat']/following-sibling::strong",
            selector:"(//div[text()='Feat'])[1]/child::*",
            locateStrategy: "xpath"
        },

        // Date of 1st and 2nd Episodes
        dateEpisode_last: {
            selector: "(//span[@class = 'src-components-podcast-episode-list-__PodcastEpisode_overview__date_3Wnu-'])[1]",
            locateStrategy: "xpath"
        },
        dateEpisode_secLast: {
            selector: "(//span[@class = 'src-components-podcast-episode-list-__PodcastEpisode_overview__date_3Wnu-'])[2]",
            locateStrategy: "xpath"
        },

        //Mobile

        subscribeButton_mob: {
            selector: "(//div[text()='Subscribe'])[2]",
            locateStrategy: "xpath"
        },
    }
}