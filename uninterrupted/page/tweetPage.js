var twitToTwitter = {
    assertContent: function(videoName) {
        return this
            .assert.containsText('@content',videoName)
    }
}

module.exports = {
    commands: [twitToTwitter],
    elements: {
        content: {
            selector: "//textarea[@id = 'status']",
            locateStrategy: "xpath"
        },
        shareText: {
            selector: "//h2[contains(text(), 'Share a link with your followers')]",
            locateStrategy: "xpath"
        }
    }
}