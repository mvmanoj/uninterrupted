var yourFeed = {
    assertYourFeedPresent: function () {
        return this
            .assert.elementPresent('@yourFeed')
    },
    clickOnYourFeed: function () {
        return this
            .waitForElementVisible('@yourFeed')
            .click('@yourFeed')
    },
    yourFeedPresent: function () {
        return this
            .expect.element('@yourFeed').to.be.present;
    },
    addShowTag: function() {
        return this
            .click('@showTag');
    },
    addHashTag: function() {
        return this
            .click('@hashTag');
    },
    addAthletesTag: function() {
        return this
            .click('@athletesTag');
    },
    addSportsTag: function() {
        return this
            .click('@sportsTag');
    },
    removeShowTag: function(){
        return this
            .click('@addedShowTag')
    },
    removeHashTag: function(){
        return this
            .click('@addedHashTag')
    },
    removeAthletesTag: function(){
        return this
            .click('@addedAthletesTag')
    },
    removeSportsTag: function(){
        return this
            .click('@addedSportsTag')
    },
    clickOnShowMore_Shows: function() {
        return this
            .click('@showMore_Shows')
    },
    clickOnShowMore_Hashtags: function() {
        return this
            .click('@showMore_Hashtags')
    },
    clickOnShowMore_Athletes: function() {
        return this
            .click('@showMore_Athletes')
    },
    clickOnShowMore_Sports: function() {
        return this
            .click('@showMore_Sports')
    },

    //Edit Your Account
    assertEditAccountText: function(text) {
        return this
            .assert.containsText('@editAccountText', text)
    },
    assertEditAccountNotPresent: function() {
        return this
            .assert.elementNotPresent('@editAccountText')
    },
    clickOnCloseEditAccount: function() {
        return this
            .waitForElementVisible('@closeEditAccount',1000)
            .click('@closeEditAccount');
    },
    clickOnRemoveAccount: function() {
        return this
            .click('@removeAccount')
    },
    clickOnYesRemoveAccount: function() {
        return this
            .click('@yes_removeAccount')
    },
    assertRemoveAccountMsgPresent: function() {
        return this
            .assert.elementPresent('@removeAccountMsg')
    },
    clickConnectWithFacebook: function() {
        return this
            .click('@connectWithFacebook')
    },
    clickConnectWithGoogle: function() {
        return this
            .click('@connectWithGoogle')
    },
    clickConnectWithTwitter: function() {
        return this
            .click('@connectWithTwitter')
    },
    assertFacebookConnect: function(text) {
        return this
            .assert.containsText('@connectWithFacebook', text)
    },
    assertTwitterConnect: function(text) {
        return this
            .assert.containsText('@connectWithTwitter', text)
    },
    assertGoogleConnect: function(text) {
        return this
            .assert.containsText('@connectWithGoogle', text)
    },
}

module.exports = {
    commands: [yourFeed],
    elements: {
        yourFeed: {
            selector: "//a[contains(text(),'Your Feed')]",
            locateStrategy: "xpath"
        },
        shows: {
            selector: "//div[text() = 'Shows']",
            locateStrategy: "xpath"
        },
        showTagAdded: {
            selector: "//div[@class = 'src-components-tag-selector-__Group_host_2bHWn']/child::*/child::*[contains(text(),",
            locateStrategy: "xpath"
        },
        showTag: {
            selector: "(//div[@class = 'src-components-tag-selector-__Tag_tag_Bmgxn src-components-tag-selector-__Tag_dark_5sOoL' and @data-type = 'shows'])[1]",
            locateStrategy: "xpath"
        },
        hashTag: {
            selector: "(//div[@class = 'src-components-tag-selector-__Tag_tag_Bmgxn src-components-tag-selector-__Tag_dark_5sOoL undefined' and @data-type = 'hashtags'])[1]",
            locateStrategy: "xpath"
        },
        athletesTag: {
            selector: "(//div[@class = 'src-components-tag-selector-__Tag_tag_Bmgxn src-components-tag-selector-__Tag_dark_5sOoL' and @data-type = 'athletes'])[1]",
            locateStrategy: "xpath"
        },
        sportsTag: {
            selector: "(//div[@class = 'src-components-tag-selector-__Tag_tag_Bmgxn src-components-tag-selector-__Tag_dark_5sOoL' and @data-type = 'sports'])[1]",
            locateStrategy: "xpath"
        },
        addedShowTag: {
            selector: "(//div[@class = 'src-components-tag-selector-__Tag_tag_Bmgxn src-components-tag-selector-__Tag_dark_5sOoL src-components-tag-selector-__Tag_active_R3iTJ' and @data-type = 'shows'])[1]",
            locateStrategy: "xpath"
        },
        addedHashTag: {
            selector: "(//div[@class = 'src-components-tag-selector-__Tag_tag_Bmgxn src-components-tag-selector-__Tag_dark_5sOoL src-components-tag-selector-__Tag_active_R3iTJ undefined' and @data-type = 'hashtags'])[1]",
            locateStrategy: "xpath"
        },
        addedAthletesTag: {
            selector: "(//div[@class = 'src-components-tag-selector-__Tag_tag_Bmgxn src-components-tag-selector-__Tag_dark_5sOoL src-components-tag-selector-__Tag_active_R3iTJ' and @data-type = 'athletes'])[1]",
            locateStrategy: "xpath"
        },
        addedSportsTag: {
            selector: "(//div[@class = 'src-components-tag-selector-__Tag_tag_Bmgxn src-components-tag-selector-__Tag_dark_5sOoL src-components-tag-selector-__Tag_active_R3iTJ' and @data-type = 'sports'])[1]",
            locateStrategy: "xpath"
        },
        showMore_Shows: {
            selector: "(//a[text() = 'Show More'])[1]",
            locateStrategy: "xpath"
        },
        showMore_Hashtags: {
            selector: "(//a[text() = 'Show More'])[2]",
            locateStrategy: "xpath"
        },
        showMore_Athletes: {
            selector: "(//a[text() = 'Show More'])[3]",
            locateStrategy: "xpath"
        },
        showMore_Sports: {
            selector: "(//a[text() = 'Show More'])[4]",
            locateStrategy: "xpath"
        },
        

        //Edit Your Account
        editAccountText: {
            // selector: "(//div[@class='src-components-heading-__Heading_text_Ub-4e'])[2]/div",
            selector: "//div[text() = 'Edit your account']",
            locateStrategy: "xpath"
        },
        closeEditAccount: {
            selector: "(//div[@class = 'src-components-sidebar-__Sidebar_close__icon_22c1a'])",
            locateStrategy: "xpath"
        },
        removeAccount: {
            selector: "//a[text() = 'Remove my account']",
            locateStrategy: "xpath"
        },
        yes_removeAccount: {
            selector: "//a[text() = 'Yes']",
            locateStrategy: "xpath"
        },
        removeAccountMsg: {
            selector: "//p[text() = 'Are you sure you want to remove your account?']",
            locateStrategy: "xpath"
        },
        connectWithFacebook: {
            selector: "(//div[@class = 'src-components-signup-widget-__Button_inner_3jVtz'])[1]",
            locateStrategy: "xpath"
        },
        connectWithGoogle: {
            selector: "(//div[@class = 'src-components-signup-widget-__Button_inner_3jVtz'])[2]",
            locateStrategy: "xpath"
        },
        connectWithTwitter: {
            selector: "(//div[@class = 'src-components-signup-widget-__Button_inner_3jVtz'])[3]",
            locateStrategy: "xpath"
        },
    }
}