var athletes = {
    assertHeading: function (heading) {
        return this
            .waitForElementVisible('@headingText')
            .assert.containsText('@headingText',heading)
    },
    clickOnAthleteName: function () {
        return this
            .waitForElementVisible('@athletesSelected')
            .click('@athletesSelected')
    }
}

module.exports = {
    commands: [athletes],
    elements: {
        headingText: {
            selector: "(//div[@class='src-components-heading-__Heading_text_Ub-4e'])[1]",
            locateStrategy: "xpath"
        },
        athletesSelected: {
            selector: "((//div[@class='src-components-athletes-__Card_overlay_3K2jt'])[1]//*)[1]",
            locateStrategy: "xpath"
        },
        athletesName_heading: {
            selector: "(//div[@class='src-components-heading-__Heading_text_Ub-4e'])[1]",
            locateStrategy: "xpath"
        }
    }
}