var podcasts = {
    assertHeading: function (heading) {
        return this
            .waitForElementVisible('@headingText')
            .assert.containsText('@headingText',heading);
    },
    playPodcast: function () {
        return this
            .waitForElementVisible('@headphoneIcon')
            .click('@headphoneIcon');
    },
    assertHeadphoneColor: function(color){
        return this
            .assert.cssProperty('@headphoneIcon','background-color',color)
    },
    playPodcast_sec: function(){
        return this
            .waitForElementVisible('@headphone_sec')
            .click('@headphone_sec');
    },
    assertStickyPlayerPresent: function () {
        return this
            .waitForElementVisible('@stickyPlayerField')
            .assert.elementPresent('@stickyPlayerField');
    },
    assertBgColor: function (color) {
        return this
            .assert.cssProperty('@podcastCard','background-color',color);
    },
    clickOnAllEpisodes: function () {
        return this
            .click('@allEpisodesButton');
    },
    clickOnAllEpisodes_Sec: function () {
        return this
            .click('@allEpisodesButton_Sec');
    },
    clickOnAllEpisode_OpenRun: function(){
        return this
            .click('@allEpisode_OpenRun')
    },
    clickOnAllEpisode_DrayDay: function(){
        return this
            .click('@allEpisode_DrayDay')
    },
    assertAllEpisodesBgColor: function (color) {
        return this
            .assert.cssProperty('@allEpisodesButton','background-color',color);
    },
    clickOnPodcast_Sticky: function(){
        return this
            .waitForElementVisible('@playedPodcast_Sticky')
            .click('@playedPodcast_Sticky');
    },
    clickOnPlayPause_Sticky: function(){
        return this
            .waitForElementVisible('@playPause_Sticky')
            .click('@playPause_Sticky');
    },
    clickOnVolume_Sticky: function(){
        return this
            .waitForElementVisible('@volumeOnOff_Sticky')
            .click('@volumeOnOff_Sticky');
    },
    assertPauseIconPresent_first() {
        return this
            .assert.elementPresent('@pausePodcast_first');
    },
    assertPauseIconPresent_sec(attVal) {
        return this
            .assert.attributeEquals('@playPausePodcast_sec','class',attVal);
    },
}

module.exports = {

    commands: [podcasts],
    elements: {
        headingText: {
            selector: "(//div[@class='src-components-heading-__Heading_text_Ub-4e'])[1]",
            locateStrategy: "xpath"
        },
        headphoneIcon: {
            selector: "(//div[@class='src-components-podcast-card-__PodcastCard_image__icon_2dsGV'])[1]/child::*",
            locateStrategy: "xpath"
        },
        headphone_sec: {
            selector: "(//*[@class = 'src-components-podcast-card-__PodcastCard_image__icon_2dsGV'])[2]/child::*",
            locateStrategy: "xpath"
        },
        stickyPlayerField: {
            selector: "//div[@class = 'src-components-sticky-player-__StickyPlayer_container_38ul6']",
            locateStrategy: "xpath"
        },
        podcastCard: {
            selector: "(//div[@class='src-components-podcast-card-__PodcastCard_contents_2FDsL'])[1]",
            locateStrategy: "xpath"
        },
        allEpisodesButton: {
            selector: "(//div[contains(text(),'All Episodes')])[1]",
            locateStrategy: "xpath"
        },
        allEpisodesButton_Sec: {
            selector: "(//div[contains(text(),'All Episodes')])[3]",
            locateStrategy: "xpath"
        },
        allEpisode_OpenRun: {
            selector: "//div[text() = 'Open Run']/parent::div//div[text()='All Episodes' and @data-primary]",
            locateStrategy: "xpath"
        },
        allEpisode_DrayDay: {
            selector: "//div[text() = 'Dray Day']/parent::div//div[text()='All Episodes' and @data-primary = 'true']",
            locateStrategy: "xpath"
        },
        podcastNameText: {
            selector: "(//div[@class='src-components-podcast-card-__PodcastCard_contents_2FDsL'])[1]/child::div[1]",
            locateStrategy: "xpath"
        },
        atheleteName: {
            selector: "(//div[text()='Feat'])[1]/child::*",
            locateStrategy: "xpath"
        },
        progressbarWidth_card: {
            selector: "//div[@class = 'src-components-podcast-card-__PodcastCard_progressBarTrack_U4uFk']",
            locateStrategy: "xpath"
        },
        pausePodcast_first: {
            selector: "(//div[@class = 'src-components-podcast-card-__PodcastCard_image__icon_2dsGV'])[1]/div/*",
            locateStrategy: "xpath"
        },
        playPausePodcast_sec: {
            selector: "(//div[@class = 'src-components-podcast-card-__PodcastCard_image__icon_2dsGV'])[2]/div/*",
            locateStrategy: "xpath"
        },
        cardPlayerState: {
            selector: "(//div[@class = 'src-components-podcast-card-__PodcastCard_image__icon_2dsGV'])[1]/div/child::*/child::*",
            locateStrategy: "xpath"
        },

        //Sticky Player
        playedPodcast_Sticky: {
            selector: "//a[@class = 'src-components-sticky-player-__StickyPlayer_currentlyPlayingTitleLink_6lQ2r']",
            locateStrategy: "xpath"
        },
        playPause_Sticky: {
            selector: "//button[@class = 'src-components-podcast-player-__Player_toggleButton_2IVvk']",
            locateStrategy: "xpath"
        },
        volumeOnOff_Sticky: {
            selector: "//button[@class=  'src-components-podcast-player-__Player_volumeButton_FgL_C']",
            locateStrategy: "xpath"
        },
        stickyPlayerState: {
            selector: "(//button[@class = 'src-components-podcast-player-__Player_toggleButton_2IVvk']//child::*)[3]",
            locateStrategy: "xpath"
        },
        stickyPlayerVolumeState: {
            selector: "//button[@class=  'src-components-podcast-player-__Player_volumeButton_FgL_C']/div/child::*/child::*",
            locateStrategy: "xpath"
        },
    }
}