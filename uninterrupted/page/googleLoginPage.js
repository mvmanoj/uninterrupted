var connectWithGoogle = {
    enterEmail: function (username) {
        return this
            .waitForElementVisible('@emailID_Google',1000)
            .setValue('@emailID_Google',username)
    },
    clickOnEmail_NextButton: function () {
        return this.waitForElementVisible('@email_nextButton',1000)
            .click('@email_nextButton')
    },
    enterPassword: function (password) {
        return this
            .waitForElementVisible('@password_Google',5000)
            // .clearValue('@password_Google')
            .setValue('@password_Google',password)
    },
    clickOnPass_NextButton: function () {
        return this.waitForElementVisible('@pass_nextButton',1000)
            .click('@pass_nextButton')
    },
}

module.exports = {
    commands: [connectWithGoogle],
    elements: {
        emailID_Google: {
            selector: "#identifierId"
        },
        password_Google: {
            selector: "#password > div.aCsJod.oJeWuf > div > div.Xb9hP > input"
        },
        email_nextButton: {
            selector: "#identifierNext"
        },
        pass_nextButton: {
            selector: "#passwordNext"
        }
    }
}