var connectWithFB = {
    login: function () {
        return this
            .waitForElementVisible('@loginButton',100)
            .click('@loginButton')
    },
    enterUserCredentials: function (username,password) {
        return this
            .setValue('@emailID_facebook',username)
            .setValue('@password_facebook',password);
    },
    assertEmailIDVisible: function(){
        return this
            .waitForElementVisible('@emailID_facebook')
            .isVisible('@emailID_facebook');
    },
    assertPasswordVisible: function(){
        return this
            .waitForElementVisible('@password_facebook')
            .isVisible('@password_facebook');
    }
}

module.exports = {
    commands: [connectWithFB],
    elements: {
        emailID_facebook: {
            selector: "#email"
        },
        password_facebook: {
            selector: "#pass"
        },
        loginButton: {
            selector: "#u_0_0"
        }
    }
}