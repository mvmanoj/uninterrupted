var search = {
    assertPageTitle: function(pageTitle) {
        return this
            .assert.title(pageTitle)
    },
    assertSuggestionText: function(text) {
        return this
            .assert.containsText('@suggestionList',text)
    },
    assertURLContains: function(urlKeyword) {
        return this 
            .assert.urlContains(urlKeyword)
    },
    assertNavBar_Videos: function() {
        return this
            .assert.elementPresent('@navBar_Videos')
    },
    assertNavBar_Podcasts: function() {
        return this
            .assert.elementPresent('@navBar_Podcasts')
    },
    assertNavBar_Athletes: function() {
        return this
            .assert.elementPresent('@navBar_Athletes')
    },
    assertNavBar_Photos: function() {
        return this
            .assert.elementPresent('@navBar_Photos')
    },
    assertNavBar_VideosText: function(color) {
        return this
            .assert.cssProperty('@navBar_Videos','color',color)
    },
    assertNavBar_PodcastsText: function(color) {
        return this
            .assert.cssProperty('@navBar_Podcasts','color',color)
    },
    assertNavBar_AthletesText: function(color) {
        return this
            .assert.cssProperty('@navBar_Athletes','color',color)
    },
    assertNavBar_PhotosText: function(color) {
        return this
            .assert.cssProperty('@navBar_Photos','color',color)
    },
    clickOnNavBar_Videos: function(){
        return this
            .click('@navBar_Videos')
    },
    clickOnNavBar_Podcasts: function(){
        return this
            .click('@navBar_Podcasts');
    },
    clickOnNavBar_Athletes: function(){
        return this
            .click('@navBar_Athletes');
    },
    clickOnNavBar_Photos: function(){
        return this
            .click('@navBar_Photos');
    },
    assertSelectedNavBar: function(className){
        return this
            .assert.attributeEquals('@selectedNavBar','class',className);
    },

    clickOnCard_first: function(){
        return this
            .click('@card_first');
    },
    assertPictorialName: function(pictorial){
        return this
            .waitForElementVisible('@pictorialName')
            .assert.containsText('@pictorialName',pictorial)
    },
    assertPlayBGColor: function(color){
        return this
            .assert.cssProperty('@card_first','background-color',color)
    },
    assertNoResultsTextPresent: function(){
        return this
            .assert.elementPresent('@noResultsText');
    },
    clickOnLoadMore: function(){
        return this
            .click('@loadMore')
    },
    clickOnTag_first: function(){
        return this
            .click('@addTag_first')
    }
    

}

module.exports = {
    commands: [search],
    elements: {
        itemLists: {
            selector: "//*[@class ='src-components-search-nav-__SearchNav_container_1fdmL']",
            locateStrategy: "xpath"
        },
        suggestionList: {
            selector: "//div[@class ='src-components-search-bar-__SearchBar_autocomplete__resultWrapper_3jG-8']",
            locateStrategy: "xpath"
        },
        navBar_Videos: {
            selector: "//div[@class ='src-components-container-__Container_container_K6z4w']/div[text()='Videos']",
            locateStrategy: "xpath"
        },
        navBar_Podcasts: {
            selector: "//div[@class ='src-components-container-__Container_container_K6z4w']/div[text()='Podcasts']",
            locateStrategy: "xpath"
        },
        navBar_Athletes: {
            selector: "//div[@class ='src-components-container-__Container_container_K6z4w']/div[text()='Athletes']",
            locateStrategy: "xpath"
        },
        navBar_Photos: {
            selector: "//div[@class ='src-components-container-__Container_container_K6z4w']/div[text()='Photos']",
            locateStrategy: "xpath"
        },
        selectedNavBar: {
            selector: "//div[@class ='src-components-search-nav-__SearchNav_item_3eFDa src-components-search-nav-__SearchNav_item__selected_1Km8u']",
            locateStrategy: "xpath"
        },
        card_first: {
            selector: "(//div[@class = 'src-components-search-results-__SearchResultCard_searchResultCard__icon_3XvYu'])[1]",
            locateStrategy: "xpath"
        },
        cardName_first: {
            selector: "(//p[@class = 'src-components-search-results-__SearchResultCard_overview__title_1_hnn'])[1]",
            locateStrategy: "xpath"
        },
        pictorialName: {
            selector: "//h2[@class = 'src-components-lightbox-__Lightbox_title_wrWna']",
            locateStrategy: "xpath"
        },
        noResultsText: {
            selector: "//h2[text() = 'No Results']",
            locateStrategy: "xpath"
        },
        cardList: {
            selector: "//div[@class = 'src-components-search-results-__SearchResultsGrid_host_1y09Y']/div",
            locateStrategy: "xpath"
        },
        underlinedKeyword: {
            selector: "//em[@class ='src-components-search-bar-__SearchBar_autocomplete__queryMatch_2DIaC']",
            locateStrategy: "xpath"
        },
        searchCount: {
            selector: "//div[@class = 'src-routes-__SearchResults_searchResults__resultCount_pNibV']",
            locateStrategy: "xpath"
        },
        loadMore: {
            selector: "//div[text() = 'Load More' and @data-primary]",
            locateStrategy: "xpath"
        },
        addTag_first: {
            selector: "(//div[@class = 'src-components-card-label-__CardLabelAnimated_text_16gHw'])[1]",
            locateStrategy: "xpath"
        },
        tag_card: {
            selector: "(//div[@class = 'src-components-card-label-__CardLabelAnimated_text_16gHw'])[1]/parent::div[1]",
            locateStrategy: "xpath"
        }
    }
}