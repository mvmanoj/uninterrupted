var landingPage = {

    //Signup Modal
    assertConnectPopUp: function (connectText) {
        return this
            .assert.containsText('@connectWithUsText',connectText)
    },
    assertSignUpModalNotPresent: function(){
        return this
            .assert.elementNotPresent('@signUpModal')
    },
    assertSignUpModalPresent: function(){
        return this
            .assert.elementPresent('@signUpModal')
    },
    subscribeNewsletter_connect: function(email) {
        return this
            .setValue('@emailNewsletter_connect',email)
            .click('@signUpButton_connect');
    },
    assertSuccessNewsletter_connect: function(){
        return this
            .waitForElementVisible('@successNewsletter_connect',1000)
            .assert.elementPresent('@successNewsletter_connect');
    },
    assertInvalidEmail: function() {
        return this
            .waitForElementVisible('@invalidEmail',1000)
            .assert.elementPresent('@invalidEmail');
    },
    clickOnFacebook: function () {
        return this
            .waitForElementVisible('@facebookButton','100')
            .click('@facebookButton')
    },
    clickOnGoogle: function () {
        return this
            .waitForElementVisible('@googleButton','100')
            .click('@googleButton')
    },
    clickOnTwitter: function () {
        return this
            .waitForElementVisible('@twitterButton','100')
            .click('@twitterButton')
    },

    //Carousel
    clcikOnFirstCarouselBar: function(){
        return this
            .waitForElementVisible('@firstCarouselSlide')
            .click('@firstCarouselSlide');
    },
    clickOnFirstWatch: function () {
        return this
            .waitForElementVisible('@firstWatchNowButton')
            .click('@firstWatchNowButton')
    },
    clickOnWatchNow: function(){
        return this
            .click('@watchNow')
    },
    clcikOnSecCarouselBar: function(){
        return this
            .waitForElementVisible('@secCarouselSlide')
            .click('@secCarouselSlide');
    },
    clickOnSecWatch: function () {
        return this
            .waitForElementVisible('@secWatchNowButton')
            .click('@secWatchNowButton')
    },
    clcikOnSixCarouselBar: function(){
        return this
            .waitForElementVisible('@sixCarouselSlide')
            .click('@sixCarouselSlide');
    },

    //Originals Container
    clickOnMoreOriginals: function () {
        return this
            .waitForElementVisible('@moreOriginals')
            .click('@moreOriginals')
    },
    clickOnOriginalCard: function(){
        return this
            .waitForElementVisible('@originalCard',100)
            .click('@originalCard');
    },
    clickOnWatchPreview: function(){
        return this
            .click('@watchPreview')
    },
    assertOriginalDetailsNotPresent: function(){
        return this
            .assert.elementHidden('@originalCardDetails')
    },
    assertOriginalTextNotPresent: function(nullValue){
        return this
            .assert.value('@originalCardDetails',nullValue)
    },
    clickOnRightArrow_originals: function(){
        return this
            .click('@rightArrow_originals');
    },
    clickOnLeftArrow_originals: function(){
        return this
            .click('@leftArrow_originals');
    },

    //Podcasts Container
    clickOnMorePodcasts: function () {
        return this
            .waitForElementVisible('@morePodcasts')
            .click('@morePodcasts')
    },
    clickOnAllEpisodes_podcast: function() {
        return this
            .waitForElementVisible('@allEpisodes_podcast',100)
            .click('@allEpisodes_podcast');
    },
    playPodcast_first: function(){
        return this
            .click('@headphone_first');
    },
    playPodcast_sec: function(){
        return this
            .click('@headphone_sec');
    },
    assertStickyPlayerPresent: function () {
        return this
            .waitForElementVisible('@stickyPlayerField')
            .assert.elementPresent('@stickyPlayerField');
    },
    assertStickyPlayerNotPresent: function () {
        return this
            .assert.elementNotPresent('@stickyPlayerField');
    },
    clickOnthumbnail_sticky: function(){
        return this
            .click('@thumbnail_sticky');
    },
    assertPodcastBgColor: function (color) {
        return this
            .assert.cssProperty('@podcastCard_first','background-color',color);
    },
    assertPauseIconPresent_first() {
        return this
            .assert.elementPresent('@pausePodcast_first');
    },
    assertPauseIconPresent_sec() {
        return this
            .assert.elementPresent('@pausePodcast_sec');
    },
    assertAllEpisodesBgColor: function (color) {
        return this
            .assert.cssProperty('@allEpisodes_podcast','background-color',color);
    },
    assertHeadphoneColor: function(color){
        return this
            .assert.cssProperty('@headphone_first','background-color',color);
    },
    assertMorePodcastsColor: function(color){
        return this
            .assert.cssProperty('@morePodcasts','background-color',color);
    },
    clickonProgressbar: function(){
        return this
            .click('@progressBar_sticky')
    },
    clickOnPlayPause_Sticky: function(){
        return this
            .waitForElementVisible('@playPause_Sticky')
            .click('@playPause_Sticky');
    },

    //Latest Stories Container
    clickOnPlay_latestStories: function () {
        return this
            .waitForElementVisible('@playVideo_latestStories')
            .click('@playVideo_latestStories');
    },
    clickOnTag_latestStories: function(){
        return this
            .click('@addTag_latestStories');
    },
    clickOnLoadMore_latestStories: function(){
        return this
            .click('@loadMore_latestStories');
    },


    //First Container
    clickOnPlay_firstContainer: function () {
        return this
            .waitForElementVisible('@playVideo_firstContainer')
            .click('@playVideo_firstContainer')
    },
    clickOnAddTag_firstContainer: function() {
        return this
            .waitForElementVisible('@addTag_firstContainer',100)
            .click('@addTag_firstContainer');
    },

    //Mobile

    clickOnGoogle_mob: function() {
        return this
            .click('@googleButton_mob');
    },
    clickOnFacebook_mob: function() {
        return this
            .click('@facebookButton_mob');
    },
    clickOnTwitter_mob: function() {
        return this
            .click('@twitterButton_mob');
    }   

}
module.exports = {

    commands: [landingPage],
    elements: {

        //Sign Up Modal
        connectWithUsText: {
            selector: "(//strong[text()='Connect with us'])[1]",
            locateStrategy: "xpath"
        },
        signUpModal: {
            selector: "(//div[@class = 'src-components-signup-modal-__SignupModal_overlay_image_1g5Lj1'])",
            locateStrategy: "xpath"
        },
        emailNewsletter_connect: {
            selector: "(//input[@type = 'text'])[2]",
            locateStrategy: "xpath"
        },
        signUpButton_connect: {
            selector: "(//button[text() = 'Sign Up'])[2]",
            locateStrategy: "xpath"
        },
        successNewsletter_connect: {
            selector: "//strong[text() = 'Success!']",
            locateStrategy: "xpath"
        },
        invalidEmail: {
            selector: "//div[text() = 'Invalid email']",
            locateStrategy: "xpath"
        },
        facebookButton: {
            selector: "(//div[@class = 'src-components-signup-modal-__Button_icon_2iLlK' ]/following-sibling::span[contains(text(),'Facebook')])[2]",
            locateStrategy: "xpath"
        },
        googleButton: {
            selector: "(//div[@class = 'src-components-signup-modal-__Button_icon_2iLlK' ]/following-sibling::span[contains(text(),'Google')])[2]",
            locateStrategy: "xpath"
        },
        twitterButton: {
            selector: "(//div[@class = 'src-components-signup-modal-__Button_icon_2iLlK' ]/following-sibling::span[contains(text(),'Twitter')])[2]",
            locateStrategy: "xpath"
        },

        //Carousel
        videoName_Carousel: {
            selector: "(//div[@class = 'src-components-heading-__Heading_text_Ub-4e'])[1]",
            locateStrategy: "xpath"
        },
        secVideoName_Carousel: {
            selector: "(//div[@class = 'src-components-heading-__Heading_text_Ub-4e'])[2]",
            locateStrategy: "xpath"
        },
        firstCarouselSlide: {
            selector: "(//div[@class = 'src-components-video-carousel-__VideoCarousel_slideShow_yTSdM'])/child::div[1]",
            locateStrategy: "xpath"
        },
        firstWatchNowButton: {
            selector: "(//*[@class = 'src-components-video-carousel-__Item_content_8TRFL'])[1]/child::*/child::a/div[1]",
            locateStrategy: "xpath"
        },
        watchNow: {
            selector: "(//div[text() = 'Watch Now'])[1]",
            locateStrategy: "xpath"
        },
        firstVideoPlayed_Carousel: {
            selector: "(//*[@class = 'src-components-video-carousel-__Item_content_8TRFL'])[1]/child::*/child::div",
            locateStrategy: "xpath"
        },
        firstSpanValue: {
            selector: "//*[@class = 'src-components-video-carousel-__VideoCarousel_at_2cIql']/span[1]",
            locateStrategy: "xpath"
        },
        secCarouselSlide: {
            selector: "(//div[@class = 'src-components-video-carousel-__VideoCarousel_slideShow_yTSdM'])/child::div[2]",
            locateStrategy: "xpath"
        },
        secWatchNowButton: {
            selector: "(//*[@class = 'src-components-video-carousel-__Item_content_8TRFL'])[2]/child::*/child::a/div[1]",
            locateStrategy: "xpath"
        },
        secVideoPlayed_Carousel: {
            selector: "(//*[@class = 'src-components-video-carousel-__Item_content_8TRFL'])[2]/child::*/child::div",
            locateStrategy: "xpath"
        },
        sixCarouselSlide: {
            selector: "(//div[@class = 'src-components-video-carousel-__VideoCarousel_slideShow_yTSdM'])/child::div[6]",
            locateStrategy: "xpath"
        },
        carouselSpan: {
            selector: "//div[@class = 'src-components-video-carousel-__VideoCarousel_at_2cIql']",
            locateStrategy: "xpath"
        },

        // Originals Container
        moreOriginals: {
            selector: "(//div[text()='More Originals'])[1]",
            locateStrategy: "xpath"
        },
        originalCardName: {
            selector: "(//div[@class = 'src-components-originals-__Card_athlete_sGFCY']/span)[1]",
            locateStrategy: "xpath"
        },
        originalCard: {
            selector: "(//div[@class = 'src-components-originals-__Card_backdrop_20u2U'])[1]",
            locateStrategy: "xpath"
        },

        originalCard_5th: {
            selector: "(//div[@class = 'src-components-originals-__Card_backdrop_20u2U'])[5]",
            locateStrategy: "xpath"
        },

        originalContainer: {
            selector: "//div[@class=  'src-components-heading-__Heading_text_Ub-4e']/child::div[text()='Originals']",
            locateStrategy: "xpath"
        },
        watchPreview: {
            selector: "(//div[text()='Watch Preview'])[1]",
            locateStrategy: "xpath"
        },
        heading_Originals: {
            selector: "//*[@class = 'src-components-in-the-spotlight-__InTheSpotlight_subTitle_1Xwjb']",
            locateStrategy: "xpath"
        },

        originalCardDetails:{
            // selector: "(//div[@class = 'src-components-originals-__Card_bodyText_1HbH4'])[1]",
            // src-components-originals-__Card_description_UGxFj
            selector: "(//div[@class = 'src-components-originals-__Card_athlete_sGFCY'])[1]/parent::*//child::p",
            locateStrategy: "xpath"
        },

        rightArrow_originals: {
            selector: "//a[@class = 'src-components-scroll-list-__ScrollList_arrow_1ro1L src-components-scroll-list-__ScrollList_right_1VKHt']",
            locateStrategy: "xpath"
        },

        leftArrow_originals: {
            selector: "//a[@class = 'src-components-scroll-list-__ScrollList_arrow_1ro1L src-components-scroll-list-__ScrollList_left_2W22S src-components-scroll-list-__ScrollList_prev_3GoB8']",
            locateStrategy: "xpath"
        },

        //Podcasts Container
        morePodcasts: {
            selector: "(//div[text()='More Podcasts'])[1]",
            locateStrategy: "xpath"
        },
        allEpisodes_podcast: {
            selector :"(//div[text() = 'All Episodes'])[1]",
            locateStrategy: "xpath"
        },
        podcastNameText: {
            selector :"(//div[@class = 'src-components-podcast-card-__PodcastCard_headline_2xYDX'])[1]",
            locateStrategy: "xpath"
        },
        podcastNameText_sec: {
            selector :"(//div[@class = 'src-components-podcast-card-__PodcastCard_headline_2xYDX'])[2]",
            locateStrategy: "xpath"
        },
        podcastContainer: {
            selector: "//div[@class=  'src-components-heading-__Heading_text_Ub-4e']/child::div[text()='UN Podcast Network']",
            locateStrategy: "xpath"
        },
        atheleteName_podcast: {
            selector: "(//div[text()='Feat'])[1]/child::span",
            locateStrategy: "xpath"
        },
        headphone_first: {
            selector: "(//*[@class = 'src-components-podcast-card-__PodcastCard_image__icon_2dsGV'])[1]",
            locateStrategy: "xpath"
        },
        headphone_sec: {
            selector: "(//*[@class = 'src-components-podcast-card-__PodcastCard_image__icon_2dsGV'])[2]",
            locateStrategy: "xpath"
        },
        stickyPlayerField: {
            selector: "//div[@class = 'src-components-sticky-player-__StickyPlayer_right_3B8NP']",
            locateStrategy: "xpath"
        },
        thumbnail_sticky: {
            selector: "//img[@alt= 'Cover art']",
            locateStrategy: "xpath"
        },
        podcastCard_first: {
            selector: "(//div[@class='src-components-podcast-card-__PodcastCard_contents_2FDsL'])[1]",
            locateStrategy: "xpath"
        },
        pausePodcast_first: {
            selector: "(//*[@class = 'icon icon-pause'])[1]",
            locateStrategy: "xpath"
        },
        pausePodcast_sec: {
            selector: "(//*[@class = 'icon icon-pause'])[2]",
            locateStrategy: "xpath"
        },
        podcastDetails_first: {
            selector: "(//div[@class= 'src-components-podcast-card-__PodcastCard_bodyText_30-cp']/p)[1]",
            locateStrategy: "xpath"
        },
        progressBar_sticky: {
            selector: "//*[@class = 'src-components-podcast-player-__Player_progress_1L1Fg']",
            locateStrategy: "xpath"
        },
        progressbarWidth_sticky: {
            selector: "//div[@class = 'src-components-podcast-player-__Player_bar_2x7Vs']",
            locateStrategy: "xpath"
        },
        progressbarWidth_card: {
            selector: "//div[@class = 'src-components-podcast-card-__PodcastCard_progressBarTrack_U4uFk']",
            locateStrategy: "xpath"
        },
        playPause_Sticky: {
            selector: "//button[@class = 'src-components-podcast-player-__Player_toggleButton_2IVvk']",
            locateStrategy: "xpath"
        },
        podcastName_sticky: {
            selector: "//a[@class = 'src-components-sticky-player-__StickyPlayer_currentlyPlayingTitleLink_6lQ2r']",
            locateStrategy: "xpath"
        },
        



        //Latest Stories
        playVideo_latestStories: {
            selector: "(//div[@class = 'src-components-video-card-__VideoCard_image__wrap_21PZD'])[5]",
            locateStrategy: "xpath"
        },
        playedVideoName_latestStories: {
            selector: "(//div[@class = 'src-components-video-card-__VideoCard_headline_hjMOF'])[5]",
            locateStrategy: "xpath"
        },
        loadMore_latestStories: {
            selector: "(//div[text()='Load More'])[1]",
            locateStrategy: "xpath"
        },
        addTag_latestStories: {
            selector: "(//div[@class ='src-components-video-card-__VideoCard_detailsInner_v5IVw'])[5]/div[2]/div[1]/div[2]",
            locateStrategy: "xpath"
        },
        tag_latestStories: {
            selector: "(//div[@class ='src-components-video-card-__VideoCard_detailsInner_v5IVw'])[5]/div[2]/div[1]",
            locateStrategy: "xpath"
        },

        //First Container
        playVideo_firstContainer: {
            selector: "(//div[@class = 'src-components-video-card-__VideoCard_image__wrap_21PZD'])[1]",
            locateStrategy: "xpath"
        },
        playedVideoName_firstContainer: {
            selector: "(//div[@class = 'src-components-video-card-__VideoCard_headline_hjMOF'])[1]",
            locateStrategy: "xpath"
        },
        playIcon_firstContainer: {
            selector: "(//*[@class = 'src-components-video-card-__VideoCard_image__icon_1JzAy'])[1]",
            locateStrategy: "xpath"
        },
        addTag_firstContainer: {
            selector: "(//div[@class = 'src-components-card-label-__CardLabelAnimated_text_16gHw'])[1]",
            locateStrategy: "xpath"
        },
        tag_firstContainer: {
            selector: "(//div[@class = 'src-components-card-label-__CardLabelAnimated_text_16gHw'])[1]/parent::div",
            locateStrategy: "xpath"
        },


        //Mobile

        googleButton_mob: {
            selector: "(//div[@class = 'src-components-signup-modal-__Button_icon_2iLlK' ]/following-sibling::span[contains(text(),'Google')])[2]",
            locateStrategy: "xpath"
        },
        facebookButton_mob: {
            selector: "(//div[@class = 'src-components-signup-modal-__Button_icon_2iLlK' ]/following-sibling::span[contains(text(),'Facebook')])[2]",
            locateStrategy: "xpath"
        },
        twitterButton_mob: {
            selector: "(//div[@class = 'src-components-signup-modal-__Button_icon_2iLlK' ]/following-sibling::span[contains(text(),'Twitter')])[2]",
            locateStrategy: "xpath"
        },
    }
}