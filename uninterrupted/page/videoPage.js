var videoPage = {
    
    clickOnVolumeFull: function () {
        return this
            .click('@volumefullButton')
    },
    clickOnPlayButton: function () {
        return this
            .waitForElementVisible('@playButton')
            .click('@playButton');
    },
    clickOnVolumeHalf: function () {
        return this
            .click('@volumehalfButton')
    },
    clickOnMute: function () {
      return this
          .waitForElementVisible('@muteButton')
          .click('@muteButton');
    },
    clickOnFullScreenOn: function () {
        return this
            .click('@fullScreenOn')
    },
    clickOnFullScreenOff: function () {
        return this
            .click('@fullScreenOff')
    },
    clickOnNextButton: function () {
        return this
            .waitForElementVisible('@nextButton')
            .click('@nextButton')
    },
    clickOnCloseButton: function () {
        return this
            .waitForElementVisible('@closeButton')
            .click('@closeButton')
    },
    clickOnSettingsButton: function(){
        return this
            .click('@settingsButton');
    },
    clickOnSettings2ndHDQuality: function(){
        return this
            .click('@settings2ndHDPQuality');
    },
    clickOnPlaylistButton: function(){
        return this
            .click('@jwPlayerPlaylistButton')
    },
    assertPlaylistPresent: function(){
        return this
            .assert.elementPresent('@jwPlayerPlaylistText')
    },
    assertPlaylistPresentMobile: function(){
        return this
            .assert.elementPresent('@jwPlayerPlaylistTextMobile')
    },
    clickOnJwPlayerPlaylist_first: function(){
        return this
            .click('@jwPlayerPlaylist_first')
    },
    clickOnJwPlayerPlaylist_firstMobile: function(){
        return this
            .click('@jwPlayerPlaylist_firstMobile')
    },

    assertVideoName: function(videoName) {
        return this
            .waitForElementVisible('@playedVideoName',8000)
            .assert.containsText('@playedVideoName',videoName)
    },
    clickOnUpArrow: function () {
        return this
            .waitForElementVisible('@upArrow')
            .click('@upArrow')
    },
    playFirstVideo: function() {
        return this
            .waitForElementVisible('@firstVideo')
            .click('@firstVideo');
    },
    playSecondVideo: function() {
        return this
            .click('@secondVideo');
    },
    clickOnFirstPlaylist: function(){
        return this
            .waitForElementVisible('@first_playlist')
            .click('@first_playlist');
    },
    assertPlaylistColor: function (color) {
        return this
            .assert.cssProperty('@playing_playlist','color',color)
    },
    clickOnFacebookShareButton: function(){
        return this
            .waitForElementVisible('@facebookShareButton',1000)
            .click('@facebookShareButton');
    },
    clickOnTweetButton: function(){
        return this
            .waitForElementVisible('@tweetButton')
            .click('@tweetButton');
    },
    clickOnEmbedVideo: function() {
        return this
            .waitForElementVisible('@embedVideo')
            .click('@embedVideo')
    },
    assertembedPopUpTextPresent: function(){
        return this
            .waitForElementVisible('@embedPopUpText')
            .assert.elementPresent('@embedPopUpText')
    },
    // assertembedPopUpLink: function(url){
    //     return this
    //         .waitForElementPresent('@embedPopUpLink')
    //         .assert.containsText('@embedPopUpLink', url)
    // },
    clickOnProgressBar: function(){
        return this
            .click('@progressBar')
    },
    clickOnRewindButton: function(){
        return this
            .click('@rewindButton')
    },
    clickOnTag: function(){
        return this
            .click('@feedTag');
    },
    assertSignUpModalPresent: function(){
        return this
            .assert.elementPresent('@signUpModal')
    },
    clickOnRightArrow: function(){
        return this
            .click('@rightArrow')
    },
    clickOnLeftArrow: function(){
        return this
            .click('@leftArrow')
    },
    assertLeftArrowPresent: function(){
      return this
        .assert.elementPresent('@visibleLeftArrow')  
    },
    assertLeftArrowNotPresent: function(){
        return this
          .assert.elementNotPresent('@visibleLeftArrow')  
    },
    assertDrawerNotVisible: function(){
        return this
            .assert.elementNotPresent('@drawerVisible')
    },
    assertDownArrowVisible: function(){
        return this
            .assert.elementPresent('@downArrow')
    },


    //Mobile
    clickOnPlayButton_mob: function(){
        return this
            .click('@playButton_mob');
    },
    clickOnNextButton_mob: function(){
        return this
            .click('@nextButton_mob');
    },
    clickOnFacebookShareButton_mob: function(){
        return this
            .click('@facebookShareButton_mob');
    },
    clickOnTweetButton_mob: function(){
        return this
            .click('@tweetButton_mob');
    },
    clickOnjwPlayerPlaylist_mob: function() {
        return this
            .click('@jwPlayerPlaylist_mob');
    }
    
}

module.exports = {
    commands: [videoPage],
    elements: {
        playButton: {
            selector: "//div[@class = 'jw-icon jw-icon-inline jw-button-color jw-reset jw-icon-playback']",
            locateStrategy: "xpath"
        },
        volumefullButton: {
            selector: "//*[@class = 'jw-svg-icon jw-svg-icon-volume-100']",
            locateStrategy: "xpath"
        },
        volumehalfButton: {
            selector: "//*[@class = 'jw-svg-icon jw-svg-icon-volume-50']",
            locateStrategy: "xpath"
        },
        muteButton: {
            selector: "(//div[@aria-label='Volume'])[1]",
            locateStrategy: "xpath"
        },
        volumeBar: {
            selector: "(//div[@class= 'jw-rail jw-reset'])[2]",
            locateStrategy: "xpath"
        },
        nextButton: {
            selector: "//div[@class = 'jw-icon jw-icon-inline jw-button-color jw-reset jw-icon-next']",
            locateStrategy: "xpath"
        },
        fullScreenOn: {
            selector: "//*[@class = 'jw-icon jw-icon-inline jw-button-color jw-reset jw-icon-fullscreen']",
            locateStrategy: "xpath"
        },
        fullScreenOff: {
            selector: "//*[@class = 'jw-icon jw-icon-inline jw-button-color jw-reset jw-icon-fullscreen jw-off']",
            locateStrategy: "xpath"
        },
        videoScreen: {
          selector: "(//video[@class='jw-video jw-reset'])[1]",
            locateStrategy: "xpath"
        },
        closeButton: {
            selector: "(//*[@class='icon icon-close'])[1]",
            locateStrategy: "xpath"
        },
        settingsButton:{
            selector: "//*[@class='jw-svg-icon jw-svg-icon-settings']/parent::*",
            locateStrategy: "xpath"
        },
        settings2ndHDPQuality: {
            selector: "(//button[@type = 'button' and @role = 'menuitemradio'])[2]",
            locateStrategy: "xpath"
        },
        jwPlayerPlaylistButton:{
            selector: "//*[@class='src-components-playlist-__Playlist_itemsWrap_O1Fal']/parent::*",
            locateStrategy: "xpath"
        },
        jwPlayerPlaylistText:{
            selector: "//div[@class = 'jw-related-title jw-reset']",
            locateStrategy: "xpath"
        },
        jwPlayerPlaylistTextMobile:{
            selector: "//div[@class = 'src-components-playlist-__Playlist_itemsWrap_O1Fal']",
            locateStrategy: "xpath"
        },
        jwPlayerPlaylist_first: {
            selector: "(//span[@class ='jw-item-index-text jw-related-item-title-text jw-reset'])[1]",
            locateStrategy: "xpath"
        },
        jwPlayerPlaylist_firstMobile: {
            selector: "//div[text() = 'Now playing']/parent::*/child::div[2]",
            locateStrategy: "xpath"
        },
        jwPlayerNextButton: {
            selector: "//div[@class = 'jw-nextup-title jw-reset']",
            locateStrategy: "xpath"
        },
        playedVideoName: {
            selector: "//div[text() = 'Now playing']/parent::*/child::div[2]",
            locateStrategy: "xpath"
        },
        upNext: {
            selector: "//a[text() = 'Up Next']",
            locateStrategy: "xpath"
        },
        upArrow: {
            selector: "//span[@class = 'src-components-video-layout-__VideoLayout_arrow_1TWWa src-components-video-layout-__VideoLayout_up_2Eom3']",
            locateStrategy: "xpath"
        },
        downArrow: {
            selector: "//span[@class = 'src-components-video-layout-__VideoLayout_arrow_1TWWa']",
            locateStrategy: "xpath"
        },
        firstVideo: {
            selector: "(//div[@class = 'src-components-playlist-__Item_details__title_9G7qE'])[1]",
            locateStrategy: "xpath"
        },
        secondVideo: {
            selector: "((//div[@class = 'src-components-playlist-__Item_details_2rUWM'])//child::*)[2]",
            locateStrategy: "xpath"
        },
        first_playlist: {
            selector: "(//a[@class = 'src-components-video-layout-__VideoLayout_link_lKDEz'])[1]",
            locateStrategy: "xpath"
        },
        playing_playlist: {
            selector: "//a[@class = 'src-components-video-layout-__VideoLayout_link_lKDEz src-components-video-layout-__VideoLayout_active_3_4tU']",
            locateStrategy: "xpath"
        },
        facebookShareButton: {
            selector: "(//div[@class = 'src-components-button-__Button_host_2lQMs src-components-button-__Button_fill_1XTBa src-components-button-__Button_facebook_3AxoM'])[3]",
            locateStrategy: "xpath"
        },
        tweetButton: {
            selector: "(//div[text() = 'Tweet'])[3]",
            locateStrategy: "xpath"
        },
        embedVideo: {
            selector: "//div[@class = 'src-components-button-__Button_host_2lQMs src-components-button-__Button_icon__inner_3Cdoo' and @data-primary]",
            locateStrategy: "xpath"
        },
        embedPopUpLink: {
            selector: "//input[@type = 'text']",
            locateStrategy: "xpath"
        },
        embedPopUpText: {
            selector: "//div[text() = 'Embed this video']",
            locateStrategy: "xpath"
        },
        progressBar: {
            selector: "(//div[@class= 'jw-rail jw-reset'])[1]",
            locateStrategy: "xpath"
        },
        rewindButton: {
            selector: "(//*[@class = 'jw-svg-icon jw-svg-icon-rewind'])[2]/parent::*",
            locateStrategy: "xpath"
        },
        feedTag: {
            selector: "(//div[@class = 'src-components-card-label-__CardLabelAnimated_text_16gHw'])[1]",
            locateStrategy: "xpath"
        },
        tag_feed: {
            selector: "(//div[@class = 'src-components-card-label-__CardLabelAnimated_text_16gHw'])[1]/parent::div",
            locateStrategy: "xpath"
        },
        signUpModal: {
            selector: "//div[@class = 'src-components-signup-modal-__SignupModal_overlay_image_1g5Lj']",
            locateStrategy: "xpath"
        },
        playedTime: {
            selector: "//div[@class = 'jw-icon jw-icon-inline jw-text jw-reset jw-text-elapsed']",
            locateStrategy: "xpath"
        },
        rightArrow: {
            selector: "//*[@class = 'icon icon-arrow-right2']",
            locateStrategy: "xpath"
        },
        leftArrow: {
            selector: "//*[@class = 'icon icon-arrow-left2']",
            locateStrategy: "xpath"
        },
        visibleLeftArrow: {
            selector: "//*[@class ='src-components-playlist-__Playlist_arrow_1JnBv src-components-playlist-__Playlist_left_22cnn src-components-playlist-__Playlist_isVisible_11atR']",
            locateStrategy: "xpath"
        },
        video_bottom: {
            selector: "//ul[@class = 'src-components-video-layout-__VideoLayout_list_2Jl9I']",
            locateStrategy: "xpath"
        },
        drawerVisible: {
            selector: "src-components-playlist-__Playlist_itemsWrap_O1Fal",
            locateStrategy: "xpath"
        },

        //Mobile
        playButton_mob: {
            selector: "//div[@class = 'jw-icon jw-icon-display jw-button-color jw-reset']",
            locateStrategy: "xpath"
        },
        nextButton_mob: {
            selector: "//div[@class = 'jw-icon jw-icon-next jw-button-color jw-reset']",
            locateStrategy: "xpath"
        },
        facebookShareButton_mob: {
            selector: "(//div[@class = 'src-components-button-__Button_host_2lQMs src-components-button-__Button_fill_1XTBa src-components-button-__Button_facebook_3AxoM'])[1]",
            locateStrategy: "xpath"
        },
        tweetButton_mob: {
            selector: "(//div[text() = 'Tweet'])[1]",
            locateStrategy: "xpath"
        },
        jwPlayerPlaylist_mob: {
            selector: "//div[@class ='jw-related-item-play jw-icon jw-icon-inline jw-reset']",
            locateStrategy: "xpath"
        }
    }
}