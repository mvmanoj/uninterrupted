var OriginalsCard = {
    assertOriginalAthleteName: function (name_athlete) {
        return this
            .assert.containsText('@originalAthlete',name_athlete);
    },
    clickOnWatchNow: function(){
        return this
            .click('@watchNow')
    },
    clickOnPlayIcon_first: function(){
        return this
            .click('@playButton_first')
    },
    assertPlayButtonBgColor_first: function(color){
        return this
            .assert.cssProperty('@playButton_first','background-color',color)
    },
    clickOnShare_facebook: function () {
        return this
            .waitForElementVisible('@share_facebook','100')
            .click('@share_facebook');
    },
    clickOnTweetButton: function () {
        return this
            .waitForElementVisible('@tweetButton','100')
            .click('@tweetButton');
    },
    clickOnShowTag: function() {
        return this
            .click('@showTag');
    },
    clickOnEpisodeTag_first: function() {
        return this
            .click('@episodeTag_first');
    },
    assertMoreOriginalPresent: function(){
        return this
            .assert.elementPresent('@moreOriginals');
    },
    clickOnOriginalCard_moreOriginals: function(){
        return this
            .click('@originalCard_moreOriginals');
    }
}

module.exports = {

    commands: [OriginalsCard],
    elements: {
        originalAthlete: {
            selector: "//div[@class = 'src-components-hero-show-__HeroShow_caption_3gp-D']/child::strong",
            locateStrategy: "xpath"
        },
        originalShowName: {
            selector: "(//div[@class = 'src-components-heading-__Heading_text_Ub-4e'])[1]/div",
            locateStrategy: "xpath"
        },
        watchNow: {
            selector: "(//div[@class = 'src-components-hero-show-__HeroShow_button_JAhfO'])[1]//div[1]",
            locateStrategy: "xpath"
        },
        episode_first: {
            selector: "(//img[@class = 'src-components-video-card-__VideoCard_image__tag_3gW5Z'])[1]",
            locateStrategy: "xpath"
        },
        playButton_first: {
            selector: "(//div[@class = 'src-components-video-card-__VideoCard_image__icon_1JzAy'])[1]",
            locateStrategy: "xpath"
        },
        playIcon_first: {
            selector: "(//*[@class='icon icon-play'])[1]",
            locateStrategy: "xpath"
        },
        episodeName_first: {
            selector: "(//div[@class = 'src-components-video-card-__VideoCard_headline_hjMOF'])[1]",
            locateStrategy: "xpath"
        },
        totalEpisode: {
            selector: "//ul[@class = 'src-components-hero-show-__HeroShow_list_icSvd']/li[2]",
            locateStrategy: "xpath"
        },
        totalCard: {
            selector: "//*[@class = 'src-components-video-items-grid-__VideoItemsGrid_host_2Q473']/div",
            locateStrategy: "xpath"
        },
        share_facebook: {
            selector: "(//*[text()='Share'])[1]",
            locateStrategy: "xpath"
        },
        tweetButton: {
            selector: "(//div[text()='Tweet'])[1]",
            locateStrategy: "xpath"
        },
        showTag: {
            selector: "(//div[@class ='src-components-card-label-__CardLabelAnimated_text_16gHw'])[1]",
            locateStrategy: "xpath"
        },
        tag_show: {
            selector: "(//div[@class ='src-components-card-label-__CardLabelAnimated_text_16gHw'])[1]/parent::div[1]",
            locateStrategy: "xpath"
        },
        episodeTag_first: {
            selector: "(//div[@class ='src-components-card-label-__CardLabelAnimated_text_16gHw'])[2]",
            locateStrategy: "xpath"
        },
        tag_episode: {
            selector: "(//div[@class ='src-components-card-label-__CardLabelAnimated_text_16gHw'])[2]/parent::div[1]",
            locateStrategy: "xpath"
        },
        moreOriginals: {
            selector: "//div[text()='More originals']",
            locateStrategy: "xpath"
        },
        originalCard_moreOriginals: {
            selector: "(//div[@class='src-components-originals-__Card_backdrop_20u2U'])[1]",
            locateStrategy: "xpath"
        },
        originalAthlete_moreOriginals: {
            selector: "(//div[@class='src-components-originals-__Card_athlete_sGFCY']/span)[1]",
            locateStrategy: "xpath"
        }
    }
}