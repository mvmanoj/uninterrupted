var athletesCard = {
    assertHeading: function (heading) {
        return this
            .waitForElementVisible('@headingText')
            .assert.value('@headingText',heading)
    },
    clickOnPlayButton: function () {
        return this
            .waitForElementVisible('@playButton')
            .click('@playButton')
    },
    assertAthleteName: function (athlete) {
        return this
            .waitForElementPresent('@athleteNameheading')
            .assert.containsText('@athleteNameheading', athlete)
    },
    clickOnHashTag: function () {
        return this
            .waitForElementPresent('@hashTag')
            .click('@hashTag');
    }
}

module.exports = {
    commands: [athletesCard],
    elements: {
        athleteNameheading: {
            selector: "(//div[@class='src-components-heading-__Heading_text_Ub-4e'])[1]",
            locateStrategy: "xpath"
        },
        playButton: {
            selector: "(//div[@class = 'src-components-search-results-__SearchResultCard_searchResultCard__icon_3XvYu'])[1]",
            locateStrategy: "xpath"
        },
        hashTag: {
            selector:"(//p[@class ='src-components-search-results-__SearchResultCard_overview__title_1_hnn'])[1]/parent::*/child::div[2]",
            locateStrategy: "xpath"
        },
        playAthletesVideo: {
            selector:"(//div[@class = 'src-components-search-results-__SearchResultCard_searchResultCard__icon_3XvYu'])[1]",
            locateStrategy: "xpath"
        }
    }
}