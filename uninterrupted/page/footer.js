var footer = {
    clickOnFacebookIcon: function(){
        return this
            .waitForElementVisible('@facebookIcon')
            .click('@facebookIcon');
    },
    clickOnTwitterIcon: function(){
        return this
            .waitForElementVisible('@twitterIcon')
            .click('@twitterIcon');
    },
    clickOnYoutubeIcon: function(){
        return this
            .waitForElementVisible('@youtubeIcon')
            .click('@youtubeIcon');
    },
    clickOnInstagramIcon: function(){
        return this
            .waitForElementVisible('@instagramIcon')
            .click('@instagramIcon');
    },
    clickOnTermsAndConditions: function(){
        return this
            .waitForElementVisible('@termsAndConditions')
            .click('@termsAndConditions');
    },
    clickOnPrivacy: function(){
        return this
            .waitForElementVisible('@privacy')
            .click('@privacy');
    },
    clickOnContact: function(){
        return this
            .waitForElementVisible('@contact')
            .click('@contact');
    },
    clickOnCareers: function(){
        return this
            .waitForElementVisible('@careers')
            .click('@careers');
    },
    enterSignUpEmail: function(email) {
        return this
            .waitForElementVisible('@signUpEmail')
            .setValue('@signUpEmail',email);
    },
    clickOnSignUpButton: function(){
        return this
            .waitForElementVisible('@signUpButton')
            .click('@signUpButton');
    },
    assertSignUpSuccessMessagePresent: function(){
        return this 
            .waitForElementVisible('@signUpSuccessMessage')
            .assert.elementPresent('@signUpSuccessMessage');
    },
    assertSuccessMessageNotPresent: function(){
        return this 
            .assert.elementNotPresent('@signUpSuccessMessage');
    },
    clickOnUninterruptedLogo: function(){
        return this
            .waitForElementVisible('@uninterruptedLogo')
            .click('@uninterruptedLogo');
    },
}

module.exports = {
    commands: [footer],
    elements: {
        facebookIcon: {
            selector: "(//a[@href='https://www.facebook.com/Uninterrupted' and @class = 'src-components-social-bar-__Item_host_3EfCs'])[2]",
            locateStrategy: "xpath"
        },
        twitterIcon: {
            selector: "(//a[@href='https://twitter.com/uninterrupted' and @class = 'src-components-social-bar-__Item_host_3EfCs'])[2]",
            locateStrategy: "xpath"
        },
        youtubeIcon: {
            selector: "(//a[@href='https://www.youtube.com/uninterrupted?sub_confirmation=1' and @class = 'src-components-social-bar-__Item_host_3EfCs'])[2]",
            locateStrategy: "xpath"
        },
        instagramIcon: {
            selector: "(//a[@href='https://www.instagram.com/uninterrupted' and @class = 'src-components-social-bar-__Item_host_3EfCs'])[2]",
            locateStrategy: "xpath"
        },
        termsAndConditions: {
            selector: "//a[text() = 'Terms']",
            locateStrategy: "xpath"
        },
        privacy: {
            selector: "//a[text() = 'Privacy']",
            locateStrategy: "xpath"
        },
        contact: {
            selector: "//a[text() = 'Contact']",
            locateStrategy: "xpath"
        },
        careers: {
            selector: "//a[text() = 'Careers']",
            locateStrategy: "xpath"
        },
        signUpEmail: {
            selector: "(//input[@type = 'email'])[1]",
            locateStrategy: "xpath"
        },
        signUpButton: {
            selector: "(//input[@type = 'submit'])[1]",
            locateStrategy: "xpath"
        },
        signUpSuccessMessage: {
            selector: "//div[text() = 'Thank you for signing up']",
            locateStrategy: "xpath"
        },
        uninterruptedLogo: {
            selector: "//a[@class = 'src-components-footer-__Footer_logo_e9jEr']",
            locateStrategy: "xpath"
        },
    }
}