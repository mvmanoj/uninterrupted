var commonMethods= require('../common/commonMethods');

module.exports={

    // JSON Format report output
    reporter: function(result, cb){
        var contentTap,content;
        var fs = require('fs');
        // Write to file in JSON format
        fs.writeFile("test-json", JSON.stringify(result), function(err) {
            if(err) {
                return console.log(err);
            }
            // First read the JSON file
            fs.readFile('./test-json', function read(err, data) {
                if (err) {
                    return console.log(err);
                } else {
                    content = data;
                    contentTap = commonMethods.convertToTAP(content);
                    // Tap Format report output
                    fs.writeFile("test-tap", contentTap, function(err) {
                        if(err) {
                            return console.log(err);
                        }
                    })
                }
            });
        }); 
    cb();
    },

    // Header
    uninterrupted_facebook: "UNINTERRUPTED - Home | Facebook",
    uninterrupted_twitter: "UNINTERRUPTED (@uninterrupted) | Twitter",
    uninterrupted_youtube: "UNINTERRUPTED - YouTube",
    uninterrupted_instagram: "UNINTERRUPTED (@uninterrupted) • Instagram photos and videos",
    uninterrupted_connectTitle: "UNINTERRUPTED - Sign Up for Video and Podcasts from Pro Athletes",
    search_ghostText:"Search",

    //HomePage
    uninterrupted_url: "https://www.uninterrupted.com/",
    title_homepage:"UNINTERRUPTED - Sign Up for Video and Podcasts from Pro Athletes",
    connectUs:"Connect with us",
    email_newsletter: "mt@qapitol.com",
    invalidEmail_newsletter: "abc",

    redColor: "rgba(237, 32, 39, 1)",
    playIconBGColor:"rgba(237, 32, 39, 0.85)",
    redBGColor: "rgba(240, 27, 27, 1)",
    podcastBGColor: "rgba(27, 43, 51, 1)",
    headphoneBGColor: "rgba(0, 0, 0, 0)",
    FBTwitterIconFill: "rgb(237, 32, 39)",
    whiteColor: "rgba(0, 0, 0, 0)",
    greyColor: "rgba(188, 188, 188, 1)",
    

    //Carousel
    style_SixCarousel: "transform: translateX(-75px);",
    style_SecCarousel: "transform: translateX(-15px);",
    style_FirstCarousel: "transform: translateX(0px);",

    //Video Page
    played_playlist_color:"rgba(51, 51, 51, 1)",
    quality_720p: "720p",

    //facebook
    emailID_Fb:"testingqapitol@gmail.com",
    pass_Fb:"Qapitol@123",
    title_facebook: "Facebook",

    //Google
    title_google: "Sign in – Google accounts",
    email_google: "mt@qapitol.com",
    pass_google: "Qapitol@123",

    //Twitter
    title_twitter: "Twitter / Authorise an application",
    email_twitter: "testuninttwitter@gmail.com",
    pass_twitter: "Qapitol@123",

    //Feed Page
    title_feedPage:"UNINTERRUPTED - Feed",
    YourFeedText:"YOUR FEED",

    //Podcasts
    podcast_heading:"PODCASTS",
    bgColor_podcastCard:"rgba(255, 255, 255, 1)",
    bgColor_podcastDetails: "rgba(0, 0, 0, 0)",

    //Sticky Player
    volumeOnSticky: "#icon-volume-on",
    volumeOffSticky: "#icon-volume-off",
    playSticky: "#icon-play",
    pauseSticky: "#icon-pause",

    //Podcast Card
    bgColor_ShowNameFeed:"rgba(0, 0, 0, 0)",
    post_facebook:"Post to Facebook",
    pause_className:"icon icon-pause",
    applePodcast: "apple-podcasts-subscribe",
    spotifyText: "Spotify",
    overcastText: "Overcast",
    googlePlayText: "Google Play",

    //Athletes
    athletes_heading:"ATHLETES",
    bgColor_HashtagFeed:"rgba(0, 0, 0, 0)",

    //Originals
    originals_heading: "ORIGINALS",
    bgColor_arrow: "rgba(255, 255, 255, 1)",

    //JW Player
    state_pause_jwplayer:"paused",
    state_play_jwplayer:"playing",

    //Search
    searchKeyword: "king",
    searchText: "The",
    longSearchText: "The Decision",
    specialCharKeyword: "@!",
    invalidCharKeyword: "000000",
    videoPodcastKeyword: "72",
    lebronJames: "Lebron James",
    blankData: "    ",

    //Get In Touch
    name_getInTouch: "lebron",
    phone_getInTouch: "9999999990",
    email_getInTouch: "mt@qapitol.com",
    invalidEmail_getInTouch: "abcd",
    company_getInTouch: "Uninterrupted",
    message_getInTouch: "This message is to test Get in Touch message",
    exceededMessage_getInTouch: "The message is to test the automation of Get in Touch function of Contact in uninterrupted Website to check whether user is able to enter more than 250 characters in the message field. On Entering 250 characters user should be shown 0 characters left",
    exceededCharMessage: "0 characters left",

    // Edit Your account
    //twitter new a/c Uname UN and @UN64684292, login : untesttweet@gmail.com, pass: Qapitol@123
    editAccountText: "EDIT YOUR ACCOUNT",
    connectedText: "CONNECT",
    //twitter_emailID: "testtwitterunint@gmail.com",
    twitter_emailID: "untesttweet@gmail.com",
    facebook_emailID: "testfbunint@gmail.com",
    google_emailID: "testunint@gmail.com",
    password: "Qapitol@123"

};